<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


	<g:if test="${pageProperty(name:'page.pageTitle')?.trim()?.size() > 0}">
		<title><g:pageProperty name="page.pageTitle"/></title>
	</g:if>
	<g:else>
		<title>Baseball</title>
	</g:else>



	<asset:javascript src="application.js"/>
	<asset:stylesheet href="application.css"/>



	<meta name="viewport" content="width=device-width, initial-scale=1">


	<link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />


	<g:layoutHead/>



	<script type="text/javascript">
		<g:pageProperty name="page.javascript"/>
	</script>



</head>
<body >




	<g:pageProperty name="page.beforecontainer" />


	<div class="container-fluid">

		<div class="row">


			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="/">Home</a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<form class="navbar-form form-inline">

							<div class="form-group">
								<input type="text" id="search-input"  placeholder="Search players..." class="form-control"  />
							</div>

						</form>
					</ul>
				</div>
			</nav>






			<g:pageProperty name="page.content" />


			<!-- End Sidebar -->
		</div>

	</div>


</body>
</html>