<%@ page import="com.baseball.Player" %>



<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'gamedayId', 'error')} required">
	<label for="gamedayId">
		<g:message code="player.gamedayId.label" default="Gameday Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="gamedayId" maxlength="40" required="" value="${playerInstance?.gamedayId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="player.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${playerInstance?.firstName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="player.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${playerInstance?.lastName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'displayName', 'error')} ">
	<label for="displayName">
		<g:message code="player.displayName.label" default="Display Name" />
		
	</label>
	<g:textField name="displayName" value="${playerInstance?.displayName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'currentTeam', 'error')} required">
	<label for="currentTeam">
		<g:message code="player.currentTeam.label" default="Current Team" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="currentTeam" name="currentTeam.id" from="${com.baseball.Team.list()}" optionKey="id" required="" value="${playerInstance?.currentTeam?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'pitches', 'error')} ">
	<label for="pitches">
		<g:message code="player.pitches.label" default="Pitches" />
		
	</label>
	<g:textField name="pitches" value="${playerInstance?.pitches}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'bats', 'error')} ">
	<label for="bats">
		<g:message code="player.bats.label" default="Bats" />
		
	</label>
	<g:textField name="bats" value="${playerInstance?.bats}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'position', 'error')} required">
	<label for="position">
		<g:message code="player.position.label" default="Position" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="position" required="" value="${playerInstance?.position}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'currentPosition', 'error')} required">
	<label for="currentPosition">
		<g:message code="player.currentPosition.label" default="Current Position" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="currentPosition" required="" value="${playerInstance?.currentPosition}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="player.status.label" default="Status" />
		
	</label>
	<g:textField name="status" value="${playerInstance?.status}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'last10FantasyPoints', 'error')} ">
	<label for="last10FantasyPoints">
		<g:message code="player.last10FantasyPoints.label" default="Last10 Fantasy Points" />
		
	</label>
	<g:field name="last10FantasyPoints" value="${fieldValue(bean: playerInstance, field: 'last10FantasyPoints')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'seasonAvgFantasyPoints', 'error')} ">
	<label for="seasonAvgFantasyPoints">
		<g:message code="player.seasonAvgFantasyPoints.label" default="Season Avg Fantasy Points" />
		
	</label>
	<g:field name="seasonAvgFantasyPoints" value="${fieldValue(bean: playerInstance, field: 'seasonAvgFantasyPoints')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'seasonFloorFantasyPoints', 'error')} ">
	<label for="seasonFloorFantasyPoints">
		<g:message code="player.seasonFloorFantasyPoints.label" default="Season Floor Fantasy Points" />
		
	</label>
	<g:field name="seasonFloorFantasyPoints" value="${fieldValue(bean: playerInstance, field: 'seasonFloorFantasyPoints')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'seasonCeilingFantasyPoints', 'error')} ">
	<label for="seasonCeilingFantasyPoints">
		<g:message code="player.seasonCeilingFantasyPoints.label" default="Season Ceiling Fantasy Points" />
		
	</label>
	<g:field name="seasonCeilingFantasyPoints" value="${fieldValue(bean: playerInstance, field: 'seasonCeilingFantasyPoints')}"/>

</div>

