<table class="table table-bordered stats pitching">

    <tr>
        <th>Year</th>
        <th>W</th>
        <th class="advanced">L</th>
        <th>ERA</th>
        <th class="advanced">G</th>
        <th>GS</th>
        <th class="advanced">CG</th>
        <th class="advanced">SHO</th>
        <th class="advanced">S</th>
        <th class="advanced">BS</th>
        <th>IP</th>
        <th>H</th>
        <th class="advanced">R</th>
        <th>ER</th>
        <th>HR</th>
        <th>BB</th>
        <th>SO</th>
        <th class="advanced">PC</th>
        <th class="advanced">BF</th>
        <th class="advanced">Strikes</th>


        <th class="advanced">OBP</th>
        <th class="advanced">SLG</th>
        <th class="advanced">OPS</th>
        <th>WOBA</th>
        <th>wFDA</th>
        <th>ISO</th>

        <th>K%</th>
        <th>BB%</th>

        <th>FD</th>
        <th>FDAvg</th>
    </tr>


    <g:each in="${pitcherSeasons}" var="pitcherSeason">

        <tr>
            <td><a href="/player/pitchingLog?id=${player.id}&year=${pitcherSeason.year}">${pitcherSeason.year}</a></td>

            <td>${pitcherSeason.wins}</td>
            <td class="advanced">${pitcherSeason.losses}</td>
            <td>${pitcherSeason.era.formatTwo()}</td>
            <td class="advanced">${pitcherSeason.games}</td>
            <td>${pitcherSeason.starts}</td>
            <td class="advanced">${pitcherSeason.cg}</td>
            <td class="advanced">${pitcherSeason.sho}</td>
            <td class="advanced">${pitcherSeason.saves}</td>
            <td class="advanced">${pitcherSeason.bs}</td>
            <td>${pitcherSeason.ip}</td>
            <td>${pitcherSeason.h}</td>
            <td class="advanced">${pitcherSeason.runs}</td>
            <td>${pitcherSeason.er}</td>
            <td>${pitcherSeason.hr}</td>
            <td>${pitcherSeason.bb}</td>
            <td>${pitcherSeason.so}</td>



            <td class="advanced">${pitcherSeason.pc}</td>


            <td class="advanced">${pitcherSeason.battersFace}</td>
            <td class="advanced">${pitcherSeason.strikes}</td>

            <td class="advanced">${pitcherSeason.opponentObp?.formatThree()}</td>
            <td class="advanced">${pitcherSeason.opponentSlg?.formatThree()}</td>
            <td class="advanced">${pitcherSeason.opponentOps?.formatThree()}</td>

            <td>${pitcherSeason.opponentWOba?.formatThree()}</td>
            <td>${pitcherSeason.opponentWFda?.formatTwo()}</td>

            <td>${pitcherSeason.opponentIso?.formatThree()}</td>

            <td>${pitcherSeason.soPercent?.formatPercent()}</td>
            <td>${pitcherSeason.bbPercent?.formatPercent()}</td>

            <td>${pitcherSeason.fanduelPoints?.formatTwo()}</td>
            <td>${pitcherSeason.fanduelAvg?.formatTwo()}</td>

        </tr>

    </g:each>

    <tr class="total">
        <td>
            Total
        </td>

        <td>${pitcherCareer.wins}</td>
        <td class="advanced">${pitcherCareer.losses}</td>
        <td>${pitcherCareer.era.formatTwo()}</td>
        <td class="advanced">${pitcherCareer.games}</td>
        <td>${pitcherCareer.starts}</td>
        <td class="advanced">${pitcherCareer.cg}</td>
        <td class="advanced">${pitcherCareer.sho}</td>
        <td class="advanced">${pitcherCareer.saves}</td>
        <td class="advanced">${pitcherCareer.bs}</td>
        <td>${pitcherCareer.ip}</td>
        <td>${pitcherCareer.h}</td>
        <td class="advanced">${pitcherCareer.runs}</td>
        <td>${pitcherCareer.er}</td>
        <td>${pitcherCareer.hr}</td>
        <td>${pitcherCareer.bb}</td>
        <td>${pitcherCareer.so}</td>



        <td class="advanced">${pitcherCareer.pc}</td>


        <td class="advanced">${pitcherCareer.battersFace}</td>
        <td class="advanced">${pitcherCareer.strikes}</td>

        <td class="advanced">${pitcherCareer.opponentObp?.formatThree()}</td>
        <td class="advanced">${pitcherCareer.opponentSlg?.formatThree()}</td>
        <td class="advanced">${pitcherCareer.opponentOps?.formatThree()}</td>
        <td>${pitcherCareer.opponentWOba?.formatThree()}</td>
        <td>${pitcherCareer.opponentWFda?.formatTwo()}</td>
        <td>${pitcherCareer.opponentIso?.formatThree()}</td>

        <td>${pitcherCareer.soPercent?.formatPercent()}</td>
        <td>${pitcherCareer.bbPercent?.formatPercent()}</td>

        <td>${pitcherCareer.fanduelPoints?.formatTwo()}</td>
        <td>${pitcherCareer.fanduelAvg?.formatTwo()}</td>

    </tr>



</table>