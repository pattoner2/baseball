<div class="container">
    <div class="row">


        <h3><a href="${player.permaLink}">${player.fullName} <small>${player.currentTeam.abbrev} ${player.currentPosition}</small></a></h3>

        <h5>${pitcherSeason?.year} Season</h5>
        <table cellspacing="0" cellpadding="0" border="0" id="pc" class="table table-bordered hitter-season-summary">
            <tbody>
                <tr>
                    <th title="W-L">W-L</th>
                    <th title="SV">SV</th>
                    <th title="K">K</th>
                    <th title="ERA">ERA</th>
                    <th title="wFDA">wFDA</th>
                    <th title="Fandeul Points">FD</th>
                </tr>
                <tr>
                    <td>${pitcherSeason?.wins} - ${pitcherSeason?.losses}</td>
                    <td>${pitcherSeason?.saves}</td>
                    <td>${pitcherSeason?.so}</td>
                    <td>${pitcherSeason?.era?.formatTwo()}</td>
                    <td>${pitcherSeason?.opponentWFda?.formatThree()}</td>
                    <td>${pitcherSeason?.fanduelAvg?.formatTwo()}</td>
                </tr>
            </tbody>
        </table>


        %{--<h5>${pitcherSeason.year} Splits</h5>--}%
        %{--<table cellspacing="0" cellpadding="0" border="0" id="pc" class="table table-bordered hitter-season-summary">--}%
            %{--<tbody>--}%
                %{--<tr>--}%
                    %{--<th title="Split">Split</th>--}%
                    %{--<th title="PA">PA</th>--}%
                    %{--<th title="Batting Average">AVG</th>--}%
                    %{--<th title="Runs Scored">R</th>--}%
                    %{--<th title="Home Runs">HR</th>--}%
                    %{--<th title="Runs Batted In">RBI</th>--}%
                    %{--<th title="Stolen Bases">SB</th>--}%
                    %{--<th title="wFDA">wFDA</th>--}%
                    %{--<th title="FD">FD</th>--}%
                %{--</tr>--}%

                %{--<tr><th colspan="9">Handedness</th></tr>--}%
                %{--<g:each in="${hittingSeasonSplits.findAll { it.isHandedness() }}" var="hitterSeasonSplit">--}%
                    %{--<g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit]" />--}%
                %{--</g:each>--}%


                %{--<tr><th colspan="9">Home/Road</th></tr>--}%
                %{--<g:each in="${hittingSeasonSplits.findAll { it.isHomeAway() }}" var="hitterSeasonSplit">--}%
                    %{--<g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit]" />--}%
                %{--</g:each>--}%


                %{--<tr><th colspan="9">Recent</th></tr>--}%
                %{--<g:each in="${hittingSeasonSplits.findAll { it.isRecent() }}" var="hitterSeasonSplit">--}%
                    %{--<g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit]" />--}%
                %{--</g:each>--}%


            %{--</tbody>--}%
        %{--</table>--}%





        <h5>Recent Games</h5>
        <table cellspacing="0" cellpadding="0" border="0" id="pc2"  class="table table-bordered hitter-recent-gamelog">
            <tbody>
                <tr>
                    <th title="Date" >DATE</th>
                    <th title="Opponent">OPP</th>
                    <th title="IP">IP</th>
                    <th title="H">H</th>
                    <th title="HR">HR</th>
                    <th title="ER">ER</th>
                    <th title="BB">BB</th>
                    <th title="K">K</th>
                    <th title="DEC">DEC</th>
                    <th title="Fantasy Points">FD</th>
                </tr>
                <g:each in="${recentGameLogs}" var="recentGameLog">
                    <tr>
                        <td>${recentGameLog.game.startDate.format("M/dd")}</td>
                        <td>${recentGameLog.opponent.abbrev}</td>
                        <td>${recentGameLog.ip}</td>
                        <td>${recentGameLog.h}</td>
                        <td>${recentGameLog.hr}</td>
                        <td>${recentGameLog.er}</td>
                        <td>${recentGameLog.bb}</td>
                        <td>${recentGameLog.so}</td>
                        <td>${recentGameLog.wins ? 'W' : ''}${recentGameLog.losses ? 'L' : ''}</td>
                        <td align="right">${recentGameLog.fanduelPoints}</td>
                    </tr>
                </g:each>

            </tbody>
        </table>



    </div>
</div>