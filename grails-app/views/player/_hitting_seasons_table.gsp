<table class="table table-bordered stats hitting">

    <tr>
        <th>Year</th>
        <th>G</th>
        <th>PA</th>
        <th>AB</th>
        <th>R</th>
        <th>H</th>
        <th>2B</th>
        <th>3B</th>
        <th>HR</th>
        <th>RBI</th>
        <th>SB</th>
        <th>CS</th>
        <th>BB</th>
        <th>SO</th>
        <th>BA</th>
        <th>OBP</th>
        <th>SLG</th>
        <th>OPS</th>
        <th class="advanced">TB</th>
        <th class="advanced">GDP</th>
        <th>HPB</th>
        <th class="advanced">SH</th>
        <th class="advanced">SF</th>
        <th class="advanced">IBB</th>

        <th class="advanced">LOB</th>
        <th class="advanced">GO</th>
        <th class="advanced">FO</th>
        <th>K%</th>
        <th>BB%</th>
        <th>ISO</th>

        <th>wOBA</th>
        <th>wFDA</th>

        <th>FD</th>
        <th>FDAvg</th>
    </th>
    </tr>

    <g:each in="${hittingSeasons}" var="hittingSeason">
        <tr>
            <td>
                <a href="/player/hittingLog?id=${player.id}&year=${hittingSeason.year}">${hittingSeason.year}</a>
            </td>

            <td>${hittingSeason.games}</td>
            <td>${hittingSeason.pa}</td>
            <td>${hittingSeason.atBats}</td>
            <td>${hittingSeason.runs}</td>
            <td>${hittingSeason.hits}</td>
            <td>${hittingSeason.doubles}</td>
            <td>${hittingSeason.triples}</td>
            <td>${hittingSeason.homeRuns}</td>
            <td>${hittingSeason.rbi}</td>
            <td>${hittingSeason.sb}</td>
            <td>${hittingSeason.cs}</td>
            <td>${hittingSeason.bb}</td>
            <td>${hittingSeason.so}</td>
            <td>${hittingSeason.avg?.formatThree()}</td>
            <td>${hittingSeason.obp?.formatThree()}</td>
            <td>${hittingSeason.slg?.formatThree()}</td>
            <td>${hittingSeason.ops?.formatThree()}</td>
            <td class="advanced">${hittingSeason.tb}</td>
            <td class="advanced">${hittingSeason.gidp}</td>
            <td>${hittingSeason.hbp}</td>
            <td class="advanced">${hittingSeason.sacBunts}</td>
            <td class="advanced">${hittingSeason.sacFlys}</td>
            <td class="advanced">${hittingSeason.ibb}</td>

            <td class="advanced">${hittingSeason.lob}</td>

            <td class="advanced">${hittingSeason.groundOuts}</td>
            <td class="advanced">${hittingSeason.flyOuts}</td>
            <td>${hittingSeason.soPercent?.formatPercent()}</td>
            <td>${hittingSeason.bbPercent?.formatPercent()}</td>
            <td>${hittingSeason.iso?.formatThree()}</td>
            <td>${hittingSeason.wOba?.formatThree()}</td>
            <td>${hittingSeason.wFda?.formatTwo()}</td>

            <td>${hittingSeason.fanduelPoints?.formatTwo()}</td>
            <td>${hittingSeason.fanduelAvg?.formatTwo()}</td>

        </tr>

    </g:each>

    <tr class="total">
        <td>
            Total
        </td>

        <td>${hitterCareer.games}</td>
        <td>${hitterCareer.pa}</td>
        <td>${hitterCareer.atBats}</td>
        <td>${hitterCareer.runs}</td>
        <td>${hitterCareer.hits}</td>
        <td>${hitterCareer.doubles}</td>
        <td>${hitterCareer.triples}</td>
        <td>${hitterCareer.homeRuns}</td>
        <td>${hitterCareer.rbi}</td>
        <td>${hitterCareer.sb}</td>
        <td>${hitterCareer.cs}</td>
        <td>${hitterCareer.bb}</td>
        <td>${hitterCareer.so}</td>
        <td>${hitterCareer.avg?.formatThree()}</td>
        <td>${hitterCareer.obp?.formatThree()}</td>
        <td>${hitterCareer.slg?.formatThree()}</td>
        <td>${hitterCareer.ops?.formatThree()}</td>
        <td class="advanced">${hitterCareer.tb}</td>
        <td class="advanced">${hitterCareer.gidp}</td>
        <td>${hitterCareer.hbp}</td>
        <td class="advanced">${hitterCareer.sacBunts}</td>
        <td class="advanced">${hitterCareer.sacFlys}</td>
        <td class="advanced">${hitterCareer.ibb}</td>

        <td class="advanced">${hitterCareer.lob}</td>

        <td class="advanced">${hitterCareer.groundOuts}</td>
        <td class="advanced">${hitterCareer.flyOuts}</td>
        <td>${hitterCareer.soPercent?.formatPercent()}</td>
        <td>${hitterCareer.bbPercent?.formatPercent()}</td>
        <td>${hitterCareer.iso?.formatThree()}</td>
        <td>${hitterCareer.wOba?.formatThree()}</td>
        <td>${hitterCareer.wFda?.formatTwo()}</td>
        <td>${hitterCareer.fanduelPoints?.formatTwo()}</td>
        <td>${hitterCareer.fanduelAvg?.formatTwo()}</td>

    </tr>


    <tr class="total">
        <td>
            Park Adj.
        </td>

        <td>${hitterCareer.games}</td>
        <td>${hitterCareer.pa}</td>
        <td>${hitterCareer.atBats}</td>
        <td>${hitterCareer.paRuns?.formatTwo()}</td>
        <td>${hitterCareer.paHits?.formatTwo()}</td>
        <td>${hitterCareer.paDoubles?.formatTwo()}</td>
        <td>${hitterCareer.paTriples?.formatTwo()}</td>
        <td>${hitterCareer.paHomeRuns?.formatTwo()}</td>
        <td>${hitterCareer.paRbi?.formatTwo()}</td>
        <td>${hitterCareer.paSb?.formatTwo()}</td>
        <td>${hitterCareer.paCs?.formatTwo()}</td>
        <td>${hitterCareer.paBb?.formatTwo()}</td>
        <td>${hitterCareer.paSo?.formatTwo()}</td>
        <td>${hitterCareer.paAvg?.formatThree()}</td>
        <td>${hitterCareer.paObp?.formatThree()}</td>
        <td>${hitterCareer.paSlg?.formatThree()}</td>
        <td>${hitterCareer.paOps?.formatThree()}</td>
        <td class="advanced">${hitterCareer.paTb}</td>
        <td class="advanced">${hitterCareer.gidp}</td>
        <td>${hitterCareer.hbp}</td>
        <td class="advanced">${hitterCareer.paSacBunts}</td>
        <td class="advanced">${hitterCareer.paSacFlys}</td>
        <td class="advanced">${hitterCareer.paIbb}</td>

        <td class="advanced">${hitterCareer.lob}</td>

        <td class="advanced">${hitterCareer.groundOuts}</td>
        <td class="advanced">${hitterCareer.flyOuts}</td>
        <td>${hitterCareer.soPercent?.formatPercent()}</td>
        <td>${hitterCareer.bbPercent?.formatPercent()}</td>
        <td>${hitterCareer.paIso?.formatThree()}</td>
        <td>${hitterCareer.paWOba?.formatThree()}</td>
        <td>${hitterCareer.paWFda?.formatTwo()}</td>

        <td>${hitterCareer.fanduelPoints?.formatTwo()}</td>
        <td>${hitterCareer.fanduelAvg?.formatTwo()}</td>

    </tr>



</table>