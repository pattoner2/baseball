<div class="container">
    <div class="row">


        <h3><a href="${player.permaLink}">${player.fullName} <small>${player.currentTeam.abbrev} ${player.currentPosition}</small></a></h3>

        <h5>${hitterSeason.year} Season</h5>
        <table cellspacing="0" cellpadding="0" border="0" id="pc" class="table table-bordered hitter-season-summary">
            <tbody>
                <tr>
                    <th title="Batting Average">AVG</th>
                    <th title="PA">PA</th>
                    <th title="Runs Scored">R</th>
                    <th title="Home Runs">HR</th>
                    <th title="Runs Batted In">RBI</th>
                    <th title="Stolen Bases">SB</th>
                    <th title="wFDA">wFDA</th>
                    <th title="Fandeul Points">FD</th>
                </tr>
                <tr>
                    <td>${hitterSeason.avg?.formatThree()}</td>
                    <td>${hitterSeason.pa}</td>
                    <td>${hitterSeason.runs}</td>
                    <td>${hitterSeason.homeRuns}</td>
                    <td>${hitterSeason.rbi}</td>
                    <td>${hitterSeason.sb}</td>
                    <td>${hitterSeason.wFda?.formatThree()}</td>
                    <td>${hitterSeason.fanduelAvg?.formatTwo()}</td>
                </tr>
                <tr>
                    <th colspan="8">Park Adjusted</th>
                </tr>
                <tr>
                    <td>${hitterSeason.paAvg?.formatThree()}</td>
                    <td>${hitterSeason.pa}</td>
                    <td>${hitterSeason.paRuns?.toInteger()}</td>
                    <td>${hitterSeason.paHomeRuns?.toInteger()}</td>
                    <td>${hitterSeason.paRbi?.toInteger()}</td>
                    <td>${hitterSeason.paSb?.toInteger()}</td>
                    <td>${hitterSeason.paWFda?.formatThree()}</td>
                    <td>${hitterSeason.fanduelAvg?.formatTwo()}</td>
                </tr>
            </tbody>
        </table>


        <h5>${hitterSeason.year} Splits</h5>
        <table cellspacing="0" cellpadding="0" border="0" id="pc" class="table table-bordered hitter-season-summary">
            <tbody>
                <tr>
                    <th title="Split">Split</th>
                    <th title="PA">PA</th>
                    <th title="Batting Average">AVG</th>
                    <th title="Runs Scored">R</th>
                    <th title="Home Runs">HR</th>
                    <th title="Runs Batted In">RBI</th>
                    <th title="Stolen Bases">SB</th>
                    <th title="wFDA">wFDA</th>
                    <th title="FD">FD</th>
                </tr>

                <tr>
                    <th colspan="9">Handedness</th>
                </tr>

                <g:each in="${hittingSeasonSplits.findAll { it.isHandedness() }}" var="hitterSeasonSplit">
                    <g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit, todayHitterGameLog: todayHitterGameLog]" />
                </g:each>


                <tr><th colspan="9">Home/Road</th></tr>
                <g:each in="${hittingSeasonSplits.findAll { it.isHomeAway() }}" var="hitterSeasonSplit">
                    <g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit, todayHitterGameLog: todayHitterGameLog]" />
                </g:each>


                <tr><th colspan="9">Recent</th></tr>
                <g:each in="${hittingSeasonSplits.findAll { it.isRecent() }}" var="hitterSeasonSplit">
                    <g:render template="hitting_splits_row" model="[hitterSeasonSplit: hitterSeasonSplit]" />
                </g:each>


            </tbody>
        </table>





        <h5>Recent Games</h5>
        <table cellspacing="0" cellpadding="0" border="0" id="pc2"  class="table table-bordered hitter-recent-gamelog">
            <tbody>
                <tr>
                    <th title="Date" >DATE</th>
                    <th title="Opponent">OPP</th>
                    <th title="Hits / At Bats">H/AB</th>
                    <th title="Runs Scored">R</th>
                    <th title="Home Runs">HR</th>
                    <th title="Runs Batted In">RBI</th>
                    <th title="Stolen Bases">SB</th>
                    <th title="Fantasy Points">FD</th>
                </tr>
                <g:each in="${recentGameLogs}" var="recentGameLog">
                    <tr>
                        <td>${recentGameLog.game.startDate.format("M/dd")}</td>
                        <td>${recentGameLog.opponent.abbrev}</td>
                        <td>${recentGameLog.hits}/${recentGameLog.atBats}</td>
                        <td>${recentGameLog.runs}</td>
                        <td>${recentGameLog.homeRuns}</td>
                        <td>${recentGameLog.rbi}</td>
                        <td>${recentGameLog.sb}</td>
                        <td align="right">${recentGameLog.fanduelPoints}</td>
                    </tr>
                </g:each>

            </tbody>
        </table>



    </div>
</div>