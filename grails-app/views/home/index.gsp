<g:applyLayout name="main">

    <head>

    </head>








    <content tag="javascript">

        $(document).ready(function() {

        var home = new Home().init();

        });


    </content>

    <content tag="content">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-form">
                        <div class='input-group date' id="date-picker" data-time="${date.getTime()}">
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <button id="clear-active-team" class="btn btn-default navbar-btn">Clear Teams</button>
                    </div>

                </div>
            </div>
        </nav>


        <div class="row">
            <div class="col-lg-5 col-lg-offset-1">
                <table class="table" id="games" >

                    <tr>
                        <th>Time</th>
                        <th>Away</th>
                        <th>Lineup</th>
                        <th>Home</th>
                        <th>Lineup</th>
                        <th>Weather</th>
                        <th>Wind</th>
                    </tr>


                    <g:each in="${games}" var="game">
                        <tr class="game ${game.hasStarted() ? 'started' : ''}">
                            <td class="game-time">
                                <g:formatDate date="${game.startDate}" timeStyle="short" type="time" />
                            </td>
                            <td class="game-team">
                                <button value="${game.awayTeam.abbrev}" data-id="${game.id + "-A"}" class="btn btn-sm btn-default team-button" >
                                    ${game.awayTeam.abbrev}
                                </button>
                                <button class="exclude-button" data-abbrev="${game.awayTeam.abbrev}" data-id="${game.id + "-A"}"><i class="glyphicon glyphicon-remove"></i></button>
                            </td>
                            <td class="lineup-status ${game.awayLineupStatus}">
                                ${game.awayLineupStatus}
                            </td>

                            <td class="game-team">
                                <button value="${game.homeTeam.abbrev}" data-id="${game.id + "-H"}" class="btn btn-sm btn-default team-button">
                                    @${game.homeTeam.abbrev}
                                </button>
                                <button class="exclude-button" data-abbrev="${game.homeTeam.abbrev}" data-id="${game.id + "-H"}"><i class="glyphicon glyphicon-remove"></i></button>
                            </td>
                            <td class="lineup-status ${game.homeLineupStatus}">
                                ${game.homeLineupStatus}
                            </td>
                            <td class="weather-status ${game.weatherStatus}">
                                <i class="wi ${game.weatherIcon}"></i> <g:if test="${game.temp}">${game.temp}&deg;</g:if>
                            </td>
                            <td class="wind-status">
                                <g:if test="${game.windDirection}">
                                    <i class="wi wi-wind ${game.windIcon}"></i>
                                    ${game.windSpeed}
                                </g:if>
                            </td>
                            <td class="">

                            </td>

                        </tr>
                    </g:each>
                </table>

            </div>

            <div class="col-lg-4 col-lg-offset-1">

                <table class="table table-bordered" id="selected-players">
                    <tr>
                        <th width="80">Pos</th>
                        <th>Name</th>
                        <th width="100">Salary</th>
                    </tr>
                </table>

            </div>
        </div>





        <div class="row">

            <div class="col-lg-10 col-lg-offset-1">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#hitters" aria-controls="hitters" role="tab" data-toggle="tab">Hitters</a></li>
                    <li role="presentation"><a href="#pitchers" aria-controls="pitchers" role="tab" data-toggle="tab">Pitchers</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">


                    <div role="tabpanel" class="tab-pane active" id="hitters">

                        <nav class="navbar navbar-inverse table-navbar">
                            <div class="container-fluid">
                                <div class="navbar-header">


                                    <form class="navbar-form form-inline">

                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search" id="hitter-table-search">
                                        </div>


                                        <div class="btn-group position-filter">
                                            <button class="btn btn-sm btn-default" data-value="ALL">All</button>
                                            <button class="btn btn-sm btn-default" data-value="C">C</button>
                                            <button class="btn btn-sm btn-default" data-value="1B">1B</button>
                                            <button class="btn btn-sm btn-default" data-value="2B">2B</button>
                                            <button class="btn btn-sm btn-default" data-value="3B">3B</button>
                                            <button class="btn btn-sm btn-default" data-value="SS">SS</button>
                                            <button class="btn btn-sm btn-default" data-value="OF">OF</button>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </nav>







                        <table id="hitter-table" class="players table table-hover table-bordered table-responsive table-condensed game-log">
                            <thead>
                            <tr class="meta-headers">
                                <th colspan="7"></th>

                                <th colspan="5">Pitcher</th>
                                <th colspan="5">Hitter</th>

                                <th colspan="2">Factors</th>

                                <th colspan="2">FP</th>
                                <th colspan="4">Proj</th>

                            </tr>


                            <tr class="stat-headers">

                                <th name="team">Team</th> <!-- hidden -->
                                <th name="pos">Pos</th> <!-- hidden -->
                                <th name="sel" width="5"></th>
                                <th name="n" width="170">Name</th>
                                <th name="pr" width="30">FD$</th>
                                <th name="o" width="20">Opp</th>

                                <!-- Vegas Odds -->
                                <th name="er" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Vegas Projected Runs Scored">Runs</span></th>

                                <!-- Pitcher -->
                                <th name="op" width="60" align="left">Pitcher</th>

                                <th name="opwFDA" width="25" ><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher wFDA for Season">wFDA</span></th>
                                <th name="opwFDASplit" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher wFDA Against Hitter Handedness Season">wFDA/H</span></th>
                                %{--<th name="opwFDAd" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher wFDA Against Hitter Handedness Percent Difference From Season wFDA">Diff</span></th>--}%
                                <th name="opwFDASplitHA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher wFDA Home/Away Season">wFDA/HA</span></th>
                                %{--<th name="opwFDAdSplitHAd" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher wFDA Home/Away Percent Difference From Season wFDA">Diff</span></th>--}%


                                <th name="opth" width="6"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Pitcher Handedness">H</span></th>

                                <!-- Hitter -->
                                <th name="b" width="6"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Hitter Handedness">H</span></th>

                                <th name="wFDA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Hitter wFDA Season">wFDA</span></th>
                                <th name="wFDASplit" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Hitter wFDA by Handedness">wFDA/H</span></th>
                                <th name="wFDASplitHA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Hitter wFDA Home/Away Season">wFDA/HA</span></th>
                                <th name="wFDASplitVs" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Hitter wFDA vs Pitcher Career (plate appearances)">vs/P</span></th>



                                <!-- Factors -->
                                <th name="pf" width="20"><span class="min-tt-md" data-toggle="tooltip" title="" data-placement="top" data-original-title="[Park Factor] The percentage difference in FP between a player's base projections and park-adjusted projections based on park factor by handedness">Park</span></th>
                                <th name="wif" width="20"><span class="min-tt-md" data-toggle="tooltip" title="" data-placement="top" data-original-title="[Wind Factor] The percentage difference in FP between a player's base projections and wind-adjusted projections">Wind</span></th>

                                <!-- FP -->
                                <th name="l7fp" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Last 7 Games FP">L7</span></th>
                                <th name="sfp" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Season Average FP">S</span></th>


                                <!-- Your Projection -->
                                <th name="hrpercent" width="20">HR%</th>
                                <th name="proj" width="20">FP</th>
                                <th name="value" width="20">Val</th>
                                <th name="actualPoints" width="20">Actual</th>


                            </tr>



                            </thead>
                            <tbody class="searchable">
                            <g:each in="${hitterGameLogs}" var="hitterGameLog">

                                <tr class="vertical-align bo-${hitterGameLog.battingOrderStatus}" id="h-${hitterGameLog.id}" data-id="${hitterGameLog.id}" data-game-id="${hitterGameLog.game.id}" data-salary="${hitterGameLog.fanduelSalary}" data-name="${hitterGameLog.hitter.fullName}" data-pos="${hitterGameLog.hitter.fanduelPosition}" data-team="${hitterGameLog.gameId}">

                                    <td name="team" class="left">${hitterGameLog.team.abbrev}</td>
                                    <td name="pos">${hitterGameLog.hitter.fanduelPosition}</td>

                                    <td name="sel"><input type="checkbox" class="select-player" /></td>
                                    <td class="playerName popup-ajax left" name="n" data-link="${hitterGameLog.hitter.permaLink}">
                                        <a data-toggle="popover" data-poload="/player/popupHitter/${hitterGameLog.hitter.id}">${hitterGameLog.hitter.fullName}</a>
                                        <span class="small">${hitterGameLog.team.abbrev} <span class="pos">${hitterGameLog.hitter.fanduelPosition}</span></span>
                                        <g:if test="${hitterGameLog.knownBattingOrder}">
                                            <span class="badge pull-right ${hitterGameLog.battingOrderStatus}">${hitterGameLog.knownBattingOrder?.toString()?.take(1)}</span>
                                        </g:if>
                                    </td>
                                    <td  name="pr"><g:formatNumber number="${hitterGameLog.fanduelSalary}" type="currency" currencyCode="USD" maxFractionDigits="0"/></td>	<!-- price -->
                                    <td  name="o" class="left">${hitterGameLog.opponent == hitterGameLog.game.homeTeam ? '@' : ''}${hitterGameLog.opponent.abbrev}</td>		<!-- opponent -->


                                    <!-- Vegas Odds -->
                                    <g:heated value="${hitterGameLog.vegasProjectedRunsScored}" list="${hitterGameLogs.collect { it.vegasProjectedRunsScored}}" name="er" format="two"/>


                                <!-- Pitcher -->
                                    <td class="oppPlayerName left" name="op">
                                        <a data-toggle="popover" data-poload="/player/popupPitcher/${hitterGameLog.opponentPitcher?.id}">${hitterGameLog.opponentPitcher?.abbrevName}</a>
                                    </td>
                                    <g:heated value="${hitterGameLog.opponentWFdaSeason}" list="${hitterGameLogs.collect { it.opponentWFdaSeason}}" name="opwFDA" format="two"/>
                                    <g:heated value="${hitterGameLog.opponentWFdaAgainstHandedness}" list="${hitterGameLogs.collect { it.opponentWFdaAgainstHandedness}}" name="opwFDSplit" format="two"/>
                                    <g:heated value="${hitterGameLog.opponentWfdaHomeAwaySeason}" list="${hitterGameLogs.collect { it.opponentWfdaHomeAwaySeason}}" name="opwFDSplitHA" format="two"/>


                                    <td  name="opth">${hitterGameLog.opponentPitcher?.pitches}</td>

                                    <!-- Hitter -->
                                    <td  name="b">${hitterGameLog.hitter?.bats}</td>


                                    <g:heated value="${hitterGameLog.wFdaSeason}" list="${hitterGameLogs.collect { it.wFdaSeason}}" name="wFDA" format="two"/>
                                    <g:heated value="${hitterGameLog.wFdaAgainstHandednessSeason}" list="${hitterGameLogs.collect { it.wFdaAgainstHandednessSeason}}" name="wFDASplit" format="two"/>
                                    <g:heated value="${hitterGameLog.wFdaHomeAwaySeason}" list="${hitterGameLogs.collect { it.wFdaHomeAwaySeason}}" name="wFDASplitHA" format="two"/>
                                    <g:heated value="${hitterGameLog.careerWfdaAgainstStarter}" list="${hitterGameLogs.collect { it.careerWfdaAgainstStarter}}" subtext="${hitterGameLog.careerAtBatsAgainstStarter?.toInteger()}" name="wFDASplitVs" format="two"/>


                                    <!-- Factors -->
                                    <g:heated value="${hitterGameLog.parkFactor}" list="${hitterGameLogs.collect { it.parkFactor}}" name="pf" format="percent"/>
                                    <g:heated value="${hitterGameLog.windFactor}" list="${hitterGameLogs.collect { it.windFactor}}" name="wif"/>

                                    <!-- Season -->
                                    <g:heated value="${hitterGameLog.last7AvgFanduelPoints}" list="${hitterGameLogs.collect { it.last7AvgFanduelPoints}}" name="l7fp" format="two"/>
                                    <g:heated value="${hitterGameLog.seasonAvgFanduelPoints}" list="${hitterGameLogs.collect { it.seasonAvgFanduelPoints}}" name="safd" format="two"/>

                                    <!-- Your Projection -->
                                    <g:heated value="${hitterGameLog.hrPercent}" list="${hitterGameLogs.collect { it.hrPercent}}" name="hrPercent" format="percent"/>
                                    <g:heated value="${hitterGameLog.projection}" list="${hitterGameLogs.collect { it.projection}}" name="proj" format="two"/>
                                    <g:heated value="${hitterGameLog.value}" list="${hitterGameLogs.collect { it.value}}" name="value" format="two"/>
                                    <g:heated value="${hitterGameLog.fanduelPoints}" list="${hitterGameLogs.collect { it.fanduelPoints}}" name="actualPoints" format="two"/>

                                </tr>

                            </g:each>





                            </tbody>
                        </table>












                    </div>
                    <div role="tabpanel" class="tab-pane" id="pitchers">


                        <nav class="navbar navbar-inverse table-navbar">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <form class="navbar-form form-inline">

                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search" id="pitcher-table-search">
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </nav>



                        <table id="pitcher-table" class="players table table-hover table-bordered table-responsive table-condensed game-log">
                            <thead>
                            <tr class="meta-headers">
                                <th colspan="5" rowspan="2">
                                </th>


                                <th colspan="3" rowspan="2">Vegas Odds</th>
                                <th colspan="3" rowspan="2">Opponent</th>
                                <th colspan="6" rowspan="1">Pitcher</th>

                                <th colspan="2" rowspan="2">Factors</th>

                                <th colspan="2" rowspan="2">FP</th>
                                <th colspan="3" rowspan="2">Proj</th>


                            </tr>
                            <tr class="meta-headers">

                                <th colspan="4"></th>
                                <th colspan="2">wFDA</th>

                            </tr>
                            <tr class="stat-headers">


                                <th name="team">Team</th> <!-- hidden -->

                                <th name="sel" width="5"></th>
                                <th name="n" width="170">Player Name</th>
                                <th name="pr" width="30">FD$</th>
                                <th name="o" width="30">Opp</th>


                                <!-- Vegas Odds -->
                                <th name="wp" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Vegas Projected Win Percentage">Win</span></th>
                                <th name="oer" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Vegas Projected Runs Against">RA</span></th>
                                <th name="ek" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Vegas Projected Strikeout Total">K</span></th>

                                <!-- Opponent -->
                                <th name="opHwFDA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Starting Lineup Average wFDA (empty until opposing starting lineup is confirmed)">wFDA</span></th>
                                <th name="opK" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Starting Lineup Average Strikeout Rate (empty until opposing starting lineup is confirmed)">K%</span></th>
                                <th name="opK" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Opposing Starting Lineup Projected Points">Proj</span></th>

                                <!-- Pitcher -->
                                <th name="K%" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="[Strikeout Rate] How often a pitcher strikeouts out the hitter on a per plate appearance basis">K%</span></th>
                                <th name="SIERA"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="[Skill-Interactive ERA] Measures pitcher's underlying skill level">SIERA</span></th>
                                <th name="xFIP"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="[Expected Fielding Independent Pitching] Measures a pitcher's expected run prevention independent of their defense">xFIP</span></th>
                                <th name="th" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Pitcher Handedness">H</span></th>
                                <th name="LHwFDA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Pitcher wFDA Season">S</span></th>
                                <th name="RHwFDA" width="20"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Pitcher wFDA Home/Away">H/A</span></th>


                                <!-- Factors -->
                                <th name="pf"><span class="min-tt-md" data-toggle="tooltip" title="" data-placement="top" data-original-title="[Park Factor] The percentage difference in FP between a player's base projections and park-adjusted projections based on park factor by handedness">Park</span></th>
                                <th name="wif"><span class="min-tt-md" data-toggle="tooltip" title="" data-placement="top" data-original-title="[Wind Factor] The percentage difference in FP between a player's base projections and wind-adjusted projections">Wind</span></th>


                                <!-- FP -->
                                <th name="l5fp"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Last 5 Starts FP">L5S</span></th>
                                <th name="sfp"><span data-toggle="tooltip" title="" data-placement="top" data-original-title="Season Average FP">S</span></th>


                                <!-- Your Projection -->
                                <th name="p" >FP</th>
                                <th name="ratio" >Val</th>
                                <th name="actual" >Actual</th>
                            </tr>
                            </thead>
                            <tbody class="searchable">

                            <g:each in="${pitcherGameLogs}" var="pitcherGameLog">

                                <tr class="vertical-align" id="p-${pitcherGameLog.id}" data-id="${pitcherGameLog.id}" data-game-id="${pitcherGameLog.game.id}" data-salary="${pitcherGameLog.fanduelSalary}" data-name="${pitcherGameLog.pitcher.fullName}" data-pos="P" data-team="${pitcherGameLog.gameId}">

                                    <td name="team">${pitcherGameLog.team.abbrev}</td>

                                    <td name="sel"><input type="checkbox" class="select-player" /></td>
                                    <!-- First 6 stats -->
                                    <td  class="playerName popup-ajax left" name="n" data-link="${pitcherGameLog.pitcher.permaLink}">
                                        <a href="${pitcherGameLog.pitcher.permaLink}" data-poload="/player/popupPitcher/${pitcherGameLog.pitcher.id}">${pitcherGameLog.pitcher.fullName}</a>
                                        <span class="small">${pitcherGameLog.team.abbrev}</span>
                                        <g:if test="${pitcherGameLog.probablePitcher}">
                                            <span class="pp">PP</span>
                                        </g:if>
                                    </td>
                                    <td  name="pr"><g:formatNumber number="${pitcherGameLog.fanduelSalary}" type="currency" currencyCode="USD" maxFractionDigits="0"/></td>	<!-- price -->
                                    <td  name="o" class="left">${pitcherGameLog.opponent == pitcherGameLog.game.homeTeam ? '@' : ''}${pitcherGameLog.opponent.abbrev}</td>		<!-- opponent -->


                                    <!-- Vegas Odds -->
                                    <g:heated value="${pitcherGameLog.vegasWinPercent}" list="${pitcherGameLogs.collect { it.vegasWinPercent}}" name="wp" format="percent"/>
                                    <g:heated value="${pitcherGameLog.vegasRunsAgainst}" list="${pitcherGameLogs.collect { it.vegasRunsAgainst}}" name="oer" format="two" inverse="true"/>
                                    <g:heated value="${pitcherGameLog.vegasStrikeouts}" list="${pitcherGameLogs.collect { it.vegasStrikeouts}}" name="vs"/>

                                    <!-- Opponent -->
                                    <g:heated value="${pitcherGameLog.opponentWFda}" list="${pitcherGameLogs.collect { it.opponentWFda}}" name="opHwFDA" format="two" inverse="true"/>
                                    <g:heated value="${pitcherGameLog.opponentSoPercent}" list="${pitcherGameLogs.collect { it.opponentSoPercent}}" name="opK" format="percent"/>
                                    <g:heated value="${pitcherGameLog.projectedOpponentPoints}" list="${pitcherGameLogs.collect { it.projectedOpponentPoints}}" name="opK" format="two" inverse="true"/>

                                    <!-- Pitcher -->
                                    <g:heated value="${pitcherGameLog.soPercentSeason}" list="${pitcherGameLogs.collect { it.soPercentSeason}}" name="K%" format="percent"/>
                                    <g:heated value="${pitcherGameLog.sieraSeason}" list="${pitcherGameLogs.collect { it.sieraSeason}}" name="SIERA" format="two"/>
                                    <g:heated value="${pitcherGameLog.xFip}" list="${pitcherGameLogs.collect { it.xFip}}" name="xFIP" format="two"/>
                                    <td class="min-col" name="th">${pitcherGameLog.pitcher.pitches}</td>
                                    <g:heated value="${pitcherGameLog.wFdaSeason}" list="${pitcherGameLogs.collect { it.wFdaSeason} }" name="LHwOBA" format="two" inverse="true"/>
                                    <g:heated value="${pitcherGameLog.wFdaSeasonHomeAway}" list="${pitcherGameLogs.collect { it.wFdaSeasonHomeAway}}" name="RHwOBA" format="two" inverse="true"/>


                                    <!-- Factors -->
                                    <td class="min-col" name="pf">${pitcherGameLog.parkFactor?.formatPercent()}</td>
                                    <td class="min-col" name="wif">${pitcherGameLog.windFactor?.formatPercent()}</td>

                                    <!-- Season -->

                                    <!-- Season -->
                                    <g:heated value="${pitcherGameLog.last5StartsAvgFanduelPoints}" list="${pitcherGameLogs.collect { it.last5StartsAvgFanduelPoints}}" name="l5fp" format="two"/>
                                    <g:heated value="${pitcherGameLog.seasonAvgFanduelPoints}" list="${pitcherGameLogs.collect { it.seasonAvgFanduelPoints}}" name="safd" format="two"/>

                                    <!-- Your Projection -->
                                    <g:heated value="${pitcherGameLog.projection}" list="${pitcherGameLogs.collect { it.projection}}" name="p" format="two"/>
                                    <g:heated value="${pitcherGameLog.value}" list="${pitcherGameLogs.collect { it.value}}" name="value" format="two"/>
                                    <g:heated value="${pitcherGameLog.fanduelPoints}" list="${pitcherGameLogs.collect { it.fanduelPoints}}" name="fanduelpoints" format="two"/>
                                </tr>
                            </g:each>


                            </tbody>
                        </table>














                    </div>
                </div>

            </div>


        </div>


















    </content>



</g:applyLayout>
