// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require moment
//= require_tree .
//= require bootstrap
//= require bootstrap-typeahead
//= require bootstrap-datetimepicker
//= require datatables
//= require_self



$(document).ready(function(){



	var searchOptions = {
		alignWidth: true,
		scrollBar: true,
		ajax: {
			url: '/search/searchJson',
			displayField: 'name',
			valueField: 'value',
			scrollBar: true,
			preDispatch: function(query) {
				return {
					query: query
				}
			},
			preProcess: function(data) {
				if (data.length == 0) {
					data.push({
						type: 'error',
						title: "No results found"
					});
				}

				return data;
			}
		},

		render: function (items) {

			var self = this;

			var players = new Array();
			var errors = new Array();

			$(items).each(function (i, item) {
				players.push(item);
			});


			items = [];


			if (errors.length > 0) {

				items.push($('<li><h4>No results found</h4></li>'))

			} else {


				if (players.length > 0) {

					items = $.merge( items,
						$(players).map(function (i, item) {
							var name = item['name'];
							var id = item['id'];

							i = $('<li><a href="#"></a></li>');
							i.attr('data-value', '/player/show/' + id );
							i.find('a').html(self.highlighter(name));
							return i[0];
						})
					);

				}



			}


			$(items).has('a').first().addClass('active');

			this.$menu.html(items);

			return this;
		},
		matcher: function() {
			return 1;
		},
		onSelect : function(info) {
			window.location = info.value;
		}
	};


	$('#search-input').typeahead(searchOptions);




	$('.stats').each(function(){

		var $table = $(this);

		$table.find("th").each(function(columnIndex) {

			var oldValue=0, currentValue=0, $elementToMark;
			var $trs = $table.find("tr");
			$trs.each(function(index, element) {
				$(this).find("td:eq("+ columnIndex +")").each(function() {
					if(currentValue>oldValue) oldValue = currentValue;

					currentValue = parseFloat($(this).html());

					if (!isNaN(currentValue)) {
						if(currentValue > oldValue) {
							$elementToMark = $(this);
						}

						if(index == $trs.length-1 && $elementToMark) {
							$elementToMark.addClass("highest");
						}

					}

				});
			});
		});



	});



});
















