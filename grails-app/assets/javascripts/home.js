function Home() {

    this._hitterTable = null;
    this._pitcherTable = null;

    this._teams = [];
    this._selectedPlayers = [];
    this._position = null;

}

Home.prototype = {

    constructor: Home,

    init : function() {

        var self = this;



        this._hitterTable = $('#hitter-table').DataTable({
            paging: false,
            "order": [[ 4, "desc" ]],
            columnDefs: [
                { targets: [0, 1], visible: false},
                { targets: [3], orderable: []},
                { targets: '_all', visible: true }
            ]
        });

        this._pitcherTable = $('#pitcher-table').DataTable({
            paging: false,
            "order": [[ 2, "desc" ]],
            columnDefs: [
                { targets: [0], visible: false},
                { targets: '_all', visible: true }
            ]
        });

        $('[data-toggle="tooltip"]').tooltip();


        this.initListeners();


        this.loadTeamState();
        this.loadPlayerState();
    },




    clearActiveTeamOnClick : function(e) {

        var self = this;

        var button = $(e.target);

        self.clearTeamButtons();
        self.clearExcludeButtons();


        self.updateSearchTeams();
        self.saveTeamState();
        self.filterTable();
        self.populateSelectedPlayerTable();

    },


    teamButtonOnClick : function(e) {

        var self = this;

        var button = $(e.currentTarget);

        self.clearExcludeButtons();



        if ($(button).hasClass("btn-primary")) {
            $(button).removeClass("btn-primary");
        } else {
            $(button).addClass("btn-primary");
        }




        self.updateSearchTeams();
        self.saveTeamState();
        self.filterTable();
        self.populateSelectedPlayerTable();

    },

    excludeButtonOnClick : function(e) {

        var self = this;

        var button = $(e.currentTarget);

        console.log(button);

        self.clearTeamButtons();


        if ($(button).hasClass("active")) {
            $(button).removeClass("active");
        } else {
            $(button).addClass("active");
        }


        self.updateSearchTeams();
        self.saveTeamState();
        self.filterTable();
        self.populateSelectedPlayerTable();

    },


    positionFilterOnClick: function (e) {

        var self = this;

        e.preventDefault();

        var button = $(e.target);

        $('.position-filter button').removeClass('active');
        $(this).addClass('active');


        var value = $(button).data('value');

        self._position = value;

        self.filterTable();

    },




    filterTable: function() {

        var self = this;

        var teamSearchString = self._teams.join('|');


        self._hitterTable
            .column(0).search(teamSearchString, true, false).draw();


        self._pitcherTable
            .column(0).search(teamSearchString, true, false).draw();



        if (self._position == "ALL" || self._position == null) {
            self._hitterTable
                .column(1).search('', true, false).draw();

        } else {
            self._hitterTable
                .column(1).search(self._position, true, false).draw();
        }


    },


    updateSearchTeams : function() {

        var self = this;

        self._teams = [];

        if ($('.team-button.btn-primary').size() > 0) {
            $('.team-button.btn-primary').each(function(){
                var team = $(this).val();
                self._teams.push(  team ) ;
            });
        }


        if ($('.exclude-button.active').size() > 0) {
            $('.exclude-button:not(".active")').each(function(){
                var team = $(this).data('abbrev');
                self._teams.push(  team ) ;
            });
        }

    },



    clearTeamButtons : function() {
        $( ".team-button" ).removeClass("btn-primary");
        $( ".team-button" ).addClass("btn-default");
    },

    clearExcludeButtons : function() {
        $(".exclude-button").removeClass("active");
    },


    getSelectedTeams : function() {
        
        var teams = [];
        
        
        var selectedTeams = $('.team-button.btn-primary').each(function() {
            teams.push($(this).data('id'));
        });
        
        return teams;
    },


    setSelectedTeams : function(teams) {

      var self = this;

      $.each(teams, function(index, team) {

          $('.team-button').each(function() {
              if (team == $(this).data('id')) {
                  $(this).trigger('click');
              }
          });

      });

    },




    getExcludedTeams : function() {

        var teams = [];

        var excludedTeams = $('.exclude-button.active').each(function() {
            teams.push($(this).data('id'));
        });

        return teams;
    },

    setExcludedTeams : function(teams) {
        var self = this;

        $.each(teams, function(index, team) {

            $('.exclude-button').each(function() {
                if (team == $(this).data('id')) {
                    $(this).trigger('click');
                }
            });

        });
    },


    getSelectedPlayers : function () {
        return this._selectedPlayers;
    },


    updateSelectedPlayers : function() {

        var players = [];

        var self = this;

        var hitters = $(self._hitterTable.cells().nodes());
        var pitchers = $(self._pitcherTable.cells().nodes());

        var rows = $.unique($.merge(hitters, pitchers).closest('tr'));

        var excludedTeams = self.getExcludedTeams();

        rows.filter('.selected').each(function() {

            var team = $(this).data('team');

            if ($.inArray(team, excludedTeams) == -1) {
                players.push({
                    id: $(this).data('id'),
                    pos: $(this).data('pos'),
                    name: $(this).data('name'),
                    team: $(this).data('team'),
                    salary: $(this).data('salary')
                });
            }
        });


        var order = ["P", "C", "1B", "2B", "3B", "SS", "LF", "RF", "CF", "OF"];

        var result = [];

        order.forEach(function(key) {
            // var found = false;
            players = players.filter(function(item) {
                if(item.pos == key) {
                    result.push(item);
                    // found = true;
                    return false;
                } else
                    return true;
            })
        }) ;


        this._selectedPlayers = result;

        return this._selectedPlayers;
    },


    setSelectedPlayers : function(players) {

        var self = this;

        $.each(players, function(index, player) {

            $('.players tr').each(function() {
                if (player.id == $(this).data('id')) {
                    $(this).find('input').trigger('click');
                }
            });

        });

    },


    saveTeamState : function() {
        localStorage.setItem("selectedTeams", JSON.stringify(this.getSelectedTeams()));
        localStorage.setItem("excludedTeams", JSON.stringify(this.getExcludedTeams()));

    },

    savePlayerState: function() {
        localStorage.setItem("selectedPlayers", JSON.stringify(this._selectedPlayers));
    },


    loadTeamState : function() {

        var selectedTeams = JSON.parse(localStorage.getItem("selectedTeams"));

        if (selectedTeams) {
            this.setSelectedTeams(selectedTeams);
        }

        var excludedTeams = JSON.parse(localStorage.getItem("excludedTeams"));

        if (excludedTeams) {
            this.setExcludedTeams(excludedTeams);
        }


    },

    loadPlayerState: function() {

        var selectedPlayers = JSON.parse(localStorage.getItem("selectedPlayers"));
        console.log(selectedPlayers);

        if (selectedPlayers) {
            this.setSelectedPlayers(selectedPlayers);
        }
    },


    togglePlayer : function(row) {

        var self = this;

        $(row).closest('tr').toggleClass('selected');

        self.updateSelectedPlayers();
        self.savePlayerState();
        self.populateSelectedPlayerTable();
    },


    populateSelectedPlayerTable : function() {
        
        var self = this;

        $('#selected-players tr').not(':first').remove();

        var selectedPlayers = self.getSelectedPlayers();

        $.each(selectedPlayers, function (index, player) {
            var salary = player.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('#selected-players').append("<tr id='selected-" + player.id + "'><td>" + player.pos + "</td><td>" + player.name + "</td><td class='salary'>$" + salary + "</td></tr>");
        });


        var totalSalary = 0;
        $("#selected-players .salary").each(function() {

            var value = $(this).text().replace("$", "").replace(",", "");
            // add only if the value is number
            if(!isNaN(value) && value.length != 0) {
                totalSalary += parseFloat(value);
            }
        });


        var remainingSalary = 35000 - totalSalary

        totalSalary = totalSalary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#selected-players').append("<tr><td></td><td><strong>Total</strong></td><td><strong>$" + totalSalary + "</strong></td></tr>");

        
    },


    initListeners : function() {

        var self = this;


        var time = $('#date-picker').data('time');
        $('#date-picker').datetimepicker({
            inline: false,
            format: 'LL',
            defaultDate: new Date(time)
        });

        $(document).on('dp.change', function(e) {
            window.location = "/?time=" + e.date.unix()

        });



        $( ".team-button" ).on('click', function(e) {
            self.teamButtonOnClick(e);
        });




        $('#clear-active-team').on('click', function(e) {
            self.clearActiveTeamOnClick(e);
        });



        $(".exclude-button").on('click', function(e) {
            self.excludeButtonOnClick(e);
        });



        $('.position-filter button').on('click', function(e) {
            self.positionFilterOnClick(e);
        });



        $('#hitter-table-search').on('keyup', function() {
            self._hitterTable
                .search(this.value, true, false).draw();

        });


        $('#pitcher-table-search').on('keyup', function() {

            self._pitcherTable
                .search(this.value, true, false).draw();

        });

        $('.players tbody').on( 'click', '.select-player', function (e) {
            self.togglePlayer(this);
        } );






        $('*[data-poload]').on('click', function(e) {

            e.preventDefault();

            var e=$(this);

            $.get(e.data('poload'),function(d) {
                e.popover({
                    content: d,
                    html: true,
                    container: 'body'
                }).popover('show');
            });
        });


        $('body').on('click', function (e) {
            //did not click a popover toggle or popover
            if ($(e.target).data('toggle') !== 'popover'
                && $(e.target).parents('.popover.in').length === 0) {
                $('[data-poload]').popover('hide');
            }
        });


    }
}

