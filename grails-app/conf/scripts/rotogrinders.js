var Nightmare = require('nightmare');
require('nightmare-download-manager')(Nightmare);


var nightmare = Nightmare({ show: false })


var games = [];

nightmare
    .goto('https://rotogrinders.com/schedules/mlb')
    .evaluate(function(){

        var games = [];

        var rows = $('#tschedules tr');

        $(rows).each(function() {

            var cells = $(this).find("td");

            if (cells.length > 0) {

                var game = {
                    //scheduleId: $(cells).get(0).data('schedule-id'),
                    team: cells.eq(1).text(),
                    pitcher: cells.eq(2).text().replace(' (R)', '').replace(' (L)', ''),
                    opponent:  cells.eq(3).text().replace('vs. ', '').replace('@ ', '').replace(' (R)', '').replace(' (L)', '').slice(0,3),
                    moneyLine: cells.eq(5).text(),
                    overUnder: cells.eq(6).text(),
                    projectedRuns: cells.eq(7).text()
                };

                games.push(game);

            }
        });

        console.log(games);

        return games;
    })
    .end()
    .then(function (result) {
        console.log(result);
    });



