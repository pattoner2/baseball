var Nightmare = require('nightmare');
var vo = require('vo');

var nightmare = Nightmare({ show: true });




var run = function * () {

    nightmare.viewport(1300, 1000);

    yield nightmare.goto('https://www.fanduel.com/p/login');

    nightmare.type('input[type=email]', 'pat.toner2@gmail.com')
        .type('input[type=password]', 'crapshoot')
        .click('input[type=submit]')
        .wait('.lobby-header__create-contest-button')


        .evaluate(function () {
            var nbaLink = $('a:has(.sport-toggle-menu__sport-label:contains("NBA"))');
            $(nbaLink).addClass("nba-link");
        })

        .click('.nba-link')
        .wait(1000)



        .evaluate(function () {
            var handLink = $('a:has(.contest-type-menu-list__label:contains("3–100 Player"))');
            $(handLink).addClass("hund-link");
        })

        .click('.hund-link')
        .wait(1000)


        .evaluate(function () {
            var mainContestLink = $('a:has(.game-toggle-menu__label:contains("Main"))');
            $(mainContestLink).addClass("main-contest-link");
        })



        .click('.main-contest-link')
        .wait(1000);


    var previousHeight, currentHeight=0;
    while(previousHeight !== currentHeight) {
        console.log('here');
        previousHeight = currentHeight;
        var currentHeight = yield nightmare.evaluate(function() {
            window.scrollTo(0, 100000);
            return document.body.scrollHeight;
        });
        yield nightmare.wait(1000);
    }


    var foundContest = true;



    while(foundContest) {

        foundContest = yield nightmare.evaluate(function() {

            var id = null;

            $('.contest-list-items tr').each(function () {

                if (!id) {
                    var entries = $(this).find('.entries').text();
                    var size = $(this).find('.size').text();

                    var percentFilled = entries / size;

                    var contestLink = $(this).find('a.contest-name');

                    var isRemoved = $(this).hasClass('contest-removed');
                    var isClicked = $(contestLink).hasClass('click-me');

                    if (percentFilled > 0.65 && !isRemoved && !isClicked) {

                        contestLink.addClass('click-me');

                        id = $(this).attr('id');

                    }
                }

            });

            return id;
        });

        if (foundContest) {
            nightmare
                .click('.click-me')
                .wait(1000)
                .click('.contest-details-tabs a:nth-of-type(2)')
                .wait(1000);


            var allPlayers = [];


            var counter = 0;
            do {

                if (counter > 0) {

                    var hasNextLink = yield nightmare.evaluate(function() {

                        var nextPageLink = $('.entries-pagination-container a:contains("›")');
                        var isDisabled = nextPageLink.hasClass('disabled');

                        if (!isDisabled) {
                            nextPageLink.addClass('next-link');
                            return true;
                        }

                        return false;

                    });

                    if (hasNextLink) {
                        nightmare
                            .click('.next-link')
                            .wait(1000);
                    }


                }



                var foundPlayers = yield nightmare.evaluate(function () {

                    var players = [];

                    $('.hep-indicator').each(function (i) {
                        players.push($(this).parent().text().trim());
                    });

                    return players;
                });

                allPlayers = allPlayers.concat(foundPlayers);
                counter++;
            }
            while(foundPlayers.length == 20);




            nightmare.click("a.modal-close");


            console.log(foundContest);
            console.log(allPlayers);

        }




    }






    yield nightmare.end();
};

vo(run)(function(err) {
    console.dir(err);
    console.log('done');
});



