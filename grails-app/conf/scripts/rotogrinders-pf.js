var Nightmare = require('nightmare');


var nightmare = Nightmare({ show: false })

nightmare
    .goto('https://swishanalytics.com/partners/mlb/mlb-park-factors.php')
    .evaluate(function(){

        var factors = [];

        var rows = $('.overall-row-margin');

        $(rows).each(function() {

            var tableRows = $(this).find('.mar-bottom-5 tr');


            var hrRow = $(tableRows).get(1);
            var triplesRow = $(tableRows).get(2);
            var doublesRow = $(tableRows).get(3);
            var singlesRow = $(tableRows).get(4);
            var runsRow = $(tableRows).get(9);


            var factor = {
                name: $(this).find('h4.lato').text().trim(),
                lRuns: $(runsRow).find('td:eq(2)').text().trim(),
                lHr:  $(hrRow).find('td:eq(2)').text().trim(),
                l3b: $(triplesRow).find('td:eq(2)').text().trim(),
                l2b: $(doublesRow).find('td:eq(2)').text().trim(),
                l1b: $(singlesRow).find('td:eq(2)').text().trim(),
                rRuns: $(runsRow).find('td:eq(0)').text().trim(),
                rHr:  $(hrRow).find('td:eq(0)').text().trim(),
                r3b: $(triplesRow).find('td:eq(0)').text().trim(),
                r2b: $(doublesRow).find('td:eq(0)').text().trim(),
                r1b: $(singlesRow).find('td:eq(0)').text().trim()
            };

            factors.push(factor);

        });


        return factors;
    })
    .end()
    .then(function (result) {
        console.log(result);
    });



