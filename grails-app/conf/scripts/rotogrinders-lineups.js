var Nightmare = require('nightmare');
require('nightmare-download-manager')(Nightmare);


var nightmare = Nightmare({ show: false })




nightmare
    .goto('https://rotogrinders.com/lineups/mlb?site=fanduel')
    .evaluate(function(){

        function nthIndex(str, pat, n){
            var L= str.length, i= -1;
            while(n-- && i++<L){
                i= str.indexOf(pat, i);
                if (i < 0) break;
            }
            return i;
        }



        var games = [];

        var rows = $('.schedules li[data-role=lineup-card]');

        $(rows).each(function() {

            var time = $(this).data('time');
            var away = $(this).data('away');
            var home = $(this).data('home');
            var scheduleId = $(this).data('schedule-id');


            var weather = $(this).find('.weather-status span').attr('class');


            var weatherDiv = $(this).find('.weather');
            var weatherStatus = '';


            if ($(weatherDiv).hasClass('red')) {
                weatherStatus = 'red';
            } else if ($(weatherDiv).hasClass('green')) {
                weatherStatus = 'green';
            } else if ($(weatherDiv).hasClass('yellow')) {
                weatherStatus = 'yellow';
            }



            //Get precipitation
            var precipLi = $(weatherDiv).find('.precip li');

            var precips = [];
            $(precipLi).each(function() {

                var percentSpan = $(this).find('span').get(0);
                var timeSpan =  $(this).find('span').get(1);

                precips.push({
                    percent:  $(percentSpan).text(),
                    time: $(timeSpan).text()
                });

            });


            //Get other weather stats
            var windStatusDiv = $(weatherDiv).find('.wind-status');

            var windIconSpan = $(windStatusDiv).find('span').get(0);
            var windIcon = $(windIconSpan).attr('class');

            var weatherStatsLi = $(windStatusDiv).find('.stats li');


            var windAndTemp = $(weatherStatsLi).get(0);

            var windSpan = $(windAndTemp).find('span').get(0);
            var tempSpan = $(windAndTemp).find('span').get(2);

            var windDirection = $(windSpan).text();


            var players = [];

            var playersDivs = $(this).find('ul.players');




            var teams = [];

            $(playersDivs).each(function() {

                var team = {
                    players: []
                };

                if ($(this).parents('.away-team').size() > 0) {
                    team.abbrev = away;

                } else {
                    team.abbrev = home;
                }


                var status = 'confirmed';
                if ($(this).hasClass('unconfirmed')) {
                    status = 'unconfirmed';
                }

                team.status = status;

                $(this).find('.player').each(function() {

                    var pos = $(this).data('pos');

                    var orderSpan = $(this).find('.order').get(0);
                    var order = $(orderSpan).text();

                    var pnameLink = $(this).find('.pname a');
                    var pname = $(pnameLink).text();

                    if ($(pnameLink).data('url')) {
                        var pid = $(pnameLink).data('url').replace( /\D+/g, '');
                        team.players.push({
                            pos: pos,
                            order: order,
                            name: pname,
                            id: pid
                        });

                    }
                });

                teams.push(team);

            });




            var game = {
                time: time,
                away: away,
                home: home,
                scheduleId: scheduleId,
                weather: weather,
                weatherStatus: weatherStatus,
                precips: precips,
                windIcon: windIcon,
                windDirection: windDirection.substr(0,windDirection.indexOf(' ')),
                windSpeed: windDirection.substr(nthIndex(windDirection, ' ', 2)+1),
                temp: $(tempSpan).text(),
                teams: teams

            }


            games.push(game);


        });








        return games;
    })
    .end()
    .then(function (result) {
        console.log(JSON.stringify(result));
    });




