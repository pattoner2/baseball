package baseballg3


class BootStrap {

    def init = { servletContext ->

        TimeZone.setDefault(TimeZone.getTimeZone("America/New_York"))

        Double.metaClass.formatThree {
            String withLeadingZero = String.format("%.3f", delegate)

            int start = 0
            if (withLeadingZero.substring(0,1) == "0") start = 1

            return withLeadingZero.substring(start, withLeadingZero.length())
        }


        Double.metaClass.formatTwo {
            return String.format("%.2f", delegate)
            //return withLeadingZero.substring(1, withLeadingZero.length())
        }

        Double.metaClass.formatPercent {
            return String.format("%.1f",delegate * 100)+"%";
            //return withLeadingZero.substring(1, withLeadingZero.length())
        }


        Double.metaClass.formatPlusMinus {

            if (delegate > 0) {
                return "+" + String.format("%.2f", delegate)
            }

            if (delegate < 0) {
                return String.format("%.2f", delegate)
            }

            if (delegate == 0.0) {
                return ''
            }

            return delegate.toString()
        }


    }
    def destroy = {
    }
}
