package com.baseball

import com.baseball.AtBat
import com.baseball.Game
import com.baseball.Handedness
import com.baseball.HitterGameLog
import com.baseball.ParkFactorValues
import com.baseball.Pitch
import com.baseball.PitcherGameLog
import com.baseball.Player
import com.baseball.Runner
import com.baseball.Team

import com.baseball.olap.dimensions.BallPark
import com.baseball.olap.dimensions.GameTime
import com.baseball.olap.dimensions.Hitter
import com.baseball.olap.dimensions.Inning
import com.baseball.olap.dimensions.PitchType
import com.baseball.olap.dimensions.Pitcher
import com.baseball.olap.dimensions.Weather
import com.baseball.olap.facts.GameFact
import com.baseball.olap.facts.PitchFact
import com.baseball.olap.facts.RunnerFact
import com.mdimension.jchronic.Chronic

import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

import static groovyx.gpars.GParsPool.withPool


class EtlService {


    PitchService pitchService
    GameService gameService
    ParkFactorService parkFactorService
    StatService statService
    PitcherService pitcherService


    private static final long NO_PLAYER_ID = 3532;


    public void processSeason(int year) {

        println "***************************************************************"
        println "***************************************************************"
        println "**************** ETLing SEASON " + year + " *******************"
        println "***************************************************************"
        println "***************************************************************"


        Date startDate = Chronic.parse('march 10 ' + year)?.beginCalendar?.time
        Date endDate = Chronic.parse('november 23 ' + year)?.beginCalendar?.time

        if (endDate.after(new Date())) endDate = new Date()


        List<Date> dates = new ArrayList<>()
        startDate.upto(endDate) {
            dates.add(it)
        }


        dates.each { Date date ->
            processDate(date)
        }



        println "***************************************************************"
        println "***************************************************************"
        println "************ FINISHED ETLING SEASON " + year + " **************"
        println "***************************************************************"
        println "***************************************************************"


    }


    public void processDate(Date date) {

        List<Game> games = gameService.getGamesForDate(date)

        games?.each { game ->
            processGame(game)
        }

    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processGame(Game theGame) {

        println "ETL Processsing game: " + theGame.textDescription


        //Remove all pitch facts about game
        deletePitchFactsForGame(theGame)

        //Remove all runner facts about the game
        deleteRunnerFactsForGame(theGame)


        //Remove game fact
        GameFact gameFact = GameFact.findByGame(theGame)
        gameFact?.delete(flush: true)  //////***************        //need to get gamefact*******SDF*S*DF*SDF*SDF**////





        GameTime gameTime = getGameTime(theGame)

        Weather weather = getWeatherForGame(theGame)

        BallPark ballPark = getBallparkForGame(theGame)

        Team homeTeam = theGame.homeTeam
        Team awayTeam = theGame.awayTeam


        println "pitchers start"
        Pitcher homeProbablePitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.homeProbablePitcher))
        Pitcher awayProbablePitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.awayProbablePitcher))
        Pitcher homeStartingPitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.homeStartingPitcher))
        Pitcher awayStartingPitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.awayStartingPitcher))
        Pitcher winningPitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.winningPitcher))
        Pitcher losingPitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.losingPitcher))
        Pitcher savePitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, theGame.savePitcher))
        println "pitchers end"

        //println "get pitches"
        List<Pitch> pitches = pitchService.getPitchesForGame(theGame)
//        println "end get pitches"


        pitches?.each { pitch ->

            PitchFact pf = new PitchFact(game: theGame, atBat: AtBat.get(pitch.atBat.id))

            pf.gameTime = gameTime
            pf.weather = weather

            pf.ballPark = ballPark

            pf.homeTeam = homeTeam
            pf.awayTeam = awayTeam



            pf.homeProbablePitcher = homeProbablePitcher
            pf.awayProbablePitcher = awayProbablePitcher
            pf.homeStartingPitcher = homeStartingPitcher
            pf.awayStartingPitcher = awayStartingPitcher
            pf.winningPitcher = winningPitcher
            pf.losingPitcher = losingPitcher
            pf.savePitcher = savePitcher

            //println "get hitter and pitcher"
            pf.hitter = getHitter(HitterGameLog.findByHitterAndGame(pitch.atBat.hitter, theGame), pitch.atBat.hitterHandedness)
            pf.pitcher = getPitcher(PitcherGameLog.findByGameAndPitcher(theGame, pitch.atBat.pitcher))
           // println "get hitter and pitcher end"


            pf.inning = getInning(pitch)
            pf.pitchType = getPitchType(pitch)


            if (pitch.on1b) {
                pf.on1b = getHitter(HitterGameLog.findByHitterAndGame(Player.get(pitch.on1b), theGame))
            } else {
                pf.on1b = getNoHitter()
            }

            if (pitch.on2b) {
                pf.on2b = getHitter(HitterGameLog.findByHitterAndGame(Player.get(pitch.on2b), theGame))
            } else {
                pf.on2b = getNoHitter()
            }

            if (pitch.on3b) {
                pf.on3b = getHitter(HitterGameLog.findByHitterAndGame(Player.get(pitch.on3b), theGame))
            } else {
                pf.on3b = getNoHitter()
            }



            pf.sequence = pitch.sequence ?: 0
            pf.sportsvisionId = pitch.sportsvisionId ?: "NA"
            pf.playGuid = pitch.playGuid ?: "NA"

            pf.startSpeed = pitch.startSpeed ?: 0
            pf.endSpeed = pitch.endSpeed ?: 0
            pf.type = pitch.type ?: "NA"




            pf.tfs = pitch.tfs

            pf.y = pitch.y
            pf.eventNum = pitch.eventNum ?: "NA"


            pf.szTop = pitch.szTop ?: 0
            pf.szBot = pitch.szBot ?: 0
            pf.pfxX = pitch.pfxX ?: 0
            pf.pfxZ = pitch.pfxZ ?: 0
            pf.px = pitch.px ?: 0
            pf.pz = pitch.pz ?: 0
            pf.x0 = pitch.x0 ?: 0
            pf.y0 = pitch.y0 ?: 0
            pf.z0 = pitch.z0 ?: 0
            pf.vx0 = pitch.vx0 ?: 0
            pf.vy0 = pitch.vy0 ?: 0
            pf.vz0 = pitch.vz0 ?: 0
            pf.ax = pitch.ax ?: 0
            pf.ay = pitch.ay ?: 0
            pf.az = pitch.az ?: 0
            pf.breakY = pitch.breakY ?: 0
            pf.breakAngle = pitch.breakAngle ?: 0
            pf.breakLength = pitch.breakLength ?: 0
            pf.typeConfidence = pitch.typeConfidence ?: 0
            pf.zone = pitch.zone ?: 0
            pf.nasty = pitch.nasty ?: 0
            pf.spinDir = pitch.spinDir ?: 0
            pf.spinRate = pitch.spinRate ?: 0




            //Measures
            List<Pitch> atbatPitches = pitches?.findAll {   it.atBat.id == pitch.atBat.id   }
            Pitch lastPitchOfAtbat = atbatPitches.max { it.sequence }

            ParkFactorValues pfv = parkFactorService.getParkFactorValuesForHandednessAndTeam(pf.homeTeam, pitch.atBat.hitterHandedness, pitch.atBat.pitcherHandedness )

            if (lastPitchOfAtbat.id == pitch.id) {

                if (pf.atBat.isHit()) {
                    pf.hits++
                }

                if (pf.atBat.isSingle()) {
                    pf.singles++
                    pf.paSingles = parkFactorService.adjustToPark(1, pfv.singles)
                    pf.paHits = pf.paSingles
                }

                if (pf.atBat.isDouble()) {
                    pf.doubles++
                    pf.padoubles = parkFactorService.adjustToPark(1, pfv.doubles)
                    pf.paHits = pf.padoubles
                }

                if (pf.atBat.isTriple()) {
                    pf.triples++
                    pf.paTriples = parkFactorService.adjustToPark(1, pfv.triples)
                    pf.paHits = pf.triples
                }

                if (pf.atBat.isHr()) {
                    pf.homeRuns++
                    pf.paHomeRuns = parkFactorService.adjustToPark(1, pfv.homeRuns)
                    pf.paHits = pf.paHomeRuns
                }

                if (pf.atBat.isBB()) {
                    pf.bb++
                }

                if (pf.atBat.isHpb()) {
                    pf.hbp++
                }

                if (pf.atBat.isStrikeout()) {
                    pf.so++
                }



                if (pf.atBat.isSacFly()) pf.sacFlys++
                if (pf.atBat.isOut()) pf.outs++

                if (pf.atBat.isGroundOut()) pf.groundOuts++
                if (pf.atBat.isFlyOut()) pf.flyOuts++

                if (pf.atBat.isLineOut()) pf.lineOuts++
                if (pf.atBat.isGroundBall()) pf.groundBalls++
                if (pf.atBat.isLineDrive()) pf.lineDrives++
                if (pf.atBat.isFlyBall()) pf.flyBalls++

                pf.tb = statService.calculateTotalBases(pf.singles, pf.doubles, pf.triples, pf.homeRuns)
                pf.paTb = statService.calculateTotalBases(pf.paSingles, pf.padoubles, pf.paTriples, pf.paHomeRuns)

                pf.rbi = pf.atBat.rbi
                pf.paRbi = parkFactorService.adjustToPark(pf.rbi, pfv.rbi)


                pf.earnedRuns = pf.atBat.earnedRuns

                pf.strikes = pf.strikes
                pf.balls = pf.balls


                if (!pf.save(flush:true)) {
                    println "*****************ERROR SAVING PITCH FACT **************************"
                } else {
                    println "SUCCESS"
                }

            }


//            if (pf.atBat.isHit()) pf.sb
//            if (pf.atBat.isHit()) pf.cs
//            if (pf.atBat) pf.runs
//            pf.paRuns
//            pf.lob
//            pf.sacBunts
//            pf.gidp
//            pf.po
//            pf.assists
//            pf.e


        }



    }



    //@Transactional(propagation =  Propagation.REQUIRES_NEW)
    public void processDimensionsForSeason(int year) {


        Date startDate = Chronic.parse('march 10 ' + year)?.beginCalendar?.time
        Date endDate = Chronic.parse('november 23 ' + year)?.beginCalendar?.time

        if (endDate.after(new Date())) endDate = new Date()


        List<Date> dates = new ArrayList<>()
        startDate.upto(endDate) {
            dates.add(it)
        }


        dates.each { Date date ->
            processDimensionsDate(date)
        }



    }



    public void processDimensionsDate(Date date) {

        List<Game> games = gameService.getGamesForDate(date)

        games?.each { game ->
            processDimensionsForGame(game)
        }

    }


    public void processDimensionsForGame(Game theGame) {

        println "****Processing dimensions for game: " + theGame.textDescription

        GameTime gameTime = getGameTime(theGame)

        Weather weather = getWeatherForGame(theGame)

        BallPark ballPark = getBallparkForGame(theGame)




        if (theGame.homeProbablePitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.homeProbablePitcher))
        }
        if (theGame.awayProbablePitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.awayProbablePitcher))
        }
        if (theGame.homeStartingPitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.homeStartingPitcher))
        }
        if (theGame.awayStartingPitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.awayStartingPitcher))
        }
        if (theGame.winningPitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.winningPitcher))
        }
        if (theGame.losingPitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.losingPitcher))
        }
        if (theGame.savePitcher) {
            getPitcher(pitcherService.getPitcherGameLog(theGame, theGame.savePitcher))
        }



        List<HitterGameLog> hitterGameLogs = HitterGameLog.findAllByGame(theGame)

        hitterGameLogs?.each { hitterGameLog ->


            switch(hitterGameLog.hitter.bats) {
                case "L":
                case "R":
                    getHitter(hitterGameLog, hitterGameLog.hitter.bats)
                case "S":
                    getHitter(hitterGameLog, "R")
                    getHitter(hitterGameLog, "L")

            }


        }




    }


    public void processGameTimes() {



    }












    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deletePitchFactsForGame(Game theGame) {

        boolean success = false

        while (!success) {
            try {

                List<PitchFact> pitchFacts = PitchFact.withCriteria {
                    createAlias("game", "g")
                    eq("g.id", theGame.id)
                }

                pitchFacts*.delete()


                success = true
            } catch (Exception ex) {
                println "---------------PITCHFACT DELETE DEADLOCK. WILL RETRY--------------------"
            }
        }


    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteRunnerFactsForGame(Game theGame) {

        boolean success = false

        while (!success) {
            try {

                List<RunnerFact> runnerFacts = RunnerFact.withCriteria {
                    createAlias("game", "g")
                    eq("g.id", theGame.id)
                }

                runnerFacts*.delete()


                success = true
            } catch (Exception ex) {
                println "---------------PITCHFACT DELETE DEADLOCK. WILL RETRY--------------------"
            }
        }


    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PitchType getPitchType(Pitch pitch) {
        PitchType pt = PitchType.findOrCreateByType(pitch.type)
        pt.save(flush:true)
        return pt
    }


    private Inning getInning(Pitch pitch) {
        return getInning(pitch.atBat.inningNum, pitch.atBat.inningTop)
    }

    private Inning getInning(Runner runner) {
        return getInning(runner.atBat.inningNum, runner.atBat.inningTop)
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Inning getInning(int inningNum, boolean inningTop ) {
        Inning i = Inning.findOrCreateByInningNumAndInningTop(inningNum, inningTop)
        i.save(flush:true)
        return i
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private GameTime getGameTime(Game theGame) {

        Date gameDate = theGame.startDate

        int day = theGame.startDate[Calendar.DAY_OF_MONTH]
        int month = theGame.startDate[Calendar.MONTH]
        int year = theGame.startDate[Calendar.YEAR]

        String dayOfWeek = theGame.startDate.format("EEEE")
        String monthName = theGame.startDate.format("MMMM")
        GameTime.HalfName half = theGame.isPreAllstar() ? GameTime.HalfName.FIRST : GameTime.HalfName.SECOND
        GameTime.GameStart gameStart = gameService.getGameStart(theGame)



        return GameTime.findOrCreateWhere(
                gameDate: gameDate,
                day: day,
                month: month,
                year: year,

                dayOfWeek:dayOfWeek,
                monthName: monthName,
                half: half,
                gameStart: gameStart
        )



    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Weather getWeatherForGame(Game theGame) {

        String w = theGame.weather ?: 'NA'
        String weatherStatus = theGame.weatherStatus ?: 'NA'
        String windDirection = theGame.windDirection ?: 'NA'
        String windSpeed = theGame.windSpeed ?: 'NA'
        double temp = theGame.temp ?: 999


        return Weather.findOrCreateWhere(
                weather: w,
                weatherStatus: weatherStatus,
                windDirection: windDirection,
                windSpeed: windSpeed,
                temp: temp
        )

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private BallPark getBallparkForGame(Game theGame) {


        return BallPark.findOrCreateWhere(
                name: theGame.homeTeam.stadiumName,
                teamAbbrev: theGame.homeTeam.abbrev
        )

    }


    private Hitter getHitter(HitterGameLog hitterGameLog, String handedness) {
        if (!hitterGameLog) return getNoHitter()
        getHitter(hitterGameLog.hitter, handedness , hitterGameLog.homeAway, hitterGameLog.battingOrder, hitterGameLog.gameOutcome as Hitter.GameOutcomeValue)
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Hitter getHitter(Player theHitter, String hitterHandedness, Hitter.HomeAwayValue homeAwayValue, Integer battingOrder, Hitter.GameOutcomeValue gameOutcome) {

        if (!theHitter) return getNoHitter()

        //theHitter.refresh()

        Hitter.HandValue hv = Hitter.HandValue.valueOf(hitterHandedness)

        return Hitter.findOrCreateWhere(
                hitter: theHitter,
                hand: hv,
                homeAway: homeAwayValue,
                battingOrder: battingOrder ?: 0,
                gameOutcome: gameOutcome
        )

    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Hitter getNoHitter() {

        List<Hitter> results = Hitter.withCriteria {
            hitter {
                eq('id', NO_PLAYER_ID)
            }
        }

        if (results?.size() > 0) return results.get(0)



        Hitter savedHitter = new Hitter(
                hitter: Player.get(NO_PLAYER_ID),
                hand: Hitter.HandValue.NA,
                homeAway: Hitter.HomeAwayValue.NA,
                battingOrder: 0,
                gameOutcome: Hitter.GameOutcomeValue.W
        )

        println "Saving hitter: " + savedHitter.hitter.fullName
        if (!savedHitter.save(flush:true)) {
            println "************ERROR SAVING NO HITTER DIMENSION******************"
        }

        return savedHitter


    }





    private Pitcher getPitcher(PitcherGameLog pitcherGameLog) {
        if (!pitcherGameLog) return getNoPitcher()
        return getPitcher(pitcherGameLog.pitcher, pitcherGameLog.homeAway, pitcherGameLog.gameOutcome as Pitcher.GameOutcomeValue)
    }






    @Transactional(propagation = Propagation.REQUIRED)
    private Pitcher getPitcher(Player thePitcher, Pitcher.HomeAwayValue homeAwayValue, Pitcher.GameOutcomeValue gameOutcome) {

        if (!thePitcher) return getNoPitcher()

        thePitcher.refresh()

        Pitcher.HandValue hv = Pitcher.HandValue.valueOf(thePitcher.pitches)


        return Pitcher.findOrCreateWhere(
                pitcher: thePitcher,
                hand: hv,
                homeAway: homeAwayValue,
                gameOutcome: gameOutcome
        )

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private Pitcher getNoPitcher() {

        List<Pitcher> results = Pitcher.withCriteria {
            pitcher {
                eq('id', NO_PLAYER_ID)
            }
        }

        Pitcher savedPitcher = null
        if (results?.size() > 0) savedPitcher = results.get(0)

        if (savedPitcher?.id) return savedPitcher

        savedPitcher = new Pitcher(
                pitcher: Player.get(NO_PLAYER_ID),
                hand: Pitcher.HandValue.NA,
                homeAway: Pitcher.HomeAwayValue.NA,
                gameOutcome: Pitcher.GameOutcomeValue.NA
        )

        if (!savedPitcher.save(flush:true)) {
            println "************ERROR SAVING NO PITCHER DIMENSION******************"
        }

        return savedPitcher


    }









}
