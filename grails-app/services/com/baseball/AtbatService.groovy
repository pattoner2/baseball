package com.baseball

import com.baseball.AtBat
import com.baseball.HitterGameLog
import com.baseball.ParkFactor
import com.baseball.ParkFactorValues
import com.baseball.PitcherGameLog
import com.baseball.PitcherSeason
import com.baseball.Player
import com.baseball.domain.PitcherLog
import grails.transaction.Transactional


class AtbatService {



    ParkFactorService parkFactorService

    public List<AtBat> getAtbats(PitcherSeason pitcherSeason) {

        AtBat.withCriteria {
            createAlias("game", "g")
            pitcher {
                eq('id', pitcherSeason.pitcher.id)
            }
            eq('g.status', "Final")
            eq('year', pitcherSeason.year)
        }

    }


    public List<AtBat> getAtBats(Player theHitter, List<HitterGameLog> gameLogs) {
        return AtBat.withCriteria {

            hitter {
                eq('id', theHitter.id)
            }

            game {
                'in'('id', gameLogs.collect { it.game?.id })
            }

        }
    }






}
