package com.baseball

import com.baseball.Game
import com.baseball.HitterGameLog
import com.baseball.HitterSeason
import com.baseball.PitcherGameLog
import com.baseball.PitcherSeason
import com.baseball.Player
import com.baseball.Team
import com.baseball.fanduel.FanduelPlayer
import com.baseball.gameday.GamedayPlayer
import com.baseball.gameday.LinescorePitcher
import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

@Transactional
class PlayerService {

    HitterService hitterService
    def grailsApplication



    public List<Player> findPlayer(String s) {


        return Player.withCriteria {
            or {
                ilike('firstName', '%' + s + '%')
                ilike('lastName', '%' + s + '%')
            }
            maxResults(20)

        }


    }


    public Player findOrCreatePitcher(LinescorePitcher pitcher, Team team) {

        Player player = Player.findByGamedayId(pitcher.playerId)

        if (player) return player

        player =  findPlayer(pitcher.firstName, pitcher.lastName, team)

        if (player) return player

        if (pitcher.playerId) {
            player = new Player()
            player.firstName = pitcher.firstName
            player.lastName = pitcher.lastName
            player.currentTeam = team
            player.pitches = pitcher.throwingHand
            player.position = "P"
            player.currentPosition = "P"
            player.gamedayId = pitcher.playerId

            if (player.save(flush: true)) {
                println "Adding pitcher: (" + player.gamedayId + ") " + player.firstName + " " + player.lastName
            } else {
                println "******* PROBLEM ADDING PITCHER ******* " + player.gamedayId
            }

        }


    }





    @Transactional(propagation = Propagation.REQUIRED)
    public Player findPlayer(String firstName, String lastName, Team team) {

        Player player = Player.findByLastNameAndFirstNameAndCurrentTeam(lastName, firstName, team)


        if (!player) {
            def results = Player.withCriteria {
                eq('firstName', firstName)
                like('lastName', lastName + '%')

                if (team) {
                    currentTeam {
                        eq('id', team.id)
                    }
                }
            }

            if (results?.size() > 0) {
                player = results?.get(0)
            }

        }


        if (!player && lastName.toUpperCase().endsWith(" JR")  || lastName.toUpperCase().endsWith(" SR") || lastName.toUpperCase().endsWith(" JR.")  || lastName.toUpperCase().endsWith(" SR.")) {
            player = findJrOrSr(firstName, lastName, team)
        }

        if (!player && firstName.contains(".")) {

            def results = Player.withCriteria {
                like('firstName', firstName?.replaceAll("\\.", "") )
                eq('lastName', lastName)
                currentTeam {
                    eq('id', team.id)
                }
            }

            if (results?.size() > 0) {
                player = results?.get(0)
            }

        }



        if (!player) {
            player = Player.findByLastNameAndFirstName(lastName, firstName)
        }

        if (!player) {
            player = Player.findByLastNameAndCurrentTeam(lastName, team)
        }


        return player
    }




    public Player findJrOrSr(String firstName, String lastName, team) {


        if (lastName.toUpperCase().endsWith(" JR") || lastName.toUpperCase().endsWith(" SR")) {
            lastName = lastName.reverse().drop(3).reverse()
        }

        if (lastName.toUpperCase().endsWith(" JR.") || lastName.toUpperCase().endsWith(" SR.")) {
            lastName = lastName.reverse().drop(4).reverse()
        }


        def results = Player.withCriteria {
            eq('firstName', firstName)
            like('lastName', lastName + '%')
            currentTeam {
                eq('id', team.id)
            }
        }

        if (results?.size() > 0) {
            return results?.get(0)
        }

    }















    @Transactional(propagation = Propagation.REQUIRED)
    public Player findPlayer(String name, Team theTeam) {

        Player player = null

        name = name.replace("'", "\\'")

        String query = "concat_ws(' ',first_name,last_name) = '${name}'"

        def exactMatchResults = Player.withCriteria {
            sqlRestriction(query)

            currentTeam {
                eq('id', theTeam.id)
            }
        }


        if (exactMatchResults?.size() > 0) {
            player = exactMatchResults?.get(0)
        }




        if (!player) {


            def results = Player.withCriteria {
                sqlRestriction("concat_ws(' ',first_name,last_name) like '%${name}%'")

                currentTeam {
                    eq('id', theTeam.id)
                }
            }

            if (results?.size() > 0) {
                player = results?.get(0)
            }

        }




        if (!player && name.toUpperCase().endsWith(" JR")  || name.toUpperCase().endsWith(" SR") || name.toUpperCase().endsWith(" JR.")  || name.toUpperCase().endsWith(" SR.")) {


            if (name.toUpperCase().endsWith(" JR") || name.toUpperCase().endsWith(" SR")) {
                name = name.reverse().drop(3).reverse()
            }

            if (name.toUpperCase().endsWith(" JR.") || name.toUpperCase().endsWith(" SR.")) {
                name = name.reverse().drop(4).reverse()
            }


            def results = Player.withCriteria {
                sqlRestriction("concat_ws(' ',first_name,last_name) like '%${name}%'")

                currentTeam {
                    eq('id', theTeam.id)
                }
            }

            if (results?.size() > 0) {
                return results?.get(0)
            }
        }







        if (!player && name.contains(".")) {

            name = name.replaceAll("\\.", "")

            def results = Player.withCriteria {
                sqlRestriction("concat_ws(' ',first_name,last_name) like '%${name}%'")

                currentTeam {
                    eq('id', theTeam.id)
                }
            }

            if (results?.size() > 0) {
                player = results?.get(0)
            }

        }


        String[] names = name.split(" ")

        String lastName = names[names.size() -1]

        if (!player) {
            player = Player.findByLastNameLikeAndCurrentTeam(lastName + '%', theTeam)
        }





        return player
    }







    public String getPermaLink(Player player) {
        def linkGen = grailsApplication.getMainContext().getBean("grailsLinkGenerator")
        return linkGen.link(uri:"/player/show/" + player.id, absolute: true)

    }



}
