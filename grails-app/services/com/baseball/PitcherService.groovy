package com.baseball

import com.baseball.AtBat
import com.baseball.Game
import com.baseball.HitterGameLog
import com.baseball.HitterSeason
import com.baseball.HitterSeasonSplit
import com.baseball.ParkFactorValues
import com.baseball.PitcherCareer
import com.baseball.PitcherCareerSplit
import com.baseball.PitcherGameLog
import com.baseball.PitcherLogsAdvancedStatBucket
import com.baseball.PitcherSeason
import com.baseball.PitcherSeasonSplit
import com.baseball.Player
import com.baseball.domain.PitcherLog
import com.baseball.gameday.NumberUtil
import com.mdimension.jchronic.Chronic
import grails.transaction.Transactional
import org.hibernate.Criteria
import org.springframework.transaction.annotation.Propagation


class PitcherService {



    StatService statService
    HitterService hitterService
    GameService gameService
    AdvancedStatService advancedStatService
    AtbatService atbatService
    ParkFactorService parkFactorService



    public void processAdvancedStatsForSeason(int year) {

        println "***************************************************************"
        println "***************************************************************"
        println "************ PROCESS ADVANCED PITCHING STATS " + year + " ******"
        println "***************************************************************"
        println "***************************************************************"


        Date startDate = Chronic.parse('march 10 ' + year)?.beginCalendar?.time
        Date endDate = Chronic.parse('november 23 ' + year)?.beginCalendar?.time

        if (endDate.after(new Date())) endDate = new Date()


        List<Date> dates = new ArrayList<>()
        startDate.upto(endDate) {
            dates.add(it)
        }


        dates.each { Date date ->
            updateAdvancedStatsForDate(date)
        }


        println "***************************************************************"
        println "***************************************************************"
        println "************ FINISHED PITCHING HITTING STATS " + year + " *****"
        println "***************************************************************"
        println "***************************************************************"


    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAdvancedStatsForDate(Date date) {

        List<PitcherGameLog> pitcherGameLogs = gameService.getProbablePitcherGameLogs(date)
        pitcherGameLogs?.each { pitcherGameLog ->

            PitcherLogsAdvancedStatBucket p = new PitcherLogsAdvancedStatBucket(
                    player: pitcherGameLog.pitcher,
                    pitcherGameLog: pitcherGameLog,
                    pitcherSeason: getPitcherSeason(pitcherGameLog.pitcher, pitcherGameLog.year),
                    pitcherSplits: getPitcherSeasonSplits(pitcherGameLog.pitcher, pitcherGameLog.year)
            )


            if (pitcherGameLog.isHome()) {
                p.opposingStartingLineup = gameService.getStartingLineup(pitcherGameLog.game, pitcherGameLog.game.awayTeam)
            } else {
                p.opposingStartingLineup = gameService.getStartingLineup(pitcherGameLog.game, pitcherGameLog.game.homeTeam)
            }

            Date startDate = new Date(p.pitcherGameLog.game.startDate.time)
            p.hitterGameLogs = hitterService.getGameLogsForDate(p.opposingStartingLineup, startDate)


            p.startingLineupGameLogs = hitterService.getGameLogsForSeason(p.opposingStartingLineup, pitcherGameLog.year)
            p.startingLineupAtbats = hitterService.getAtBatsForSeason(p.opposingStartingLineup, pitcherGameLog.year)

            p.last5Starts = getRecentStarts(p.pitcherSeason, 5)

            advancedStatService.updatePitcherGameLogAdvancedStats(p)


            if (p.pitcherGameLog.save(flush: true)) {
                println "Updating advanced stats for pitcherGameLog: " + p.pitcherGameLog.game.startDate + " - " + p.pitcherGameLog.pitcher.fullName
            } else {
                println "******* PROBLEM SAVING PITCHER GAME LOG ***********"
            }


        }
    }




    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePitcherSeasonStats(Player thePitcher, Integer year) {

        if (thePitcher == null) return

        thePitcher = Player.get(thePitcher.id)

        println String.format('Updating season stats for %1s (%2s)', thePitcher.firstName + " " + thePitcher.lastName, year)

        List<PitcherGameLog> gameLogList = PitcherGameLog.withCriteria {

            createAlias("game", "g")
            pitcher {
                eq('id', thePitcher.id)
            }

            eq('year', year)
            eq('gamedayPopulated', true)

            eq('g.status', "Final")

        }

        if (gameLogList?.size() == 0) return


        PitcherSeason pitcherSeason = PitcherSeason.findByPitcherAndYear(thePitcher, year)

        if (!pitcherSeason) {
            pitcherSeason = new PitcherSeason(pitcher: thePitcher, year: year)
        }

        List<AtBat> atBats = atbatService.getAtbats(pitcherSeason)


        updatePitcherLog(pitcherSeason, gameLogList, atBats)


        if (!pitcherSeason.save(flush: true)) {
            println "************** ERROR SAVING ADVANCED STATS FOR PITCHER SEASON **************"
        }




        List<PitcherSeasonSplit> seasonSplits = generateSplits(pitcherSeason, atBats, gameLogList)

        seasonSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "***************** ERROR SAVING PITCHER SEASON SPLIT**************"
            }
        }


        List<PitcherSeasonSplit> recentSplits = generateRecentSplits(thePitcher)
        recentSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "***************** ERROR SAVING RECENT PITCHER SEASON SPLIT**************"
            }
        }

        updatePitcherCareer(thePitcher)

    }



    public updatePitcherLog(PitcherLog pitcherLog, List<PitcherLog> pitcherLogs,  List<AtBat> atBats) {

        pitcherLog.clearAll()


        atBats?.each {

            pitcherLog.battersFace                     = statService.incrementField(pitcherLog.battersFace)

            if (it.isOut()) pitcherLog.outs            = statService.incrementField(pitcherLog.outs)
            if (it.earnedRuns) pitcherLog.er           = statService.incrementBy(pitcherLog.er, it.earnedRuns)
            if (it?.isStrikeout()) pitcherLog.so       = statService.incrementField(pitcherLog.so)


            if (it?.isHit()) pitcherLog.h              = statService.incrementField(pitcherLog.h)
            if (it?.isBB()) pitcherLog.bb              = statService.incrementField(pitcherLog.bb)
            if (it?.isSingle()) pitcherLog.singles     = statService.incrementField(pitcherLog.singles)
            if (it?.isDouble()) pitcherLog.doubles     = statService.incrementField(pitcherLog.doubles)
            if (it?.isTriple()) pitcherLog.triples     = statService.incrementField(pitcherLog.triples)




            if (it?.isHr()) pitcherLog.hr               = statService.incrementField(pitcherLog.hr)


            if (it?.isFlyOut()) pitcherLog.flyOuts      = statService.incrementField(pitcherLog.flyOuts)
            if (it?.isFlyBall()) pitcherLog.flyBalls    = statService.incrementField(pitcherLog.flyBalls)
            if (it?.isGroundOut()) pitcherLog.groundOuts   = statService.incrementField(pitcherLog.groundOuts)
            if (it?.isGroundBall()) pitcherLog.groundBalls = statService.incrementField(pitcherLog.groundBalls)
            if (it?.isLineDrive()) pitcherLog.lineDrives   = statService.incrementField(pitcherLog.lineDrives)
            if (it?.isLineOut()) pitcherLog.lineOuts       = statService.incrementField(pitcherLog.lineOuts)
            if (it?.isHpb()) pitcherLog.hbp                = statService.incrementField(pitcherLog.hbp)



            if (it?.pc) pitcherLog.pc                      = statService.incrementBy(pitcherLog.pc, it.pc)
            if (it?.pitchesStrikes) pitcherLog.strikes     = statService.incrementBy(pitcherLog.strikes, it.pitchesStrikes)
            if (it?.pitchesBalls) pitcherLog.balls         = statService.incrementBy(pitcherLog.balls, it.pitchesBalls)


           // parkFactorService.updateParkAdjustedStats(pitcherLog, it)

        }



        pitcherLogs?.each {

            if (it?.games) pitcherLog.games         = statService.incrementBy(pitcherLog.games, it.games)
            if (it?.starts) pitcherLog.starts       = statService.incrementBy(pitcherLog.starts, it.starts)
            if (it?.wins) pitcherLog.wins           = statService.incrementBy(pitcherLog.wins, it.wins)
            if (it?.losses) pitcherLog.losses       = statService.incrementBy(pitcherLog.losses, it.losses)
            if (it?.saves) pitcherLog.saves         = statService.incrementBy(pitcherLog.saves, it.saves)
            if (it?.bs) pitcherLog.bs               = statService.incrementBy(pitcherLog.bs, it.bs)

            if (it?.sho) pitcherLog.sho             = statService.incrementBy(pitcherLog.sho, it.sho)
            if (it?.cg) pitcherLog.cg               = statService.incrementBy(pitcherLog.cg, it.cg)

        }





        if (pitcherLog.outs > 0) {
            pitcherLog.era = (  (pitcherLog?.er ?:0 ) /  (pitcherLog?.outs / 3)  ) * 9
        } else {
            pitcherLog.era = 0
        }


        pitcherLog.opponentObp = statService.calculateObp(atBats)
        pitcherLog.opponentSlg = statService.calculateSlg(atBats)

        if (pitcherLog?.opponentOps && pitcherLog?.opponentSlg) {
            pitcherLog.opponentOps = pitcherLog.opponentOps + pitcherLog.opponentSlg
        }

        pitcherLog.opponentIso = statService.calculateIso(atBats)
        pitcherLog.opponentWOba = statService.calculateWOba(atBats)
        pitcherLog.opponentWFda = statService.calculateWfda(atBats)


        pitcherLog.soPercent = statService.calculateSoPercent(atBats)
        pitcherLog.bbPercent = statService.calculateBBPercent(atBats)


        pitcherLog.fanduelPoints = statService.calculateFanduelPoints(pitcherLogs)
    }














    public List<PitcherSeasonSplit> generateSplits(PitcherSeason pitcherSeason, List<AtBat> atBats, List<PitcherGameLog> gameLogs) {

        List<HitterGameLog> gameLogsH = new ArrayList<>()
        List<HitterGameLog> gameLogsA = new ArrayList<>()
        List<AtBat> atBatsL = new ArrayList<>()
        List<AtBat> atBatsR = new ArrayList<>()
        List<AtBat> atBatsH = new ArrayList<>()
        List<AtBat> atBatsA = new ArrayList<>()


        gameLogs.findAll {
            it.opponent == it.game.awayTeam ? gameLogsH.add(it) : gameLogsA.add(it)
        }

        atBats.each {
            "L".equals(it.hitterHandedness) ? atBatsL.add(it) : atBatsR.add(it)
            "H".equals(it.pitcherHomeAway) ? atBatsH.add(it) : atBatsA.add(it)
        }


        List<PitcherSeasonSplit> results = new ArrayList<>()


        PitcherSeasonSplit lPitcherSeasonSplit = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "L")
        updatePitcherLog(lPitcherSeasonSplit, null, atBatsL)

        PitcherSeasonSplit rPitcherSeasonSplit = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "R")
        updatePitcherLog(rPitcherSeasonSplit, null, atBatsR)


        PitcherSeasonSplit hPitcherSeasonSplit = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "H")
        updatePitcherLog(hPitcherSeasonSplit, gameLogsH, atBatsH)


        PitcherSeasonSplit aPitcherSeasonSplit = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "A")
        updatePitcherLog(aPitcherSeasonSplit, gameLogsA, atBatsA)


        results.add(lPitcherSeasonSplit)
        results.add(rPitcherSeasonSplit)
        results.add(hPitcherSeasonSplit)
        results.add(aPitcherSeasonSplit)

        return results

    }


    public List<PitcherCareerSplit> generateSplits(PitcherCareer pitcherCareer, List<AtBat> atBats, List<PitcherGameLog> gameLogs) {

        List<HitterGameLog> gameLogsH = new ArrayList<>()
        List<HitterGameLog> gameLogsA = new ArrayList<>()
        List<AtBat> atBatsL = new ArrayList<>()
        List<AtBat> atBatsR = new ArrayList<>()
        List<AtBat> atBatsH = new ArrayList<>()
        List<AtBat> atBatsA = new ArrayList<>()


        gameLogs.findAll {
            it.opponent == it.game.awayTeam ? gameLogsH.add(it) : gameLogsA.add(it)
        }

        atBats.each {
            "L".equals(it.hitterHandedness) ? atBatsL.add(it) : atBatsR.add(it)
            "H".equals(it.pitcherHomeAway) ? atBatsH.add(it) : atBatsA.add(it)
        }

        List<PitcherCareerSplit> results = new ArrayList<>()


        PitcherCareerSplit pitcherCareerSplitL = PitcherCareerSplit.findOrCreateByPitcherAndSplit(pitcherCareer.pitcher, "L")
        updatePitcherLog(pitcherCareerSplitL, null, atBatsL)

        PitcherCareerSplit pitcherCareerSplitR = PitcherCareerSplit.findOrCreateByPitcherAndSplit(pitcherCareer.pitcher, "R")
        updatePitcherLog(pitcherCareerSplitR, null, atBatsR)

        PitcherCareerSplit pitcherCareerSplitH = PitcherCareerSplit.findOrCreateByPitcherAndSplit(pitcherCareer.pitcher, "H")
        updatePitcherLog(pitcherCareerSplitH, gameLogsH, atBatsH)


        PitcherCareerSplit pitcherCareerSplitA = PitcherCareerSplit.findOrCreateByPitcherAndSplit(pitcherCareer.pitcher, "A")
        updatePitcherLog(pitcherCareerSplitA, gameLogsA, atBatsA)


        results.add(pitcherCareerSplitA)
        results.add(pitcherCareerSplitL)
        results.add(pitcherCareerSplitR)
        results.add(pitcherCareerSplitH)


        return results
    }



    public List<PitcherSeasonSplit> generateRecentSplits(Player player) {

        List<HitterGameLog> gameLogs7 = getRecentGameLogs(player, 7)
        List<HitterGameLog> gameLogs15 = getRecentGameLogs(player, 15)
        List<HitterGameLog> gameLogs30 = getRecentGameLogs(player, 30)

        Integer mostRecentSeason = gameLogs7.collect { it.year ? it.year : 0}.max()
        if (!mostRecentSeason) return

        PitcherSeason pitcherSeason = PitcherSeason.findByPitcherAndYear(player, mostRecentSeason)
        if (!pitcherSeason) return


        List<PitcherSeasonSplit> result = new ArrayList<>()

        if (gameLogs7?.size() > 0) {
            List<AtBat> atBats7 = getRecentAtBats(pitcherSeason, gameLogs7)

            PitcherSeasonSplit pitcherSeasonSplit7 = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "7")
            updatePitcherLog(pitcherSeasonSplit7, gameLogs7, atBats7)
            result.add(pitcherSeasonSplit7)
        }
        if (gameLogs15?.size() > 0) {
            List<AtBat> atBats15 = getRecentAtBats(pitcherSeason, gameLogs15)

            PitcherSeasonSplit pitcherSeasonSplit15 = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "15")
            updatePitcherLog(pitcherSeasonSplit15, gameLogs15, atBats15)
            result.add(pitcherSeasonSplit15)
        }
        if (gameLogs30?.size() > 0) {
            List<AtBat> atBats30 = getRecentAtBats(pitcherSeason, gameLogs30)

            PitcherSeasonSplit pitcherSeasonSplit30 = PitcherSeasonSplit.findOrCreateByPitcherAndYearAndSplit(pitcherSeason.pitcher, pitcherSeason.year, "30")
            updatePitcherLog(pitcherSeasonSplit30, gameLogs30, atBats30)
            result.add(pitcherSeasonSplit30)
        }

        return result

    }






    public List<PitcherGameLog> getRecentGameLogs(Player thePitcher, int ageDays) {

        Date theDate = new Date() - ageDays
        theDate.clearTime()

        Date today = new Date()


        return PitcherGameLog.withCriteria {

            createAlias("game", "g")
            pitcher {
                eq('id', thePitcher.id)
            }
            between("g.startDate", theDate, today)

            order("g.startDate", "desc")

        }
    }


    public List<PitcherGameLog> getRecentStarts(PitcherSeason pitcherSeason, int number) {

        if (!pitcherSeason) return null

        return PitcherGameLog.withCriteria {

            createAlias("game", "g")
            createAlias("g.awayStartingPitcher", "asp")
            createAlias("g.homeStartingPitcher", "hsp")
            pitcher {
                eq('id', pitcherSeason.pitcher.id)
            }


            or {
                eq('asp.id', pitcherSeason.pitcher.id)
                eq('hsp.id', pitcherSeason.pitcher.id)
            }

            eq('year', pitcherSeason.year)


            maxResults(number)
            order("g.startDate", "desc")

        }
    }





    public List<AtBat> getRecentAtBats(PitcherSeason pitcherSeason, List<PitcherGameLog> gameLogs) {
        return AtBat.withCriteria {

            hitter {
                eq('id', pitcherSeason.pitcher.id)
            }

            game {
                'in'('id', gameLogs.collect { it.game.id})
            }

        }
    }


    public PitcherGameLog getPitcherGameLog(Game theGame, Player thePitcher){

        def c = PitcherGameLog.createCriteria()
        List<PitcherGameLog> results = c.list {
            game {
                eq('id', theGame.id)
            }

            pitcher {
                eq('id', thePitcher.id)
            }
        }

        if (results?.size() > 0) return results.get(0)

        return null
    }





    public PitcherCareer updatePitcherCareer(Player thePitcher) {

        PitcherCareer pitcherCareer = PitcherCareer.findOrCreateByPitcher(thePitcher)


        List<PitcherSeason> pitcherSeasons = PitcherSeason.findAllByPitcher(thePitcher)
        List<AtBat> pitcherAtBats = AtBat.findAllByPitcher(thePitcher)
        List<PitcherGameLog> pitcherGameLogs = PitcherGameLog.findAllByPitcher(thePitcher)

        updatePitcherLog(pitcherCareer, pitcherSeasons, pitcherAtBats)


        if (pitcherCareer.save(flush: true)) {
            println "Updated career for: " + pitcherCareer.pitcher.fullName
        } else {
            println "*******ERROR UPDATING CAREER FOR PITCHER: " + pitcherCareer.pitcher.fullName
        }




        List<PitcherCareerSplit> pitcherCareerSplits = generateSplits(pitcherCareer, pitcherAtBats, pitcherGameLogs)

        pitcherCareerSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "************************ ERROR UPDATING CAREER SPLITS FOR PITCHER************************"
            }
        }





        return pitcherCareer

    }












    public List<PitcherSeasonSplit> getPitcherSeasonSplits(Player thePitcher, Integer year) {
        return PitcherSeasonSplit.withCriteria {
            eq('year', year)
            pitcher {
                eq('id', thePitcher.id)
            }
        }
    }






    public PitcherCareer getPitcherCareer(Player thePitcher) {
        PitcherCareer pitcherCareer = PitcherCareer.findOrCreateByPitcher(thePitcher)
        return pitcherCareer
    }

    public PitcherSeason getPitcherSeason(Player thePitcher, int year) {
        PitcherSeason pitcherSeason = PitcherSeason.findByPitcherAndYear(thePitcher, year)
        return pitcherSeason
    }


    public List<PitcherSeason> getPitcherSeasons(Player thePitcher) {
        List<PitcherSeason> pitcherSeasons = PitcherSeason.findAllByPitcher(thePitcher)
        return pitcherSeasons
    }

    public List<PitcherCareerSplit> getPitcherCareerSplits(Player thePitcher) {
        List<PitcherCareerSplit> pitcherCareerSplits = PitcherCareerSplit.findAllByPitcher(thePitcher)
        return pitcherCareerSplits
    }

    public List<AtBat> getPitcherCareerAtBats(Player thePitcher) {
        List<AtBat> pitcherAtBats = AtBat.findAllByPitcher(thePitcher)
        return pitcherAtBats
    }

    public List<PitcherGameLog> getPitcherCareerGameLogs(Player theHitter) {
        List<PitcherGameLog> pitcherGameLogs = PitcherGameLog.findAllByPitcher(thePitcher)
        return pitcherGameLogs
    }







}
