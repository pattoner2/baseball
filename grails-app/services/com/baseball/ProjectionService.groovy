package com.baseball

import com.baseball.AtBat
import com.baseball.HitterCareer
import com.baseball.HitterCareerSplit
import com.baseball.HitterGameLog
import com.baseball.HitterLogsAdvancedStatBucket
import com.baseball.HitterSeason
import com.baseball.HitterSeasonSplit
import com.baseball.ParkFactor
import com.baseball.PitcherCareer
import com.baseball.PitcherGameLog
import com.baseball.PitcherSeason
import com.baseball.PitcherSeasonSplit
import grails.plugin.cache.Cacheable
import grails.transaction.Transactional


@Transactional
class ProjectionService {

    StatService statService
    HitterService hitterService


    static final double PARK_FACTOR_WEIGHT = 1.0

    public Double calculateProjection(HitterLogsAdvancedStatBucket bucket) {

        EventProbabilities h = getHitterEventProbabilties(bucket)
        EventProbabilities p = getPitcherEventProbabilties(bucket) //no stolen bases
        EventProbabilities lapp = getLeagueAveragePitcherEventProbabilties(bucket.hitterGameLog.year, bucket.hitterGameLog.hitter.bats, bucket.hitterGameLog.isHome()) // no stolen bases





        //Final values
        EventProbabilities f = new EventProbabilities()

        f.bb =  calculateChanceOfSuccess(h.bb, p.bb, lapp.bb)
        f.singles = calculateChanceOfSuccess(h.singles, p.singles, lapp.singles)
        f.doubles = calculateChanceOfSuccess(h.doubles, p.doubles, lapp.doubles)
        f.triples = calculateChanceOfSuccess(h.triples, p.triples, lapp.triples)
        f.hr = calculateChanceOfSuccess(h.hr, p.hr, lapp.hr)
        f.rbi = calculateChanceOfSuccess(h.rbi, p.rbi, lapp.rbi)
        f.runs = calculateChanceOfSuccess(h.runs, p.runs, lapp.runs)
        f.sb = h.sb
        f.hbp = calculateChanceOfSuccess(h.hbp, p.hbp, lapp.hbp)



        double baseAtBat = statService.calculateProjectedFanduelPoints(f.singles, f.doubles, f.triples, f.hr, f.rbi, f.runs, f.bb, f.sb, f.hbp)


        //Account for stadium. Multiply by park factor.
        ParkFactor pf = ParkFactor.findByTeam(bucket.hitterGameLog.game.homeTeam)

        double pf1b = 0
        double pf2b = 0
        double pf3b = 0
        double pfHr = 0
        double pfRuns = 0
        double pfRbi = 0



        String bats = bucket.hitterGameLog.hitter.bats
        if ("S".equals(bats)) {
            //Pretend switch hitters are going to be hitting from their ideal side.
            bats = "L".equals(bucket.hitterGameLog.opponentPitcher.pitches) ? "R" : "L"
        }

        switch (bats) {
            case "L":
                pf1b = pf.l1b
                pf2b = pf.l2b
                pf3b = pf.l3b
                pfHr = pf.lHr
                pfRuns = pf.lRuns
                pfRbi = pf.lRuns
                break;
            case "R":
                pf1b = pf.r1b
                pf2b = pf.r2b
                pf3b = pf.r3b
                pfHr = pf.rHr
                pfRuns = pf.rRuns
                pfRbi = pf.rRuns
                break;
        }



        f.singles   = f.singles * weightAverage(pf1b, PARK_FACTOR_WEIGHT)
        f.doubles   = f.doubles  * weightAverage(pf2b, PARK_FACTOR_WEIGHT)
        f.triples   = f.triples * weightAverage(pf3b, PARK_FACTOR_WEIGHT)
        f.hr        = f.hr * weightAverage(pfHr, PARK_FACTOR_WEIGHT)
        f.runs      = f.runs * weightAverage(pfRuns, PARK_FACTOR_WEIGHT)
        f.rbi       = f.rbi  * weightAverage(pfRbi, PARK_FACTOR_WEIGHT)


        double afterParkFactor = statService.calculateProjectedFanduelPoints(f.singles, f.doubles, f.triples, f.hr, f.rbi, f.runs, f.bb, f.sb, f.hbp)


        bucket.hitterGameLog.parkFactor = statService.calculateDiff(afterParkFactor, baseAtBat)


        if (-1 == bucket.hitterGameLog.knownBattingOrder) return 0

        //Account for batting order. This is probably multiplying by the number of expected at-bats in the game.
        int bo = (int) bucket.hitterGameLog.knownBattingOrder / 100
        double projectedAtBats = getProjectedAtBats(bo)


        bucket.hitterGameLog.hrPercent = 1 - ( (1- f.hr)**projectedAtBats)


        //TODO: Account for weather


        return afterParkFactor * getProjectedAtBats(bo)

    }









    public Double calculateProjection(PitcherGameLog gameLog) {


        //Figure out chances of getting a win
        double projectedWin = gameLog.vegasWinPercent




        //Guess how many earned runs
        double projectedEr = 0




        //Guess how many strikeouts
        double projectedSo = 0


        //Guess how many outs they'll pitch for
        double projectedOuts = 0


        return 0//statService.calculatePitcherFanduelPoints(projectedWin, projectedEr, projectedSo, projectedOuts )

    }



































    public EventProbabilities getHitterEventProbabilties(HitterLogsAdvancedStatBucket h) {



        HitterSeason currentSeason = h.hitterSeason


        List<HitterSeasonSplit> currentSeasonSplits = h.hitterSplits

        HitterCareer career = h.hitterCareer
        List<HitterCareerSplit> careerSplits = h.hitterCareerSplits


        HitterSeasonSplit currentSeasonHandednessSplit = "L".equals(h.hitterGameLog.opponentPitcher?.pitches) ? currentSeasonSplits.find{ "L".equals(it.split)} : currentSeasonSplits.find{ "R".equals(it.split)}
        HitterSeasonSplit currentSeasonHomeAwaySplit = h.hitterGameLog.isHome() ? currentSeasonSplits.find{ "H".equals(it.split)} : currentSeasonSplits.find{ "A".equals(it.split)}

        List<HitterCareerSplit> careerHandednessSplits = "L".equals(h.hitterGameLog.opponentPitcher?.pitches) ? careerSplits.findAll{ "L".equals(it.split)} : careerSplits.findAll{ "R".equals(it.split)}
        List<HitterCareerSplit> careerHomeAwaySplits = h.hitterGameLog.isHome() ? careerSplits.findAll{ "H".equals(it.split)} : careerSplits.findAll{ "A".equals(it.split)}


        List<AtBat> careerVsPitcherAtbats = h.atBatsVsPitcher



        EventProbabilities p = new EventProbabilities()

        if (!career) return p

        double careerDividend = career?.pa ?: 0
        double seasonDividend = currentSeason?.pa ?: 0
        double careerHomeAwayDividend = careerHomeAwaySplits?.collect { it?.pa ?: 0}?.sum() ?: 0
        double careerHandednessDividend = careerHandednessSplits?.collect { it?.pa ?: 0}?.sum() ?: 0
        double seasonHomeAwayDividend = currentSeasonHomeAwaySplit?.pa ?: 0
        double seasonHandednessDividend = currentSeasonHandednessSplit?.pa ?: 0
        double careerVsPitcherDividend = careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.pa ? 1 : 0}?.sum() : 0


        p.bb = new HitterStatProbability(

                careerDivisor: career.paBb ? career.paBb : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paBb ? currentSeason.paBb : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paBb ? it.paBb : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paBb ? it.paBb : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paBb ? currentSeasonHomeAwaySplit.paBb : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paBb ? currentSeasonHandednessSplit.paBb : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? (careerVsPitcherAtbats?.collect { it?.isBB() ? 1 : 0}?.sum() ?: 0) : 0,
                careerVsPitcherDividend: careerVsPitcherDividend

        ).calculate()



        p.singles = new HitterStatProbability(

                careerDivisor: career.paSingles ? career.paSingles : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paSingles ? currentSeason.paSingles : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paSingles ? currentSeasonHomeAwaySplit.paSingles : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paSingles ? currentSeasonHandednessSplit.paSingles : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.isSingle() ? 1 : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()

        p.doubles = new HitterStatProbability(

                careerDivisor: career.paDoubles ? career.paDoubles : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paDoubles ? currentSeason.paDoubles : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paDoubles ? currentSeasonHomeAwaySplit.paDoubles : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paDoubles ? currentSeasonHandednessSplit.paDoubles : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.isDouble() ? 1 : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()


        p.triples = new HitterStatProbability(

                careerDivisor: career?.paTriples ? career.paTriples : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paTriples ? currentSeason.paTriples : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paTriples ? currentSeasonHomeAwaySplit.paTriples : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paTriples ? currentSeasonHandednessSplit.paTriples : 0 ,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.isTriple() ? 1 : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()


        p.hr = new HitterStatProbability(

                careerDivisor: career?.paHomeRuns ? career.paHomeRuns : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paHomeRuns ? currentSeason.paHomeRuns : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paHomeRuns ? it.paHomeRuns : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paHomeRuns ? it.paHomeRuns : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paHomeRuns ? currentSeasonHomeAwaySplit.paHomeRuns : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paHomeRuns ? currentSeasonHandednessSplit.paHomeRuns : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.isHr() ? 1 : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()


        p.rbi = new HitterStatProbability(

                careerDivisor: career?.paRbi ? career.paRbi : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paRbi ? currentSeason.paRbi : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paRbi ? currentSeasonHomeAwaySplit.paRbi : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paRbi ?  currentSeasonHandednessSplit.paRbi : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.rbi ? it.rbi : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()

        p.runs = new HitterStatProbability(

                careerDivisor: career?.paRuns ?  career.paRuns  : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paRuns ? currentSeason.paRuns : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paRuns ? currentSeasonHomeAwaySplit.paRuns : 0 ,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paRuns ? currentSeasonHandednessSplit.paRuns : 0 ,
                seasonHandednessDividend: seasonHandednessDividend,



        ).calculate()


        p.sb = new HitterStatProbability(

                careerDivisor: career?.paSb ? career.paSb : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paSb ? currentSeason.paSb : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paSb ? it.paSb : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paSb ? it.paSb : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paSb ? currentSeasonHomeAwaySplit.paSb : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paSb ? currentSeasonHandednessSplit.paSb : 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()





        double careerHomeAwayDivisor = careerHomeAwaySplits?.collect { it?.hbp ? it?.hbp : 0}?.sum()
        double careerHandednessDivisor = careerHandednessSplits?.collect { it?.hbp ? it?.hbp : 0}?.sum()
        p.hbp = new HitterStatProbability(

                careerDivisor: career?.paHbp ? career.paHbp : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paHbp ? currentSeason.paHbp : 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwayDivisor,
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessDivisor,
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paHbp ? currentSeasonHomeAwaySplit.paHbp : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paHbp ? currentSeasonHandednessSplit.paHbp : 0,
                seasonHandednessDividend: seasonHandednessDividend,

                careerVsPitcherDivisor:  careerVsPitcherAtbats?.size() > 0 ? careerVsPitcherAtbats?.collect { it?.isHpb() ? 1 : 0}?.sum() : 0,
                careerVsPitcherDividend: careerVsPitcherDividend


        ).calculate()


        return p

    }






    public EventProbabilities getPitcherEventProbabilties(HitterLogsAdvancedStatBucket h) {

        PitcherSeason currentSeason = h.pitcherSeason
        List<PitcherSeasonSplit> currentSeasonSplits = h.pitcherSplits

        PitcherCareer career = h.pitcherCareer
        List<PitcherSeasonSplit> careerSplits = h.pitcherCareerSplits


        PitcherSeasonSplit currentSeasonHandednessSplit = "L".equals(h.hitterGameLog.hitter?.bats) ? currentSeasonSplits.find{ "L".equals(it.split)} : currentSeasonSplits.find{ "R".equals(it.split)}
        PitcherSeasonSplit currentSeasonHomeAwaySplit = h.hitterGameLog.isHome() ? currentSeasonSplits.find{ "A".equals(it.split)} : currentSeasonSplits.find{ "H".equals(it.split)}

        List<PitcherSeasonSplit> careerHandednessSplits = "L".equals(h.hitterGameLog.hitter?.bats) ? careerSplits.findAll{ "L".equals(it.split)} : careerSplits.findAll{ "R".equals(it.split)}
        List<PitcherSeasonSplit> careerHomeAwaySplits = h.hitterGameLog.isHome() ? careerSplits.findAll{ "A".equals(it.split)} : careerSplits.findAll{ "H".equals(it.split)}





        EventProbabilities p = new EventProbabilities()

        if (!career) return getLeagueAveragePitcherEventProbabilties(2016, h.hitterGameLog.hitter.bats, h.hitterGameLog.isHome())

        double careerDividend = career.battersFace ? career.battersFace : 0


        //If a pitcher has faced less than 100 batters we're going to treat them like a league-average pitcher
        if (careerDividend < 100) return getLeagueAveragePitcherEventProbabilties(2016, h.hitterGameLog.hitter.bats, h.hitterGameLog.isHome())




        double seasonDividend = currentSeason?.battersFace ?: 0
        double careerHomeAwayDividend = careerHomeAwaySplits?.collect { it?.battersFace ?: 0}.sum() ?: 0
        double careerHandednessDividend = careerHandednessSplits?.collect { it?.battersFace ?: 0}.sum() ?: 0
        double seasonHomeAwayDividend = currentSeasonHomeAwaySplit?.battersFace ? currentSeasonHomeAwaySplit?.battersFace : 0
        double seasonHandednessDividend = currentSeasonHandednessSplit?.battersFace ? currentSeasonHandednessSplit?.battersFace : 0



        p.bb = new HitterStatProbability(

                careerDivisor: career.bb ? career.bb : 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.bb ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.bb ? it.bb : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.bb ? it.bb : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.bb ? currentSeasonHomeAwaySplit.bb : 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.bb ? currentSeasonHandednessSplit.bb : 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()



        p.singles = new HitterStatProbability(

                careerDivisor: career?.paSingles ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paSingles ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paSingles ?: 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paSingles ?: 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paSingles ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paSingles ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()

        p.doubles = new HitterStatProbability(

                careerDivisor: career?.paDoubles ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paDoubles ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paDoubles ?: 0}.sum() ?: 0,
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paDoubles ?: 0}.sum() ?: 0,
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paDoubles ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paDoubles ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()


        p.triples = new HitterStatProbability(

                careerDivisor: career?.paTriples ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paTriples ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paTriples ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paTriples ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()


        p.hr = new HitterStatProbability(

                careerDivisor: career?.paHr ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paHr ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paHr ?: 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paHr ?: 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paHr ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paHr ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()



        p.rbi = new HitterStatProbability(

                careerDivisor: career?.paRbi ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.paRbi ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paRbi ?: 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paRbi ?: 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.paRbi ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.paRbi ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()



        p.runs = p.rbi




        p.hbp = new HitterStatProbability(

                careerDivisor: career?.hbp ?: 0,
                careerDividend: careerDividend,

                seasonDivisor: currentSeason?.hbp ?: 0,
                seasonDividend: seasonDividend,

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.hbp ?: 0}.sum(),
                careerHomeAwayDividend: careerHomeAwayDividend,

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.hbp ?: 0}.sum(),
                careerHandednessDividend: careerHandednessDividend,


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit?.hbp ?: 0,
                seasonHomeAwayDividend: seasonHomeAwayDividend,

                seasonHandednessDivisor: currentSeasonHandednessSplit?.hbp ?: 0,
                seasonHandednessDividend: seasonHandednessDividend

        ).calculate()


        return p

    }









    @Cacheable("leagueAveragePitcherEventProbabilties")
    public EventProbabilities getLeagueAveragePitcherEventProbabilties(int year, String bats, boolean isBatterHome) {

        List<PitcherSeason> currentSeason = PitcherSeason.findAllByYear(year)
        List<PitcherSeasonSplit> currentSeasonSplits = PitcherSeasonSplit.findAllByYear(year)

        List<PitcherCareer> career = PitcherCareer.withCriteria {
            pitcher {
                'in'('id', currentSeason.collect { it.pitcher.id})
            }
        }


        List<PitcherSeasonSplit> careerSplits = PitcherSeasonSplit.withCriteria {
            pitcher {
                'in'('id', currentSeason.collect { it.pitcher.id})
            }
        }


        List<PitcherSeasonSplit> currentSeasonHandednessSplit = "L".equals(bats) ? currentSeasonSplits.findAll{ "L".equals(it.split)} : currentSeasonSplits.findAll{ "R".equals(it.split)}
        List<PitcherSeasonSplit> currentSeasonHomeAwaySplit = isBatterHome ? currentSeasonSplits.findAll{ "A".equals(it.split)} : currentSeasonSplits.findAll{ "H".equals(it.split)}

        List<PitcherSeasonSplit> careerHandednessSplits = "L".equals(bats) ? careerSplits.findAll{ "L".equals(it.split)} : careerSplits.findAll{ "R".equals(it.split)}
        List<PitcherSeasonSplit> careerHomeAwaySplits = isBatterHome ? careerSplits.findAll{ "A".equals(it.split)} : careerSplits.findAll{ "H".equals(it.split)}





        EventProbabilities p = new EventProbabilities()


        p.bb = new HitterStatProbability(

                careerDivisor: career.collect { it?.bb ? it.bb : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.bb ? it.bb : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.bb ? it.bb : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.bb ? it.bb : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.bb ? it.bb : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.bb ? it.bb : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()



        p.singles = new HitterStatProbability(

                careerDivisor: career.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paSingles ? it.paSingles : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()

        p.doubles = new HitterStatProbability(

                careerDivisor: career.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paDoubles ? it.paDoubles : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()


        p.triples = new HitterStatProbability(

                careerDivisor: career.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paTriples ? it.paTriples : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()


        p.hr = new HitterStatProbability(

                careerDivisor: career.collect { it?.paHr ? it.paHr : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paHr ? it.paHr : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paHr ? it.paHr : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paHr ? it.paHr : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paHr ? it.paHr : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paHr ? it.paHr : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()


        p.rbi = new HitterStatProbability(

                careerDivisor: career.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paRbi ? it.paRbi : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()

        p.runs = new HitterStatProbability(

                careerDivisor: career.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.paRuns ? it.paRuns : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()




        p.hbp = new HitterStatProbability(

                careerDivisor: career.collect { it?.hbp ? it.hbp : 0}.sum(),
                careerDividend: career.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonDivisor: currentSeason.collect { it?.hbp ? it.hbp : 0}.sum(),
                seasonDividend: currentSeason.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHomeAwayDivisor: careerHomeAwaySplits?.collect { it?.hbp ? it.hbp : 0}.sum(),
                careerHomeAwayDividend: careerHomeAwaySplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                careerHandednessDivisor: careerHandednessSplits?.collect { it?.hbp ? it.hbp : 0}.sum(),
                careerHandednessDividend: careerHandednessSplits?.collect { it?.battersFace ? it.battersFace : 0}.sum(),


                seasonHomeAwayDivisor: currentSeasonHomeAwaySplit.collect { it?.hbp ? it.hbp : 0}.sum(),
                seasonHomeAwayDividend: currentSeasonHomeAwaySplit.collect { it?.battersFace ? it.battersFace : 0}.sum(),

                seasonHandednessDivisor: currentSeasonHandednessSplit.collect { it?.hbp ? it.hbp : 0}.sum(),
                seasonHandednessDividend: currentSeasonHandednessSplit.collect { it?.battersFace ? it.battersFace : 0}.sum()

        ).calculate()


        return p

    }


    public double calculateChanceOfSuccess(double hitterPercent, double pitcherPercent, double leagueAverage) {

        double divisor =  (hitterPercent * pitcherPercent) / leagueAverage
        double dividend =  divisor + (   (1-hitterPercent)  * (1-pitcherPercent) / (1-leagueAverage) )

        if (dividend == 0) return 0.0

        double value =  divisor / dividend
        return value
    }



    public double weightAverage(double start, double weight) {
        double result = 1 - ((1 - start) * weight)
        return result
    }



    public double getProjectedAtBats(int order) {

        //http://www.smartfantasybaseball.com/2014/10/the-effect-of-batting-order-on-r-and-rbi-production/
        //TODO: Take into account league differences

        switch(order) {

            case 1: return 4.63
            case 2: return 4.52
            case 3: return 4.42
            case 4: return 4.32
            case 5: return 4.22
            case 6: return 4.11
            case 7: return 3.99
            case 8: return 3.88
            case 9: return 3.75


        }

    }








    public double calculateDiff(double first, double second) {
        return statService.calculateDiff(first, second)
    }




    public class EventProbabilities {

        double bb
        double singles
        double doubles
        double triples
        double hr
        double rbi
        double runs
        double sb
        double hbp

    }



    public class HitterStatProbability {

        double careerDivisor
        double careerDividend

        double careerHomeAwayDivisor
        double careerHomeAwayDividend

        double careerHandednessDivisor
        double careerHandednessDividend

        double seasonDivisor
        double seasonDividend

        double seasonHomeAwayDivisor
        double seasonHomeAwayDividend

        double seasonHandednessDivisor
        double seasonHandednessDividend

        double careerVsPitcherDivisor
        double careerVsPitcherDividend


        static final double CAREER_WEIGHT = 0.4
        static final double VS_WEIGHT = 3.5



        public double calculate() {

            double careerAvg =  careerDividend > 0 ? careerDivisor / careerDividend : 0
            double careerHomeAwayAvg = careerHomeAwayDividend > 0 ? careerHomeAwayDivisor / careerHomeAwayDividend : 0
            double careerHandednessAvg = careerHandednessDividend > 0 ?  careerHandednessDivisor / careerHandednessDividend : 0
            double careerVsPitcherAvg = careerVsPitcherDividend > 0 ? careerVsPitcherDivisor / careerVsPitcherDividend : 0


            WeightedSet careerWeightedSet = new WeightedSet(avg: careerAvg, total: careerDividend)
            WeightedSet careerHomeAwayWeightedSet = new WeightedSet(avg: careerHomeAwayAvg, total: careerHomeAwayDividend)
            WeightedSet careerHandednessWeightedSet = new WeightedSet(avg: careerHandednessAvg, total: careerHandednessDividend)
            WeightedSet careerVsPitchingWeightedSet = new WeightedSet(avg: careerVsPitcherAvg, total: careerVsPitcherDivisor * VS_WEIGHT)



            double careerWeightedAvg = calculateWeightedAverage(careerWeightedSet, careerHomeAwayWeightedSet, careerHandednessWeightedSet, careerVsPitchingWeightedSet)



            double seasonAvg =  seasonDividend > 0 ? seasonDivisor / seasonDividend : 0
            double seasonHomeAwayAvg = seasonHomeAwayDividend ? seasonHomeAwayDivisor / seasonHomeAwayDividend : 0
            double seasonHandednessAvg = seasonHandednessDividend ? seasonHandednessDivisor / seasonHandednessDividend : 0


            WeightedSet seasonWeightedSet = new WeightedSet(avg: seasonAvg, total: seasonDividend)
            WeightedSet seasonHomeAwayWeightedSet = new WeightedSet(avg: seasonHomeAwayAvg, total: seasonHomeAwayDividend)
            WeightedSet seasonHandednessWeightedSet = new WeightedSet(avg: seasonHandednessAvg, total: seasonHandednessDividend)


            double seasonWeightedAvg = calculateWeightedAverage(seasonWeightedSet, seasonHomeAwayWeightedSet, seasonHandednessWeightedSet)




            double careerWeight = (careerDividend + seasonDividend) > 0 ? careerDividend / (careerDividend + seasonDividend) : 0
            careerWeight = careerWeight * CAREER_WEIGHT


            double seasonWeight = 1.0 - careerWeight


            //Adjust for low sample sizes for the season. Less than 50 at bats and we're just going to use the season
            double probability = 0

            if (seasonDividend < 50) {
                probability = careerWeightedAvg
            } else {
                probability = (careerWeightedAvg * careerWeight) + (seasonWeightedAvg * seasonWeight)
            }





            return probability

        }


        public double calculateWeightedAverage(WeightedSet... sets) {

            double sum =0
            sets?.each {
                double setWeight = it.total / sets?.collect { it.total}.sum()
                sum += it.avg * setWeight
            }

            return sum
        }


        public class WeightedSet {
            double avg
            double total
        }




    }

}




