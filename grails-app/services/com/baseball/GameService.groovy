package com.baseball

import com.baseball.Game
import com.baseball.HitterGameLog
import com.baseball.PitcherGameLog
import com.baseball.Player
import com.baseball.Team
import com.baseball.olap.dimensions.GameTime.GameStart
import grails.transaction.Transactional
import groovy.time.TimeCategory
import org.springframework.transaction.annotation.Propagation


class GameService {


    public List<Game> getGamesForDate(Date date) {

        date.clearTime()

        Date tomorrow = date + 1
        tomorrow.clearTime()

        return Game.withCriteria {

            between("startDate", date, tomorrow)
            order("startDate", "asc")
        }


    }


    public List<HitterGameLog> getHitterGameLogsForDate(Date date) {

        date.clearTime()

        Date tomorrow = date + 1
        tomorrow.clearTime()

        return HitterGameLog.withCriteria {
            createAlias("game", "g")
            hitter {
                ne('position', 'P')
            }

            or {
                isNotNull("battingOrder")
                isNotNull("battingOrderConfirmed")
                isNotNull("battingOrderUnconfirmed")
            }

//            isNotNull("fanduelSalary")
            between("g.startDate", date, tomorrow)
        }//.findAll { it.battingOrderStatus != "out"}


    }



    public List<PitcherGameLog> getProbablePitcherGameLogs(Date date) {


        date.clearTime()

        Date tomorrow = date + 1
        tomorrow.clearTime()

//        use( TimeCategory ) {
//            tomorrow = tomorrow + 11.hours
//        }

        def results = PitcherGameLog.withCriteria {
            createAlias("game", "g")
            between("g.startDate", date, tomorrow)

        }

        return results.findAll{ it.probablePitcher}
    }






    public List<Player> getStartingLineup(Game theGame, Team theTeam) {

        List<Player> hitters = null

        //Get official lineup
        hitters = HitterGameLog.withCriteria {

            game {
                eq('id', theGame.id)
            }


            team {
                eq('id', theTeam.id)
            }

            isNotNull("battingOrder")
            sqlRestriction("MOD(batting_order, 100) = 0")

        }?.collect { it.hitter}


        if (hitters?.size() > 0) return hitters



        //Get confirmed lineup
        hitters = HitterGameLog.withCriteria {

            game {
                eq('id', theGame.id)
            }


            team {
                eq('id', theTeam.id)
            }

            isNotNull("battingOrderConfirmed")
            sqlRestriction("MOD(batting_order_confirmed, 100) = 0")

        }?.collect { it.hitter}


        if (hitters?.size() > 0) return hitters




        //Get unconfirmed lineup
        hitters = HitterGameLog.withCriteria {

            game {
                eq('id', theGame.id)
            }


            team {
                eq('id', theTeam.id)
            }

            isNotNull("battingOrderUnconfirmed")
            sqlRestriction("MOD(batting_order_unconfirmed, 100) = 0")

        }?.collect { it.hitter}


        if (hitters?.size() > 0) return hitters


    }


    public boolean isPreAllStar(Game theGame) {

        Game allstarGame = Game.findByTypeAndYear("A", theGame.year)

        if (allstarGame && theGame?.startDate?.before(allstarGame?.startDate)) return true

        return false

    }

    public GameStart getGameStart(Game theGame) {

        if (theGame.startDate.hours <= 2) return GameStart.EARLY
        if (theGame.startDate.hours <= 5) return GameStart.AFTERNOON
        return GameStart.NIGHT

    }



}
