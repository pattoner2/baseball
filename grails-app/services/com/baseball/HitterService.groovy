package com.baseball

import com.baseball.AtBat
import com.baseball.HitterCareer
import com.baseball.HitterCareerSplit
import com.baseball.HitterGameLog
import com.baseball.HitterLogsAdvancedStatBucket
import com.baseball.HitterSeason
import com.baseball.HitterSeasonSplit
import com.baseball.PitcherGameLog
import com.baseball.PitcherSeason
import com.baseball.PitcherSeasonSplit
import com.baseball.Player
import com.baseball.SplitStats
import com.baseball.domain.HitterLog
import com.baseball.gameday.NumberUtil
import com.mdimension.jchronic.Chronic
import grails.transaction.Transactional
import org.hibernate.criterion.Projection
import org.springframework.transaction.annotation.Propagation

import static groovyx.gpars.GParsPool.withPool


class HitterService {

    StatService statService
    PitcherService pitcherService
    GameService gameService
    ProjectionService projectionService
    AtbatService atbatService
    AdvancedStatService advancedStatService
    ParkFactorService parkFactorService



    public void processAdvacedStatsForSeason(int year) {

        println "***************************************************************"
        println "***************************************************************"
        println "************ PROCESS ADVANCED HITTING STATS " + year + " ******"
        println "***************************************************************"
        println "***************************************************************"


        Date startDate = Chronic.parse('march 10 ' + year)?.beginCalendar?.time
        Date endDate = Chronic.parse('november 23 ' + year)?.beginCalendar?.time

        if (endDate.after(new Date())) endDate = new Date()


        List<Date> dates = new ArrayList<>()
        startDate.upto(endDate) {
            dates.add(it)
        }


        dates.each { Date date ->
            updateAdvancedStatsForDate(date)
        }


        println "***************************************************************"
        println "***************************************************************"
        println "************ FINISHED ADVANCED HITTING STATS " + year + " *****"
        println "***************************************************************"
        println "***************************************************************"


    }





    public void updateAdvancedStatsForDate(Date date) {

        List<HitterGameLog> hitterGameLogs = gameService.getHitterGameLogsForDate(date)

        ///withPool(5) {
            hitterGameLogs?.each { hitterGameLog ->
                updateAdvancedStatsForGameLog(hitterGameLog)
            }


        //}

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAdvancedStatsForGameLog(HitterGameLog hitterGameLog) {

        if (!hitterGameLog.opponentPitcher) return


        HitterLogsAdvancedStatBucket h = getHitterLogsAdvancedStatBucket(hitterGameLog)


        advancedStatService.updateHitterGameLogAdvancedStats(h)


        try {
            if (h.hitterGameLog.save(flush: true)) {
                println "Updating advanced stats for hitterGameLog: " + h.hitterGameLog.game.startDate + " - " + h.hitterGameLog.hitter.firstName + " " +h.hitterGameLog.hitter.lastName
            } else {
                println "******* PROBLEM SAVING HITTER GAME LOG ***********"
            }
        } catch(Exception ex) {
            ex.printStackTrace()
        }
    }




    public HitterLogsAdvancedStatBucket getHitterLogsAdvancedStatBucket(HitterGameLog hitterGameLog) {

//
//
//        def h = new HitterLogsAdvancedStatBucket(
//                player: hitterGameLog.hitter,
//                hitterGameLog: hitterGameLog,
//                hitterSeason: getHitterSeasonBeforeDate(hitterGameLog.hitter, hitterGameLog.year, hitterGameLog.game.startDate),
//                hitterSplits: getHitterSeasonSplitsBeforeDate(hitterGameLog.hitter, hitterGameLog.year, hitterGameLog.game.startDate),
//                hitterCareer: getHitterCareer(hitterGameLog.hitter),
//                hitterCareerSplits: getHitterCareerSplits(hitterGameLog.hitter),
//                pitcherSeason: pitcherService.getPitcherSeason(hitterGameLog.opponentPitcher, hitterGameLog.year),
//                pitcherSplits: pitcherService.getPitcherSeasonSplits(hitterGameLog.opponentPitcher, hitterGameLog.year),
//                pitcherCareer: pitcherService.getPitcherCareer(hitterGameLog.opponentPitcher),
//                pitcherCareerSplits: pitcherService.getPitcherCareerSplits(hitterGameLog.opponentPitcher),
//                atBatsVsPitcher: getHitterPitcherCareerAtBats(hitterGameLog.hitter, hitterGameLog.opponentPitcher)
//        )


        def h2 = new HitterLogsAdvancedStatBucket(
                player: hitterGameLog.hitter,
                hitterGameLog: hitterGameLog,
                hitterSeason: getHitterSeason(hitterGameLog.hitter, hitterGameLog.year),
                hitterSplits: getHitterSeasonSplits(hitterGameLog.hitter, hitterGameLog.year),
                hitterCareer: getHitterCareer(hitterGameLog.hitter),
                hitterCareerSplits: getHitterCareerSplits(hitterGameLog.hitter),
                pitcherSeason: pitcherService.getPitcherSeason(hitterGameLog.opponentPitcher, hitterGameLog.year),
                pitcherSplits: pitcherService.getPitcherSeasonSplits(hitterGameLog.opponentPitcher, hitterGameLog.year),
                pitcherCareer: pitcherService.getPitcherCareer(hitterGameLog.opponentPitcher),
                pitcherCareerSplits: pitcherService.getPitcherCareerSplits(hitterGameLog.opponentPitcher),
                atBatsVsPitcher: getHitterPitcherCareerAtBats(hitterGameLog.hitter, hitterGameLog.opponentPitcher)
        )


        return h2
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateHitterSeasonStats(Player theHitter, Integer theYear) {

        if (theHitter == null) return

        theHitter = Player.get(theHitter.id)

        println String.format('Updating season stats for %1s (%2s)', theHitter.firstName + " " + theHitter.lastName, theYear)

        List<HitterGameLog> gameLogList = HitterGameLog.withCriteria {
            createAlias("game", "g")
            hitter {
                eq('id', theHitter.id)
            }

            and {
                eq('year', theYear)
                eq('gamedayPopulated', true)
                eq('g.status', "Final")
            }
        }

        if (gameLogList?.size() == 0) return

        HitterSeason hitterSeason = HitterSeason.findByHitterAndYear(theHitter, theYear)

        if (!hitterSeason) {
            hitterSeason = new HitterSeason(hitter: theHitter, year: theYear)
            hitterSeason.save(flush:true)
        }


        List<AtBat> atBats = getAtbats(hitterSeason)

        updateHitterLog(hitterSeason, gameLogList, atBats)

        if (!hitterSeason.save(flush: true)) {
            println "*************ERROR SAVING HITTER SEASON " + hitterSeason.errors + " *************************"
        }



        List<HitterSeasonSplit> seasonSplits = generateSplits(hitterSeason, atBats, gameLogList)

        seasonSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "***************** ERROR SAVING HITTER SEASON SPLIT**************"
            }
        }



        List<HitterSeasonSplit> recentSplits = generateRecentSplits(theHitter)

        recentSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "***************** ERROR SAVING RECENT HITTER SEASON SPLIT**************"
            }
        }



        updateHitterCareer(theHitter)



    }



    public void updateHitterLog(HitterLog hitterLog, SplitStats splitStats) {
        updateHitterLog(hitterLog, splitStats.gameLogs, splitStats.atBats)
    }

    public void updateHitterLog(HitterLog hitterLog, List<HitterLog> hitterLogs, List<AtBat> atBats) {

        hitterLog.clearAll()



        hitterLogs?.each {
            if (it.games) hitterLog.games       = statService.incrementBy(hitterLog.games, it.games)
            if (it.atBats) hitterLog.atBats     = statService.incrementBy(hitterLog.atBats, it.atBats)
            if (it.hits) hitterLog.hits         = statService.incrementBy(hitterLog.hits, it.hits)
            if (it.singles) hitterLog.singles   = statService.incrementBy(hitterLog.singles, it.singles)
            if (it.doubles) hitterLog.doubles   = statService.incrementBy(hitterLog.doubles, it.doubles)
            if (it.triples) hitterLog.triples   = statService.incrementBy(hitterLog.triples, it.triples)
            if (it.homeRuns) hitterLog.homeRuns = statService.incrementBy(hitterLog.homeRuns, it.homeRuns)
            if (it.runs) hitterLog.runs         = statService.incrementBy(hitterLog.runs, it.runs)
            if (it.rbi) hitterLog.rbi           = statService.incrementBy(hitterLog.rbi, it.rbi)
            if (it.bb) hitterLog.bb             = statService.incrementBy(hitterLog.bb, it.bb)
            if (it.hbp) hitterLog.hbp           = statService.incrementBy(hitterLog.hbp, it.hbp)
            if (it.sb) hitterLog.sb             = statService.incrementBy(hitterLog.sb, it.sb)
            if (it.cs) hitterLog.cs             = statService.incrementBy(hitterLog.cs, it.cs)
            if (it.so) hitterLog.so             = statService.incrementBy(hitterLog.so, it.so)
            if (it.lob) hitterLog.lob           = statService.incrementBy(hitterLog.lob, it.lob)
            if (it.sacBunts) hitterLog.sacBunts = statService.incrementBy(hitterLog.sacBunts, it.sacBunts)
            if (it.sacFlys) hitterLog.sacFlys   = statService.incrementBy(hitterLog.sacFlys, it.sacFlys)
            if (it.groundOuts) hitterLog.groundOuts = statService.incrementBy(hitterLog.groundOuts, it.groundOuts)
            if (it.flyOuts) hitterLog.flyOuts   = statService.incrementBy(hitterLog.flyOuts, it.flyOuts)




            if (it?.flyBalls) hitterLog.flyBalls                   = statService.incrementBy(hitterLog.flyBalls, it.flyBalls)
            if (it?.groundBalls) hitterLog.groundBalls                   = statService.incrementBy(hitterLog.groundBalls, it.groundBalls)
            if (it?.lineDrives) hitterLog.lineDrives                   = statService.incrementBy(hitterLog.lineDrives, it.lineDrives)
            if (it?.lineOuts) hitterLog.lineOuts                   = statService.incrementBy(hitterLog.lineOuts, it.lineOuts)




            if (it.gidp) hitterLog.gidp         = statService.incrementBy(hitterLog.gidp, it.gidp)
            if (it.po) hitterLog.po             = statService.incrementBy(hitterLog.po, it.po)
            if (it.assists) hitterLog.assists   = statService.incrementBy(hitterLog.assists, it.assists)
            if (it.e) hitterLog.e               = statService.incrementBy(hitterLog.e, it.e)
            if (it.pa) hitterLog.pa             = statService.incrementBy(hitterLog.pa, it.pa)
            if (it.ibb) hitterLog.ibb           = statService.incrementBy(hitterLog.ibb, it.ibb)



            //Park adjusted
            if (it.paHits) hitterLog.paHits         = statService.incrementBy(hitterLog.paHits, it.paHits)
            if (it.paSingles) hitterLog.paSingles   = statService.incrementBy(hitterLog.paSingles, it.paSingles)
            if (it.paDoubles) hitterLog.paDoubles   = statService.incrementBy(hitterLog.paDoubles, it.paDoubles)
            if (it.paTriples) hitterLog.paTriples   = statService.incrementBy(hitterLog.paTriples, it.paTriples)
            if (it.paHomeRuns) hitterLog.paHomeRuns = statService.incrementBy(hitterLog.paHomeRuns, it.paHomeRuns)
            if (it.paRuns) hitterLog.paRuns         = statService.incrementBy(hitterLog.paRuns, it.paRuns)
            if (it.paRbi) hitterLog.paRbi           = statService.incrementBy(hitterLog.paRbi, it.paRbi)
            if (it.paBb) hitterLog.paBb             = statService.incrementBy(hitterLog.paBb, it.paBb)
            if (it.paSb) hitterLog.paSb             = statService.incrementBy(hitterLog.paSb, it.paSb)
            if (it.paCs) hitterLog.paCs             = statService.incrementBy(hitterLog.paCs, it.paCs)
            if (it.paHbp) hitterLog.paHbp           = statService.incrementBy(hitterLog.paHbp, it.paHbp)
            if (it.paSo) hitterLog.paSo             = statService.incrementBy(hitterLog.paSo, it.paSo)
            if (it.paSacBunts) hitterLog.paSacBunts = statService.incrementBy(hitterLog.paSacBunts, it.paSacBunts)
            if (it.paSacFlys) hitterLog.paSacFlys   = statService.incrementBy(hitterLog.paSacFlys, it.paSacFlys)
        }



        hitterLog.avg = 0
        if (hitterLog.atBats && hitterLog.atBats > 0) {
            hitterLog.avg = (hitterLog.hits ?: 0) / hitterLog.atBats
        }


        hitterLog.obp = statService.calculateObp(atBats)



        hitterLog.slg = statService.calculateSlg(atBats)
        hitterLog.tb = statService.calculateTotalBases(atBats)
        hitterLog.ops = hitterLog.obp + hitterLog.slg
        hitterLog.wOba = statService.calculateWOba(atBats )
        hitterLog.wFda = statService.calculateWfda( atBats, hitterLogs )
        hitterLog.soPercent = statService.calculateSoPercent(atBats)
        hitterLog.bbPercent = statService.calculateBBPercent(atBats)
        hitterLog.iso = statService.calculateIso(atBats)
        hitterLog.fanduelPoints = calculateFanduelPoints(hitterLogs)


        parkFactorService.calculateParkAdjustedStats(hitterLog)



    }







    public List<HitterSeasonSplit> generateSplits(HitterSeason hitterSeason, List<AtBat> atBats, List<HitterGameLog> gameLogs) {

        List<HitterGameLog> gameLogsL = new ArrayList<>()
        List<HitterGameLog> gameLogsR = new ArrayList<>()
        List<HitterGameLog> gameLogsH = new ArrayList<>()
        List<HitterGameLog> gameLogsA = new ArrayList<>()
        List<AtBat> atBatsL = new ArrayList<>()
        List<AtBat> atBatsR = new ArrayList<>()
        List<AtBat> atBatsH = new ArrayList<>()
        List<AtBat> atBatsA = new ArrayList<>()

        gameLogs.findAll {
            "L".equals(it.opponentPitcher?.pitches) ? gameLogsL.add(it) : gameLogsR.add(it)
            it.opponent == it.game.awayTeam ? gameLogsH.add(it) : gameLogsA.add(it)
        }


        atBats.each {
            "L".equals(it.pitcherHandedness) ? atBatsL.add(it) : atBatsR.add(it)
            "H".equals(it.hitterHomeAway) ? atBatsH.add(it) : atBatsA.add(it)
        }


        List<HitterSeasonSplit> results = new ArrayList<>()


        HitterSeasonSplit lHitterSeasonSplit = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "L")
        updateHitterLog(lHitterSeasonSplit, gameLogsL, atBatsL )


        HitterSeasonSplit rHitterSeasonSplit = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "R")
        updateHitterLog(rHitterSeasonSplit, gameLogsR, atBatsR )

        HitterSeasonSplit hHitterSeasonSplit = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "H")
        updateHitterLog(hHitterSeasonSplit, gameLogsH, atBatsH)


        HitterSeasonSplit aHitterSeasonSplit = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "A")
        updateHitterLog(aHitterSeasonSplit, gameLogsA, atBatsA)


        results.add(lHitterSeasonSplit)
        results.add(rHitterSeasonSplit)
        results.add(hHitterSeasonSplit)
        results.add(aHitterSeasonSplit)


        return results

    }




    public List<HitterCareerSplit> generateSplits(HitterCareer hitterCareer, List<AtBat> atBats, List<HitterGameLog> gameLogs) {


        List<HitterGameLog> gameLogsL = new ArrayList<>()
        List<HitterGameLog> gameLogsR = new ArrayList<>()
        List<HitterGameLog> gameLogsH = new ArrayList<>()
        List<HitterGameLog> gameLogsA = new ArrayList<>()
        List<AtBat> atBatsL = new ArrayList<>()
        List<AtBat> atBatsR = new ArrayList<>()
        List<AtBat> atBatsH = new ArrayList<>()
        List<AtBat> atBatsA = new ArrayList<>()

        gameLogs.findAll {
            "L".equals(it.opponentPitcher?.pitches) ? gameLogsL.add(it) : gameLogsR.add(it)
            it.opponent == it.game.awayTeam ? gameLogsH.add(it) : gameLogsA.add(it)
        }


        atBats.each {
            "L".equals(it.hitterHandedness) ? atBatsL.add(it) : atBatsR.add(it)
            "H".equals(it.hitterHomeAway) ? atBatsH.add(it) : atBatsA.add(it)
        }

        List<HitterCareerSplit> results = new ArrayList<>()


        HitterCareerSplit hitterCareerSplitL = HitterCareerSplit.findOrCreateByHitterAndSplit(hitterCareer.hitter, "L")
        updateHitterLog(hitterCareerSplitL, gameLogsL, atBatsL )

        HitterCareerSplit hitterCareerSplitR = HitterCareerSplit.findOrCreateByHitterAndSplit(hitterCareer.hitter, "R")
        updateHitterLog(hitterCareerSplitR, gameLogsR, atBatsR )

        HitterCareerSplit hitterCareerSplitH = HitterCareerSplit.findOrCreateByHitterAndSplit(hitterCareer.hitter, "H")
        updateHitterLog(hitterCareerSplitH, gameLogsH, atBatsH)


        HitterCareerSplit hitterCareerSplitA = HitterCareerSplit.findOrCreateByHitterAndSplit(hitterCareer.hitter, "A")
        updateHitterLog(hitterCareerSplitA, gameLogsA, atBatsA)


        results.add(hitterCareerSplitL)
        results.add(hitterCareerSplitR)
        results.add(hitterCareerSplitH)
        results.add(hitterCareerSplitA)


        return results

    }




    public List<HitterSeasonSplit> generateRecentSplits(Player player) {

        //Look up most recent season for hitter
        List<HitterGameLog> gameLogs7 = getRecentGameLogs(player, 7)
        List<HitterGameLog> gameLogs15 = getRecentGameLogs(player, 15)
        List<HitterGameLog> gameLogs30 = getRecentGameLogs(player, 30)


        Integer mostRecentSeason = gameLogs7.collect { it.year ? it.year : 0}.max()
        if (!mostRecentSeason) return

        HitterSeason hitterSeason = HitterSeason.findByHitterAndYear(player, mostRecentSeason)
        if (!hitterSeason) return



        List<HitterSeasonSplit> results = new ArrayList<>()

        if (gameLogs7?.size() > 0) {
            List<AtBat> atBats7 = atbatService.getAtBats(player, gameLogs7)


            HitterSeasonSplit hitterSeasonSplit7 = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "7")
            updateHitterLog(hitterSeasonSplit7, gameLogs7, atBats7)
            results.add(hitterSeasonSplit7)
        }

        if (gameLogs15?.size() > 0) {
            List<AtBat> atBats15 = atbatService.getAtBats(player, gameLogs15)

            HitterSeasonSplit hitterSeasonSplit15 = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "15")
            updateHitterLog(hitterSeasonSplit15, gameLogs15, atBats15)
            results.add(hitterSeasonSplit15)
        }

        if (gameLogs30?.size() > 0) {
            List<AtBat> atBats30 = atbatService.getAtBats(player, gameLogs30)

            HitterSeasonSplit hitterSeasonSplit30 = HitterSeasonSplit.findOrCreateByHitterAndYearAndSplit(hitterSeason.hitter, hitterSeason.year, "30")
            updateHitterLog(hitterSeasonSplit30, gameLogs30, atBats30 )
            results.add(hitterSeasonSplit30)
        }

        return results

    }








    public List<HitterGameLog> getRecentGameLogs(Player theHitter, int ageDays) {

        Date theDate = new Date() - ageDays
        theDate.clearTime()

        Date today = new Date()


        return HitterGameLog.withCriteria {

            createAlias("game", "g")
            hitter {
                eq('id', theHitter.id)
            }
            between("g.startDate", theDate, today)
            eq('g.status', "Final")
            order("g.startDate", "desc")

        }
    }



    public List<HitterGameLog> getRecentGameLogsMax(HitterSeason hitterSeason, int max) {

        if (!hitterSeason) return null

        return HitterGameLog.withCriteria {

            createAlias("game", "g")
            hitter {
                eq('id', hitterSeason.hitter.id)
            }

            eq('year', hitterSeason.year)

            maxResults(max)
            order("g.startDate", "desc")

        }
    }




    public List<HitterGameLog> getGameLogsForDate(List<Player> hitters, Date date) {

        date.clearTime()

        Date tomorrow = date + 1
        tomorrow.clearTime()

        return HitterGameLog.withCriteria {

            createAlias("game", "g")
            hitter {
                'in'('id', hitters?.collect {it.id})
            }
            between("g.startDate", date, tomorrow)

        }

    }



    public HitterGameLog getGameLogForDate(Player theHitter, Date date) {

        date.clearTime()

        Date tomorrow = date + 1
        tomorrow.clearTime()

        return HitterGameLog.withCriteria {

            createAlias("game", "g")
            hitter {
                eq('id', theHitter.id)
            }
            between("g.startDate", date, tomorrow)

        }?.first()

    }




    public List<HitterGameLog> getGameLogsForSeasonBeforeDate(List<Player> hitters, int theYear, Date date) {

        date.clearTime()


        return HitterGameLog.withCriteria {

            createAlias("game", "g")
            hitter {
                'in'('id', hitters?.collect {it.id})
            }
            eq('year', theYear)
            lt("g.startDate", date)

        }

    }


    public List<AtBat> getAtBatsForSeasonBeforeDate(List<Player> hitters, int theYear, Date date) {

        date.clearTime()

        return AtBat.withCriteria {
            createAlias("game", "g")

            hitter {
                'in'('id', hitters.collect { it?.id})
            }
            eq('year', theYear)
            lt("g.startDate", date)
        }

    }



    public List<HitterGameLog> getGameLogsForSeason(List<Player> players, int year) {

        if (players?.size() == 0) return null


        return HitterGameLog.withCriteria {

            eq('year', year)
            hitter {
                'in'('id', players.collect { it?.id})
            }
        }

    }


    public List<AtBat> getAtBatsForSeason(List<Player> players, int year) {

        if (players?.size() == 0) return null


        return AtBat.withCriteria {

            eq('year', year)
            hitter {
                'in'('id', players.collect { it?.id})
            }
        }


    }



    public List<AtBat> getAtbats(HitterSeason hitterSeason) {

        AtBat.withCriteria {
            createAlias("game", "g")
            hitter {
                eq('id', hitterSeason.hitter.id)
            }

            eq('g.status', "Final")
            eq('year', hitterSeason.year)
        }

    }


    public List<AtBat> getHitterPitcherCareerAtBats(Player theHitter, Player thePitcher) {


        AtBat.withCriteria {
            createAlias("game", "g")
            hitter {
                eq('id', theHitter.id)
            }

            pitcher {
                eq('id', thePitcher.id)
            }
            eq('g.status', "Final")
        }

    }













    public Double calculateFanduelPoints(List<HitterLog> gameLogs) {

        if (gameLogs?.size() == 0) return 0


        int total = 0


        gameLogs?.each {
            total += calculateFanduelPoints(it)
        }

        return total
    }


    public Double calculateFanduelPoints(HitterLog hitterLog) {

        int singles = hitterLog.singles ?: 0
        int doubles = hitterLog.doubles ?: 0
        int triples = hitterLog.triples ?: 0
        int hr = hitterLog.homeRuns ?: 0
        int rbi = hitterLog.rbi ?: 0
        int r = hitterLog.runs ?: 0
        int bb = hitterLog.bb ?: 0
        int sb = hitterLog.sb ?: 0
        int hpb = hitterLog.hbp ? hitterLog.hbp : 0

        return statService.calculateHitterFanduelPoints(singles, doubles, triples, hr, rbi, r, bb, sb, hpb)

    }








    public HitterCareer updateHitterCareer(Player theHitter) {

        HitterCareer hitterCareer = getHitterCareer(theHitter)
        List<HitterSeason> hitterSeasons = getHitterSeasons(theHitter)
        List<AtBat> hitterAtBats = getHitterCareerAtBats(theHitter)
        List<HitterGameLog> hitterGameLogs = getHitterCareerGameLogs(theHitter)


        updateHitterLog(hitterCareer, hitterSeasons, hitterAtBats )


        if (hitterCareer.save(flush: true)) {
            println "Updated career for: " + hitterCareer.hitter.fullName
        } else {
            println "*******ERROR UPDATING CAREER FOR HITTER: " + hitterCareer.hitter.fullName
        }



        List<HitterCareerSplit> hitterCareerSplits = generateSplits(hitterCareer, hitterAtBats, hitterGameLogs)

        hitterCareerSplits?.each { split ->
            if (!split.save(flush:true)) {
                println "***************ERROR UPDATING CAREER SPLIT FOR HITTER *************************"
            }
        }




        return hitterCareer

    }



    public List<HitterSeasonSplit> getHitterSeasonSplits(Player theHitter, Integer year) {

        return HitterSeasonSplit.withCriteria {
            eq('year', year)
            hitter {
                eq('id', theHitter.id)
            }
        }
    }



    public List<HitterSeasonSplit> getHitterSeasonSplitsBeforeDate(Player theHitter, int year, Date date) {

        List<HitterGameLog> gameLogs = getGameLogsForSeasonBeforeDate([theHitter], year, date)
        List<AtBat> atbats = getAtBatsForSeasonBeforeDate([theHitter], year, date)

        HitterSeasonSplit hitterSeasonSplitToDate = new HitterSeasonSplit(hitter: theHitter, year: year)
        updateHitterLog(hitterSeasonSplitToDate, gameLogs, atbats)

        return hitterSeasonSplitToDate
    }








    public HitterCareer getHitterCareer(Player theHitter) {
        HitterCareer hitterCareer = HitterCareer.findOrCreateByHitter(theHitter)
        return hitterCareer
    }

    public HitterSeason getHitterSeason(Player theHitter, int year) {
        HitterSeason hitterSeason = HitterSeason.findByHitterAndYear(theHitter, year)
        return hitterSeason
    }

    public HitterSeason getHitterSeasonBeforeDate(Player theHitter, int year, Date date) {

        List<HitterGameLog> gameLogs = getGameLogsForSeasonBeforeDate([theHitter], year, date)
        List<AtBat> atbats = getAtBatsForSeasonBeforeDate([theHitter], year, date)

        HitterSeason hitterSeasonToDate = new HitterSeason(hitter: theHitter, year: year)
        updateHitterLog(hitterSeasonToDate, gameLogs, atbats)

        return hitterSeasonToDate
    }

    //public List<HitterSeasonSplit> getHitterSeasonSplitsBeforeDate()


    public List<HitterSeason> getHitterSeasons(Player theHitter) {
        List<HitterSeason> hitterSeasons = HitterSeason.findAllByHitter(theHitter)
        return hitterSeasons
    }

    public List<HitterCareerSplit> getHitterCareerSplits(Player theHitter) {
        List<HitterCareerSplit> hitterCareerSplits = HitterCareerSplit.findAllByHitter(theHitter)
        return hitterCareerSplits
    }

    public List<AtBat> getHitterCareerAtBats(Player theHitter) {
        List<AtBat> hitterAtBats = AtBat.findAllByHitter(theHitter)
        return hitterAtBats
    }

    public List<HitterGameLog> getHitterCareerGameLogs(Player theHitter) {
        List<HitterGameLog> hitterGameLogs = HitterGameLog.findAllByHitter(theHitter)
        return hitterGameLogs
    }












    public int getRecentAverageBattingOrder(Player player ) {

        List<HitterGameLog> gameLogs7 = getRecentGameLogs(player, 7)

        List<HitterGameLog> startingGameLogs = gameLogs7?.findAll { it.battingOrder?.toString()?.endsWith("00")  }

        if (startingGameLogs?.size() == 0) return 0

        return startingGameLogs?.collect { it.battingOrder ? (it.battingOrder / 100) : 0 }.sum() / startingGameLogs?.size()

    }







}
