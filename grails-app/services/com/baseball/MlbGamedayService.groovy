package com.baseball


import com.baseball.gameday.GamedayAtbat
import com.baseball.gameday.Batting
import com.baseball.gameday.BattingAppearance
import com.baseball.gameday.BoxScore
import com.baseball.gameday.GameUrls
import com.baseball.gameday.GamedayGame
import com.baseball.gameday.GamedayGameEvents
import com.baseball.gameday.GamedayPlayer
import com.baseball.gameday.GamedayPlayers
import com.baseball.gameday.LinescorePitcher
import com.baseball.gameday.Pitching
import com.baseball.gameday.PitchingAppearance
import com.mdimension.jchronic.Chronic

import grails.transaction.Transactional
import org.hibernate.StaleObjectStateException
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.jdbc.UncategorizedSQLException
import org.springframework.transaction.annotation.Propagation

import static groovyx.gpars.GParsPool.withPool


class MlbGamedayService {

    PlayerService playerService
    HitterService hitterService
    PitcherService pitcherService
    StatService statService
    ParkFactorService parkFactorService
    GameService gameService
    EtlService etlService


    static String baseURL = "http://gd2.mlb.com/components/game/mlb";


    public void processNew() {

        //Process all games since most recent game.
        List<Game> games = Game.listOrderByStartDate(max: 1, order: "desc")

        List<Date> dates = new ArrayList<>()


        Date startDate = games.get(0).startDate - 5
        Date endDate = new Date() + 4

        startDate.upto(endDate) {
            dates.add(it)
        }


        download(dates)

    }




    public void processSeasonStats(int year) {


        withPool(15) { pool ->

            int currentHitter = 0
            int currentPitcher = 0

            List<Player> hittersForYear = getHittersForYear(year)

            hittersForYear?.eachParallel { hitter ->
                hitterService.updateHitterSeasonStats(hitter, year)
                synchronized (currentHitter) {
                    currentHitter++
                    println currentHitter + " of " + hittersForYear.size() + " hitters"
                }

            }

            List<Player> pitchersForYear = getPitchersForYear(year)

            pitchersForYear?.eachParallel { pitcher ->
                pitcherService.updatePitcherSeasonStats(pitcher, year)
                synchronized (currentPitcher) {
                    currentPitcher++
                    println currentPitcher + " of " + pitchersForYear.size() + " pitchers"
                }
            }

        }



    }




    public void downloadSeason(int year) {

        println "***************************************************************"
        println "***************************************************************"
        println "************ DOWLOADING SEASON " + year + " *******************"
        println "***************************************************************"
        println "***************************************************************"


        Date startDate = Chronic.parse('march 1 ' + year)?.beginCalendar?.time
        Date endDate = Chronic.parse('november 23 ' + year)?.beginCalendar?.time

        if (endDate.after(new Date())) endDate = new Date()


        List<Date> dates = new ArrayList<>()
        startDate.upto(endDate) {
            dates.add(it)
        }


        dates.each { Date date ->
            download(date)
        }



        println "***************************************************************"
        println "***************************************************************"
        println "************ FINISHED DOWLOADING SEASON " + year + " **********"
        println "***************************************************************"
        println "***************************************************************"


    }




    public void download(List<Date> dates) {
        dates.each { Date date ->
            download(date)
        }
    }





    public void download(Date date) {

        println "************ FETCHING GAMES FOR DATE: " + date + " *******************"


        int year = date[Calendar.YEAR]
        int month = date[Calendar.MONTH] + 1
        int day = date[Calendar.DATE]

        String dateUrl = String.format("%s/year_%02d/month_%02d/day_%02d", baseURL, year, month, day);

        List<String> filePathsForDay = getFilePathsForDay(dateUrl)
        List<GameUrls> urlsForDay = getUrlsForDay(dateUrl, filePathsForDay)


        urlsForDay?.each {

            BoxScore boxScore = getBoxScore(it)
            GamedayGame game = getGame(it)
            GamedayPlayers players = getPlayers(it)
            GamedayGameEvents gamedayGameEvents = getGameEvents(it)

            if ("R".equals(game?.type) || "A".equals(game?.type)) {
                Game processedGame = process(boxScore,game, players, gamedayGameEvents)
            }

        }

    }


    private GamedayPlayers getPlayers(GameUrls gameUrls) {

        def slurper = new XmlSlurper()

        try {
            def parsedPlayers = slurper.parse(gameUrls.urls.get(FileType.PLAYERS).toString())
            return new GamedayPlayers(parsedPlayers)

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {}
        return null
    }


    private BoxScore getBoxScore(GameUrls gameUrls) {

        def slurper = new XmlSlurper()

        try {
            def parsedBoxScore = slurper.parse(gameUrls.urls.get(FileType.BOX_SCORE).toString())
            return new BoxScore(parsedBoxScore)
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {}

        return null

    }


    private GamedayGame getGame(GameUrls gameUrls) {

        def slurper = new XmlSlurper()

        try {
            def parsedLinescore = slurper.parse(gameUrls.urls.get(FileType.LINE_SCORE).toString())
            return new GamedayGame(parsedLinescore)
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {}

        return null

    }


    private GamedayGameEvents getGameEvents(GameUrls gameUrls) {

        def slurper = new XmlSlurper()

        try {
            def parsedGameEvents = slurper.parse(gameUrls.urls.get(FileType.INNING_ALL).toString())
            return new GamedayGameEvents(parsedGameEvents)

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {}
        return null
    }



    private List<String> getFilePathsForDay(String dateUrl) {

        List<String> gameFolders = new ArrayList<String>();
        try {
            Document doc = Jsoup.connect(dateUrl).post();
            Document parse = Jsoup.parse(doc.html());


            Elements lis = parse.select("li > a");
            for (Element li : lis) {
                String href = li.attr("href");
                if (href.startsWith("gid_")) {
                    gameFolders.add(href.replace("/", ""));
                }
            }
        } catch (IOException e) {

        }

        return gameFolders
    }


    private List<GameUrls> getUrlsForDay(String dateUrl, List<String> filePathsForDay) {

        List<GameUrls> urlsForDay = new ArrayList<>()

        for (String filePath : filePathsForDay) {
            String directory = String.format("%s/%s", dateUrl, filePath);


            GameUrls gameUrls = new GameUrls()
            gameUrls.gameId = filePath

            for (FileType fileType : FileType.list()) {
                URL url = null;

//                if (fileType.fileName.contains("inning_")) {
//                    url = new URL(String.format("%s/%s/%s", directory, "inning", fileType.fileName));
//                } else {
                    url = new URL(String.format("%s/%s", directory, fileType.fileName));
//                }

                gameUrls.urls.put(fileType, url)

            }


            urlsForDay.add(gameUrls)

        }

        return urlsForDay

    }



    @Transactional(propagation = Propagation.REQUIRED)
    private Game process(BoxScore boxScore, GamedayGame gamedayGame, GamedayPlayers gamedayPlayers, GamedayGameEvents gameEvents) {

        Long gameId = getGame(gamedayGame, boxScore)

        Game game = Game.get(gameId)


        if (!game) {
            println "********************* SKIPPING GAME. NOT SURE WHY ***********************"
            return
        }

        if (!gameEvents) {
            println "***************NO GAME EVENTS*****************"
        }

        //Update all players available in the players list. Only available after game starts.
        updatePlayers(gamedayPlayers, game.startDate)


        deletePitchesForGame(game)
        deleteRunnersForGame(game)
        deleteAtBatsForGame(game)



        /*
         * Process at bats
         */

        List<AtBat> atBats = new ArrayList<>()
        if (gameEvents?.atBats?.size() > 0) {

            atBats = updateGameAtBats(game, gameEvents.atBats)

            //Get the starting pitchers
            game.homeStartingPitcher = getHomeStarter(gameEvents.atBats)
            game.awayStartingPitcher = getAwayStarter(gameEvents.atBats)
        }


        /**
         * Process box score
         */
        if (boxScore) {
            //Pitching
            updatePitchingAppearances(boxScore.pitching, game.id, atBats)


            //Batting
            updateBattingAppearances(boxScore.batting, game.id, atBats)

            game.awayTeamRuns = boxScore.awayTeamRuns
            game.homeTeamRuns = boxScore.homeTeamRuns
            game.awayTeamHits = boxScore.awayTeamHits
            game.homeTeamHits = boxScore.homeTeamHits
            game.awayTeamErrors = boxScore.awayTeamErrors
            game.homeTeamErrors = boxScore.homeTeamErrors

        }





        if (!game.save(flush: true)) {
            println "************ ERROR UPDATING GAME " + game.textDescription +" ******************"
        }

        return game

    }



    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteAtBatsForGame(Game theGame) {

        boolean success = false

        while (!success) {
            try {

                etlService.deletePitchFactsForGame(theGame)
                etlService.deleteRunnerFactsForGame(theGame)


                List<AtBat> atBats = AtBat.withCriteria {
                    createAlias("game", "g")
                    eq("g.gamePk", theGame.gamePk)
                }

                atBats*.delete()

                success = true
            } catch (Exception ex) {
                println "---------------ATBAT DELETE DEADLOCK. WILL RETRY--------------------"
            }
        }


    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deletePitchesForGame(Game theGame) {

        boolean success = false

        while (!success) {
            try {

                List<Pitch> pitches = Pitch.withCriteria {
                    createAlias("atBat", "a")
                    createAlias("a.game", "g")
                    eq("g.gamePk", theGame.gamePk)
                }

                pitches*.delete()

                success = true
            } catch (Exception ex) {
                println "---------------PITCH DELETE DEADLOCK. WILL RETRY--------------------"
            }
        }


    }



    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteRunnersForGame(Game theGame) {

        boolean success = false

        while (!success) {
            try {

                List<Runner> runners = Runner.withCriteria {
                    createAlias("atBat", "a")
                    createAlias("a.game", "g")
                    eq("g.gamePk", theGame?.gamePk)
                }

                runners*.delete()


//                AtBat.executeUpdate("delete AtBat where game.id = :gameId", [gameId: game.id])
                success = true
            } catch (Exception ex) {
                println "---------------RUNNER DELETE DEADLOCK. WILL RETRY--------------------"
            }
        }


    }





    @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = [Exception.class, OptimisticLockingFailureException.class, StaleObjectStateException.class, UncategorizedSQLException.class, IllegalArgumentException.class])
    private List<AtBat> updateGameAtBats(Game game, List<GamedayAtbat> atBats) {

        println String.format("Saving %1s atbats for game: %3s", atBats?.size(), game.id)

        List<AtBat> results = new ArrayList<>()

        //Insert new list
        atBats?.each { gamedayAtbat ->
            results.add(updateAtBat(game, gamedayAtbat))
        }

        return results;



    }

    @Transactional(propagation = Propagation.REQUIRED)
    public AtBat updateAtBat(Game game, GamedayAtbat gamedayAtbat) {

        AtBat atBat = new AtBat(game: game)
        atBat.eventNum = gamedayAtbat.eventNum
        atBat.year = game.startDate[Calendar.YEAR]
        atBat.inningNum = gamedayAtbat.inningNum
        atBat.inningTop = gamedayAtbat.inningTop


        atBat.hitter = Player.findByGamedayId(gamedayAtbat.batterId)
        atBat.pitcher = Player.findByGamedayId(gamedayAtbat.pitcherId)

        atBat.pitcherHandedness = atBat.pitcher.pitches


        if ("S".equalsIgnoreCase(atBat.hitter.bats)) {
            atBat.hitterHandedness = "L".equals(atBat.pitcherHandedness) ? "R" : "L"
        } else {
            atBat.hitterHandedness = atBat.hitter.bats
        }




        if (atBat.inningTop) {
            atBat.hitterHomeAway = "A"
            atBat.pitcherHomeAway = "H"
        } else {
            atBat.hitterHomeAway = "H"
            atBat.pitcherHomeAway = "A"
        }


        atBat.startTimeUtc = gamedayAtbat.startTimeUtc

        atBat.num = gamedayAtbat.num
        atBat.balls = gamedayAtbat.balls
        atBat.strikes = gamedayAtbat.strikes
        atBat.outs = gamedayAtbat.outs

        atBat.description = gamedayAtbat.description
        atBat.event = gamedayAtbat.event

        atBat.score = gamedayAtbat.score
        atBat.awayTeamRuns = gamedayAtbat.awayTeamRuns
        atBat.homeTeamRuns = gamedayAtbat.homeTeamRuns

        atBat.baseRunner1Id = gamedayAtbat.baseRunner1Id
        atBat.baseRunner2Id = gamedayAtbat.baseRunner2Id
        atBat.baseRunner3Id = gamedayAtbat.baseRunner3Id

        atBat.rbi = gamedayAtbat.rbi
        atBat.earnedRuns = gamedayAtbat.earnedRun

        atBat.pc = gamedayAtbat.pitches?.size()
        atBat.pitchesStrikes = gamedayAtbat.pitches?.count { "S".equals(it.type)}
        atBat.pitchesBalls = gamedayAtbat.pitches?.count { "B".equals(it.type)}

        try {
            if (atBat.validate()) {
                if (!atBat.save(flush: true)) {
                    println(String.format("********** ERROR SAVING AT BAT ***********: %1s vs %2s", atBat.hitter.fullName, atBat.pitcher.fullName))
                }
            } else {
                println(String.format("********** ATBAT FAILED VALIDATION ***********: %1s vs %2s", atBat.hitter.fullName, atBat.pitcher.fullName))
            }
        } catch (Exception ex) {
            ex.printStackTrace()
            println(String.format("********** ERROR SAVING AT BAT ***********: %1s vs %2s", atBat.hitter.fullName, atBat.pitcher.fullName))
        }



        //Update pitches
        gamedayAtbat?.pitches?.each { gamedayPitch ->
            Pitch p = new Pitch()
            p.atBat = atBat

            p.sequence       = gamedayPitch.sequence
            p.sportsvisionId = gamedayPitch.sportsvisionId
            p.description   = gamedayPitch.description
            p.startSpeed    = gamedayPitch.startSpeed
            p.endSpeed      = gamedayPitch.endSpeed
            p.type          = gamedayPitch.type
            p.pitchType     = gamedayPitch.pitchType

            p.tfs           = gamedayPitch.tfs
            p.y             = gamedayPitch.y
            p.eventNum      = gamedayPitch.eventNum
            p.on1b          = gamedayPitch.on1b
            p.on2b          = gamedayPitch.on2b
            p.on3b          = gamedayPitch.on3b
            p.playGuid      = gamedayPitch.playGuid
            p.szTop         = gamedayPitch.szTop
            p.szBot         = gamedayPitch.szBot
            p.pfxX          = gamedayPitch.pfxX
            p.pfxZ          = gamedayPitch.pfxZ
            p.px            = gamedayPitch.px
            p.pz            = gamedayPitch.pz
            p.x0            = gamedayPitch.x0
            p.y0            = gamedayPitch.y0
            p.z0            = gamedayPitch.z0
            p.vx0           = gamedayPitch.vx0
            p.vy0           = gamedayPitch.vy0
            p.vz0           = gamedayPitch.vz0
            p.ax            = gamedayPitch.ax
            p.ay            = gamedayPitch.ay
            p.az            = gamedayPitch.az
            p.breakY        = gamedayPitch.breakY
            p.breakAngle    = gamedayPitch.breakAngle
            p.breakLength   = gamedayPitch.breakLength
            p.typeConfidence  = gamedayPitch.typeConfidence
            p.zone          = gamedayPitch.zone
            p.nasty         = gamedayPitch.nasty
            p.spinDir       = gamedayPitch.spinDir
            p.spinRate      = gamedayPitch.spinRate


            if (!p.save(flush:true)) {
                println "^^^^^^^^^^^^^^^^^^ERROR SAVING PITCH^^^^^^^^^^^^^^^^^^^^^^^^^^^"
            }

        }

        gamedayAtbat?.runners?.each { gamedayRunner ->
            Runner runner = new Runner()
            runner.atBat = atBat

            runner.start = gamedayRunner.start
            runner.end = gamedayRunner.end
            runner.event = gamedayRunner.event
            runner.score = (gamedayRunner.score)
            runner.earned = (gamedayRunner.earned)
            runner.eventNum = gamedayRunner.eventNum

            if (!runner.save(flush:true)) {
                println "^^^^^^^^^^^^^^^^^ERROR SAVING RUNNER^^^^^^^^^^^^^^^^^^^^^^^^"
            }

        }



        return atBat
    }





    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Long getGame(GamedayGame gamedayGame, BoxScore boxScore) {

        Game game = Game.findByGamePk(gamedayGame.primaryKey)


        if (!game) {
            game = new Game()
        }

        game.gameId = gamedayGame.id


        //Process game
        game.year = gamedayGame.timeDate[Calendar.YEAR]
        game.gamePk = gamedayGame.primaryKey
        game.status = gamedayGame.status
        game.type = gamedayGame.type
        game.startDate = gamedayGame.timeDate
        game.venueWeatherChannelCode = gamedayGame.venueWeatherChannelCode

        game.homeTeam= getHomeTeam(gamedayGame, boxScore)
        game.awayTeam = getAwayTeam(gamedayGame, boxScore)


        //Sometimes info is missing from the Gamedaygame. Try to get it from the boxscore
        if (boxScore) {
            if (!game.gameId) game.gameId = boxScore.gameId
            if (!game.gamePk) game.gamePk = boxScore.gamePk
            if (!game.status) game.status = boxScore.status
            if (!game.startDate) game.startDate = boxScore.date


            Batting homeBatting = boxScore?.batting?.find { it.teamFlag == "home"}
            Batting awayBatting = boxScore?.batting?.find { it.teamFlag == "away"}




            if (homeBatting?.appearances?.size() > 0 && homeBatting?.appearances?.findAll{ it.battingOrder != null}?.size() > 0) {
                game.homeLineupPopulated = true
            }

            if (awayBatting?.appearances?.size() > 0 && awayBatting?.appearances?.findAll{ it.battingOrder != null}?.size() > 0) {
                game.awayLineupPopulated = true
            }


        }


        println "Processsing game: (" + game.startDate + ") " + game.homeTeam.name + " / " + game.awayTeam.name


        if (gamedayGame.homeProbablePitcher) {
            game.homeProbablePitcher = playerService.findOrCreatePitcher(gamedayGame.homeProbablePitcher, game.homeTeam)
        }

        if (gamedayGame.awayProbablePitcher) {
            game.awayProbablePitcher = playerService.findOrCreatePitcher(gamedayGame.awayProbablePitcher, game.awayTeam)
        }

        if (gamedayGame.winningPitcher) {
            game.winningPitcher = playerService.findOrCreatePitcher(gamedayGame.winningPitcher,  null)
        }

        if (gamedayGame.losingPitcher) {
            game.losingPitcher = playerService.findOrCreatePitcher(gamedayGame.losingPitcher, null)
        }

        if (gamedayGame.savePitcher) {
            game.savePitcher = playerService.findOrCreatePitcher(gamedayGame.savePitcher, null)
        }


        game.preAllstar = gameService.isPreAllStar(game)


        game.save(flush: true)

        Long id = game.id

        return id
    }




    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void updateBattingAppearances(List<Batting> batting, Long gameId, List<AtBat> gameAtBats) {
        batting.each { b ->
            String teamFlag = b.teamFlag
            b.appearances.each { a ->

                String hitterGamedayId = a.playerId
                List<AtBat> playerAtbats = gameAtBats.findAll { it.hitter.gamedayId == hitterGamedayId}


                updateHitterGameLog(a, teamFlag, gameId, playerAtbats)
            }
        }
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void updatePitchingAppearances(List<Pitching> pitching, Long gameId, List<AtBat> gameAtBats) {

        Player homeStarter = getHomeStarterFromAtBats(gameAtBats)
        Player awayStarter = getAwayStarterFromAtBats(gameAtBats)


        pitching.each { p ->
            String teamFlag = p.teamFlag

            p.appearances.each { a ->

                String pitcherGamedayId = a.playerId
                List<AtBat> playerAtbats = gameAtBats.findAll { it.pitcher.gamedayId == pitcherGamedayId}

                boolean isStart = (homeStarter?.gamedayId == pitcherGamedayId || awayStarter?.gamedayId == pitcherGamedayId)

                updatePitcherGameLog(a, teamFlag, gameId, playerAtbats, isStart)
            }
        }
    }



    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void updatePlayers(GamedayPlayers gamedayPlayers, Date startDate) {
        gamedayPlayers?.playerList.each {
            updatePlayer(it, startDate)
        }
    }



    @Transactional(propagation = Propagation.REQUIRED)
    private void updateHitterGameLog(BattingAppearance battingAppearance, String teamFlag, Long gameId,  List<AtBat> atBats) {

        Player theHitter = Player.findByGamedayId(battingAppearance.playerId)
        Game game = Game.get(gameId)

        boolean isNew = false

        HitterGameLog hitterGameLog = HitterGameLog.findByHitterAndGame(theHitter, game)

        if (!hitterGameLog) {
            isNew = true
            hitterGameLog = new HitterGameLog(game: game, hitter: theHitter)
        }



        if (teamFlag.equals("home")) {
            hitterGameLog.opponent = game.awayTeam
            hitterGameLog.team = game.homeTeam
        } else {
            hitterGameLog.opponent = game.homeTeam
            hitterGameLog.team = game.awayTeam
        }

        hitterGameLog.clearAll()

        hitterGameLog.year = game.startDate[Calendar.YEAR]
        hitterGameLog.gamedayPopulated = true
        hitterGameLog.pa = atBats?.size() ?: 0
        hitterGameLog.battingOrder = battingAppearance.battingOrder ?: 0
        hitterGameLog.hits = battingAppearance.hits ?: 0
        hitterGameLog.singles = battingAppearance.hits - battingAppearance.hr - battingAppearance.triples - battingAppearance.doubles
        hitterGameLog.doubles = battingAppearance.doubles ?: 0
        hitterGameLog.triples = battingAppearance.triples ?: 0
        hitterGameLog.homeRuns = battingAppearance.hr ?: 0
        hitterGameLog.runs = battingAppearance.runs ?: 0
        hitterGameLog.rbi = battingAppearance.rbi ?: 0
        hitterGameLog.bb = battingAppearance.bb ?: 0
        hitterGameLog.hbp = battingAppearance.hbp ?: 0
        hitterGameLog.sb = battingAppearance.sb ?: 0
        hitterGameLog.cs = battingAppearance.cs ?: 0
        hitterGameLog.avg = battingAppearance.avg ?: 0
        hitterGameLog.atBats = battingAppearance.atBats ?: 0

        hitterGameLog.hbp = battingAppearance.hbp ?: 0
        hitterGameLog.so = battingAppearance.so ?: 0
        hitterGameLog.lob  = battingAppearance.lob ?: 0
        hitterGameLog.sacBunts = battingAppearance.sacBunts ?: 0
        hitterGameLog.sacFlys = battingAppearance.sacFlys ?: 0
        hitterGameLog.groundOuts = battingAppearance.groundOuts ?: 0
        hitterGameLog.flyOuts = battingAppearance.flyOuts ?: 0
        hitterGameLog.gidp = battingAppearance.gidp ?: 0
        hitterGameLog.po = battingAppearance.po ?: 0
        hitterGameLog.assists = battingAppearance.assists ?: 0
        hitterGameLog.e = battingAppearance.errors ?: 0


        atBats?.each {
            if (it.isIBB()) hitterGameLog.ibb = statService.incrementField(hitterGameLog.ibb)
            if (it.isLineOut()) hitterGameLog.lineOuts = statService.incrementField(hitterGameLog.lineOuts)
            if (it.isGroundBall()) hitterGameLog.groundBalls = statService.incrementField(hitterGameLog.groundBalls)
            if (it.isLineDrive()) hitterGameLog.lineDrives = statService.incrementField(hitterGameLog.lineDrives)
            if (it.isFlyBall()) hitterGameLog.flyBalls = statService.incrementField(hitterGameLog.flyBalls)
        }


        hitterGameLog.fanduelPoints = hitterService.calculateFanduelPoints(hitterGameLog)






        parkFactorService.updateParkAdjustedStats(hitterGameLog)
        parkFactorService.calculateParkAdjustedStats(hitterGameLog)


        if (!hitterGameLog.save(flush: true)) {
            println "******* PROBLEM SAVING HITTER GAME LOG ***********"
        }



//        playerService.updateHitterSeasonStats(hitter, game.startDate[Calendar.YEAR])



    }






    @Transactional(propagation = Propagation.REQUIRED)
    private void updatePitcherGameLog(PitchingAppearance pitchingAppearance, String teamFlag, Long gameId, List<AtBat> atBats, boolean isStart) {

        Player pitcher = Player.findByGamedayId(pitchingAppearance.playerId)
        Game game = Game.get(gameId)



        boolean isNew = false

        PitcherGameLog pitcherGameLog = PitcherGameLog.findByPitcherAndGame(pitcher, game)

        if (!pitcherGameLog) {
            isNew = true
            pitcherGameLog = new PitcherGameLog(game: game, pitcher: pitcher)
        }


        pitcherGameLog.year = game.startDate[Calendar.YEAR]
        pitcherGameLog.gamedayPopulated = true

        if (teamFlag.equals("home")) {
            pitcherGameLog.opponent = game.awayTeam
            pitcherGameLog.team = game.homeTeam
        } else {
            pitcherGameLog.opponent = game.homeTeam
            pitcherGameLog.team = game.awayTeam
        }


        pitcherGameLog.clearAll()

        pitcherGameLog.games = 1
        pitcherGameLog.starts = isStart ? 1 : 0
        pitcherGameLog.wins = pitchingAppearance.won ? 1 : 0
        pitcherGameLog.losses = pitchingAppearance.lost ? 1 : 0
        pitcherGameLog.saves = pitchingAppearance.saved ? 1 : 0
        pitcherGameLog.bs = pitchingAppearance.blewSave ? 1 : 0


        pitcherGameLog.outs = pitchingAppearance.outs ?: 0
        pitcherGameLog.er = pitchingAppearance.earnedRuns ?: 0
        pitcherGameLog.so = pitchingAppearance.so ?: 0
        pitcherGameLog.h = pitchingAppearance.hits ?: 0
        pitcherGameLog.bb = pitchingAppearance.bb ?: 0
        //pitcherGameLog.sho = pitchingAppearance. TODO:// FIGURED OUT HOW TO POPULATE
        //pitcherGameLog.cg = pitchingAppearance.
        pitcherGameLog.pc = pitchingAppearance.numberOfPitches ?: 0


        pitcherGameLog.battersFace = pitchingAppearance.battersFace ?: 0
        pitcherGameLog.strikes = pitchingAppearance.strikes ?: 0
        pitcherGameLog.runs = pitchingAppearance.runs ?: 0
        pitcherGameLog.hr = pitchingAppearance.hr ?: 0







        //Park adjusted values
        ParkFactor pf = ParkFactor.findByTeam(pitcherGameLog.game.homeTeam)





        atBats?.each {


            if (it.isHpb()) pitcherGameLog.hbp = statService.incrementField(pitcherGameLog.hbp)
            if (it.isSingle()) pitcherGameLog.singles = statService.incrementField(pitcherGameLog.singles)
            if (it.isDouble()) pitcherGameLog.doubles = statService.incrementField(pitcherGameLog.doubles)
            if (it.isTriple()) pitcherGameLog.triples = statService.incrementField(pitcherGameLog.triples)


            if (it.isFlyOut()) pitcherGameLog.flyOuts = statService.incrementField(pitcherGameLog.flyOuts)
            if (it.isGroundOut()) pitcherGameLog.groundOuts = statService.incrementField(pitcherGameLog.groundOuts)
            if (it.isLineOut()) pitcherGameLog.lineOuts = statService.incrementField(pitcherGameLog.lineOuts)
            if (it.isGroundBall()) pitcherGameLog.groundBalls = statService.incrementField(pitcherGameLog.groundBalls)
            if (it.isLineDrive()) pitcherGameLog.lineDrives = statService.incrementField(pitcherGameLog.lineDrives)
            if (it.isFlyBall()) pitcherGameLog.flyBalls = statService.incrementField(pitcherGameLog.flyBalls)



            parkFactorService.updateParkAdjustedStats(pitcherGameLog, it)



        }




        pitcherGameLog.fanduelPoints = statService.calculateFanduelPoints(pitcherGameLog)



        if (!pitcherGameLog.save(flush: true)) {
            println "******* PROBLEM SAVING PITCHER GAME LOG ***********"
        }


    }






    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void updatePlayer(GamedayPlayer gamedayPlayer, Date gameDate) {

        Player player = Player.findByGamedayId(gamedayPlayer.playerId)

        boolean isNew = false

        if (!player) {
            isNew = true
            player = new Player()
            player.gamedayId = gamedayPlayer.playerId
        }

        if (isNew || gameDate.after(player.lastUpdated)) {
            player.firstName = gamedayPlayer.firstName
            player.lastName = gamedayPlayer.lastName
            player.currentTeam = Team.findByGamedayId(gamedayPlayer.teamId)

            if (player.currentTeam == null) {
                player.currentTeam = Team.findByAbbrev(gamedayPlayer.gamedayTeamId)
            }



            player.pitches = gamedayPlayer.pitches
            player.bats = gamedayPlayer.bats
            player.position = gamedayPlayer.position
            player.currentPosition = gamedayPlayer.currentPosition

            if (player.save(flush: true)) {
                if (isNew) {
                    println "Adding player: (" + player.gamedayId + ") " + player.firstName + " " + player.lastName
                } else {
                    println "Updating player: (" + player.gamedayId + ") " + player.firstName + " " + player.lastName
                }
            } else {
                println "******* PROBLEM ADDING PLAYER ******* " + player.gamedayId
            }



        }



    }


    private Player getPitcher(LinescorePitcher linescorePitcher, Team team) {


        Player pitcher = Player.findByGamedayId(linescorePitcher.playerId)

        if (!pitcher) {
            println "Adding pitcher: " + linescorePitcher.playerId

            pitcher = new Player()
            pitcher.gamedayId = linescorePitcher.playerId
            pitcher.position = "P"
            pitcher.currentPosition = "P"
            pitcher.currentTeam = team
            pitcher.firstName = linescorePitcher.firstName
            pitcher.lastName = linescorePitcher.lastName
            pitcher.displayName = linescorePitcher.displayName
            pitcher.save(flush: true)
        }


        return pitcher

    }



    private Team getAwayTeam(GamedayGame game, BoxScore boxScore) {

        if (!game.awayTeamId) game.awayTeamId = boxScore?.awayId


        Team team = Team.findByGamedayId(game.awayTeamId)

        if (!team) {
            println "Adding team: " + game.awayTeamName
            team = new Team()
            team.gamedayId = game.awayTeamId
            team.name = game.getAwayTeamName()
            team.abbrev = game.getAwayNameAbbrev()
            team.city = game.getAwayTeamCity()
            team.code = game.getAwayCode()
            team.save(flush: true)
        }

        return team

    }


    private Team getHomeTeam(GamedayGame game, BoxScore boxScore) {

        if (!game.awayTeamId) game.homeTeamId = boxScore?.homeId

        Team team = Team.findByGamedayId(game.homeTeamId)

        if (!team) {
            println "Adding team: " + game.homeTeamName
            team = new Team()
            team.gamedayId = game.homeTeamId
            team.name = game.getHomeTeamName()
            team.abbrev = game.getHomeNameAbbrev()
            team.city = game.getHomeTeamCity()
            team.code = game.getHomeCode()
            team.save(flush: true)
        }


        return team

    }


    private List<Player> getHittersForYear(int year) {

        def criteria = HitterGameLog.createCriteria()
        def hitters = criteria.listDistinct() {
            projections {
                groupProperty('hitter')
            }

            eq('year', year)
        }

        return hitters

    }


    private List<Player> getPitchersForYear(int year) {

        def criteria = PitcherGameLog.createCriteria()
        def pitchers = criteria.listDistinct() {
            projections {
                groupProperty('pitcher')
            }

            eq('year', year)
        }

        return pitchers

    }

    private Player getAwayStarter(List<GamedayAtbat> atbats) {

        if (!atbats || atbats.size() == 0) return null

        //Get the starting pitchers
        List<GamedayAtbat> homeAtbats = atbats.findAll {
            it.inningTop == false
        }

        GamedayAtbat firstAtBat = homeAtbats.min {
            it.num
        }

        if (!firstAtBat) return null

        return Player.findByGamedayId(firstAtBat?.pitcherId)

    }


    private Player getHomeStarter(List<GamedayAtbat> atbats) {

        if (!atbats || atbats.size() == 0) return null

        //Get the starting pitchers
        List<GamedayAtbat> awayAtbats = atbats.findAll {
            it.inningTop == true
        }

        GamedayAtbat firstAtBat = awayAtbats.min {
            it.num
        }

        if (!firstAtBat) return null

        return Player.findByGamedayId(firstAtBat?.pitcherId)

    }


    private Player getAwayStarterFromAtBats(List<AtBat> atbats) {

        if (!atbats || atbats.size() == 0) return null

        //Get the starting pitchers
        List<GamedayAtbat> homeAtbats = atbats.findAll {
            it.inningTop == false
        }

        AtBat firstAtBat = homeAtbats.min {
            it.num
        }

        if (!firstAtBat) return null

        return Player.findByGamedayId(firstAtBat?.pitcher.gamedayId)

    }


    private Player getHomeStarterFromAtBats(List<AtBat> atbats) {

        if (!atbats || atbats.size() == 0) return null

        //Get the starting pitchers
        List<AtBat> awayAtbats = atbats.findAll {
            it.inningTop == true
        }

        AtBat firstAtBat = awayAtbats.min {
            it.num
        }

        if (!firstAtBat) return null

        return Player.findByGamedayId(firstAtBat?.pitcher.gamedayId)

    }





    //creates an array list of lists with max size of batchSize.
    //If I pass in a batch size of 3 it will convert [1,2,3,4,5,6,7,8] into [[1,2,3],[4,5,6],[7,8]]
    //see http://stackoverflow.com/questions/2924395/groovy-built-in-to-split-an-array-into-equal-sized-subarrays
    //and http://stackoverflow.com/questions/3147537/split-collection-into-sub-collections-in-groovy
    def batchChunks(theList, batchSize) {
        if (!theList) return [] //return and empty list if its already empty

        def batchedList = []
        int chunkCount = theList.size() / batchSize

        chunkCount.times { chunkNum ->
            def start = chunkNum * batchSize
            def end = start + batchSize - 1
            batchedList << theList[start..end]
        }

        if (theList.size() % batchSize){
            batchedList << theList[chunkCount * batchSize..-1]
        }
        return batchedList
    }









}
