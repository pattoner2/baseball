package com.baseball

import com.baseball.AtBat
import com.baseball.HitterGameLog
import com.baseball.ParkFactor
import com.baseball.ParkFactorValues
import com.baseball.PitcherGameLog
import com.baseball.Team
import com.baseball.domain.HitterLog
import com.baseball.domain.PitcherLog
import grails.plugin.cache.Cacheable
import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation


class ParkFactorService {



    ProjectionService projectionService
    StatService statService

    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(value="parkFactorValuesForHandednessAndTeam", key = "#team.id.toString() + '-' + #hitterHandedness + '-' + #pitcherHandedness")
    public ParkFactorValues getParkFactorValuesForHandednessAndTeam(Team team, String hitterHandedness, String pitcherHandedness) {
        ParkFactor pf = ParkFactor.findByTeam(team)
        ParkFactorValues pfv = getParkFactorForHandedness(pf, hitterHandedness, pitcherHandedness)
        return pfv
    }



    @Transactional(propagation = Propagation.REQUIRED)
    public ParkFactorValues getParkFactorForHandedness(ParkFactor pf, String hitterHandedness, String pitcherHandedness){

        if ("S".equals(hitterHandedness)) {
            //Pretend switch hitters are going to be hitting from their ideal side.
            hitterHandedness = "L".equals(pitcherHandedness) ? "R" : "L"
        }

        ParkFactorValues pfv = new ParkFactorValues()

        switch (hitterHandedness) {
            case "L":
                pfv.singles = projectionService.weightAverage(pf.l1b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.doubles = projectionService.weightAverage(pf.l2b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.triples = projectionService.weightAverage(pf.l3b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.homeRuns = projectionService.weightAverage(pf.lHr, projectionService.PARK_FACTOR_WEIGHT)
                pfv.runs = projectionService.weightAverage(pf.lRuns, projectionService.PARK_FACTOR_WEIGHT)
                pfv.rbi = projectionService.weightAverage(pf.lRuns, projectionService.PARK_FACTOR_WEIGHT)
                break;
            case "R":
                pfv.singles = projectionService.weightAverage(pf.r1b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.doubles = projectionService.weightAverage(pf.r2b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.triples = projectionService.weightAverage(pf.r3b, projectionService.PARK_FACTOR_WEIGHT)
                pfv.homeRuns = projectionService.weightAverage(pf.rHr, projectionService.PARK_FACTOR_WEIGHT)
                pfv.runs = projectionService.weightAverage(pf.rRuns, projectionService.PARK_FACTOR_WEIGHT)
                pfv.rbi = projectionService.weightAverage(pf.rRuns, projectionService.PARK_FACTOR_WEIGHT)
                break;
        }

        return pfv


    }



    @Transactional(propagation = Propagation.REQUIRED)
    public void updateParkAdjustedStats(HitterGameLog hitterLog) {

        ParkFactor pf = ParkFactor.findByTeam(hitterLog.game.homeTeam)

        ParkFactorValues pfv = getParkFactorForHandedness(pf, hitterLog.hitter.bats, hitterLog?.opponentPitcher?.pitches)


        hitterLog.paSingles = adjustToPark(hitterLog.singles, pfv.singles)
        hitterLog.paDoubles = adjustToPark(hitterLog.doubles, pfv.doubles)
        hitterLog.paTriples = adjustToPark(hitterLog.triples, pfv.triples)
        hitterLog.paHomeRuns = adjustToPark(hitterLog.homeRuns, pfv.homeRuns)
        hitterLog.paRuns     = adjustToPark(hitterLog.runs, pfv.runs)
        hitterLog.paRbi      = adjustToPark(hitterLog.rbi, pfv.rbi)

        hitterLog.paHits = hitterLog.paSingles + hitterLog.paDoubles + hitterLog.paTriples + hitterLog.paHomeRuns
        hitterLog.paAvg = hitterLog.pa > 0 ? hitterLog.paHits / hitterLog.pa : 0

        hitterLog.paBb = hitterLog.bb
        hitterLog.paSb =  hitterLog.sb
        hitterLog.paCs = hitterLog.cs
        hitterLog.paHbp = hitterLog.hbp
        hitterLog.paSo = hitterLog.so
        hitterLog.paSacBunts = hitterLog.sacBunts
        hitterLog.paSacFlys = hitterLog.sacFlys
        hitterLog.paIbb = hitterLog.ibb

    }



    @Transactional(propagation = Propagation.REQUIRED)
    public void updateParkAdjustedStats(PitcherLog pitcherLog, AtBat atBat) {


        Team homeTeam = atBat.game.homeTeam
        String bats = atBat.hitter.bats
        String pitches = pitcherLog.pitcher.pitches

        ParkFactorValues pfv = getParkFactorValuesForHandednessAndTeam(homeTeam, bats, pitches)

        updateParkAdjustedStats(pfv, pitcherLog, atBat)

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateParkAdjustedStats(ParkFactorValues pfv, PitcherLog pitcherLog, AtBat atBat) {
        if ( atBat.isSingle ( ) ) pitcherLog.paSingles += adjustToPark ( 1, pfv.singles ) ?: 0
        if ( atBat.isDouble ( ) ) pitcherLog.paDoubles += adjustToPark ( 1, pfv.doubles ) ?: 0
        if ( atBat.isTriple ( ) ) pitcherLog.paTriples += adjustToPark ( 1, pfv.triples ) ?: 0
        if ( atBat.isHr ( ) )     pitcherLog.paHr      += adjustToPark ( 1, pfv.homeRuns ) ?: 0

        if (atBat.rbi > 0) {
            pitcherLog.paRuns     += adjustToPark(atBat.earnedRuns ?: 0 ,pfv.runs ) ?: 0
            pitcherLog.paRbi    =pitcherLog.paRuns
        }

    }









    @Transactional(propagation = Propagation.REQUIRED)
    public void calculateParkAdjustedStats(HitterLog hitterLog) {


        //Park adjusted calcs
        hitterLog.paTb = statService.calculateTotalBases(hitterLog.paSingles ?: 0, hitterLog.paDoubles ?: 0, hitterLog.paTriples ?: 0, hitterLog.paHomeRuns ?: 0)
        hitterLog.paObp = statService.calculateObp(hitterLog.paHits ?: 0, hitterLog.paBb ?: 0, hitterLog.paHbp ?: 0, hitterLog.atBats ?: 0, hitterLog.sacFlys ?: 0)
        hitterLog.paSlg = statService.calculateSlugging(hitterLog.paTb ?: 0, hitterLog.atBats ?: 0 )
        hitterLog.paOps = hitterLog.paObp + hitterLog.paSlg
        hitterLog.paWOba = statService.calculateWOba(hitterLog.bb ?: 0, hitterLog.hbp ?: 0, hitterLog.paSingles ?: 0, hitterLog.paDoubles ?: 0, hitterLog.paTriples ?: 0, hitterLog.paHomeRuns ?: 0, hitterLog.atBats ?: 0)
        hitterLog.paWFda = statService.calculateWfda(hitterLog.bb ?: 0, hitterLog.hbp ?: 0, hitterLog.paSingles ?: 0, hitterLog.paDoubles ?: 0, hitterLog.paTriples ?: 0,  hitterLog.paHomeRuns ?: 0, hitterLog.paRbi ?: 0, hitterLog.paRuns ?: 0, hitterLog.sb ?: 0, 0, hitterLog.pa ?: 0)

        hitterLog.paAvg = 0
        if (hitterLog.atBats && hitterLog.atBats > 0) {
            hitterLog.paAvg = (hitterLog.paHits ?: 0) / hitterLog.atBats
        }
    }











    public double adjustToPark(Integer value, Double parkAdjustment) {
        return ((1 - parkAdjustment) + 1) * value
    }
}
