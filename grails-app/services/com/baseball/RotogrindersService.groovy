package com.baseball

import com.baseball.Game
import com.baseball.HitterGameLog
import com.baseball.ParkFactor
import com.baseball.Player
import com.baseball.Team
import com.baseball.fanduel.FanduelPlayer
import com.baseball.gameday.NumberUtil
import com.baseball.rotogrinders.RotogrinderGame
import com.baseball.rotogrinders.RotogrinderParkFactor
import com.baseball.rotogrinders.RotogrinderWeatherAndLineup
import grails.converters.JSON
import grails.io.IOUtils
import grails.transaction.Transactional
import org.grails.web.json.JSONArray
import org.springframework.transaction.annotation.Propagation

import static com.xlson.groovycsv.CsvParser.parseCsv

@Transactional
class RotogrindersService {

    GameService gameService
    PlayerService playerService
    def grailsResourceLocator
    def grailsApplication


    public void updateToday() {

        List<Game> todaysGames = gameService.getGamesForDate(new Date())



        updateVegasOdds(todaysGames)
        updateWeatherAndLineups(todaysGames)


    }



    public void updateWeatherAndLineups(List<Game> todaysGames) {

        //Grab weather info from another page
        JSONArray weatherAndLineups = getWeatherAndLineups()


        List<RotogrinderWeatherAndLineup> weatherAndLineupList = new ArrayList<>()
        weatherAndLineups?.each { weatherAndLineup ->
            weatherAndLineupList.add(new RotogrinderWeatherAndLineup(weatherAndLineup))

        }



        weatherAndLineupList?.each { weatherAndLineup ->


            Game game = todaysGames.find { theGame ->
                    weatherAndLineup.away.equals(theGame.awayTeam.rotogrinderId) &&
                    weatherAndLineup.home.equals(theGame.homeTeam.rotogrinderId) &&
                    weatherAndLineup.time == theGame.startDate
            }


            if (game) {
                game.temp = weatherAndLineup.temp
                game.weather = weatherAndLineup.weather
                game.weatherStatus = weatherAndLineup.weatherStatus
                game.windDirection = weatherAndLineup.windDirection
                game.windIcon = weatherAndLineup.windIcon
                game.windSpeed = weatherAndLineup.windSpeed


                weatherAndLineup.teams?.each { rotogrinderTeam ->

                    Team theTeam = null
                    Team opponent = null

                    if (rotogrinderTeam.abbrev == game.awayTeam.rotogrinderId) {
                        theTeam = game.awayTeam
                        opponent = game.homeTeam

                        game.awayLineupConfirmed = false
                        game.awayLineupUnconfirmed = false

                        if (rotogrinderTeam.status == "unconfirmed") {
                            game.awayLineupUnconfirmed = true
                        } else if (rotogrinderTeam.status == "confirmed") {
                            game.awayLineupConfirmed = true
                        }

                    } else {
                        theTeam = game.homeTeam
                        opponent = game.awayTeam

                        game.homeLineupConfirmed = false
                        game.homeLineupUnconfirmed = false

                        if (rotogrinderTeam.status == "unconfirmed") {
                            game.homeLineupUnconfirmed = true
                        } else if (rotogrinderTeam.status == "confirmed") {
                            game.homeLineupConfirmed = true
                        }

                    }


                    rotogrinderTeam.players?.each { rotogrinderPlayer ->

                        Player thePlayer = getAndUpdatePlayer(rotogrinderPlayer, theTeam)


                        HitterGameLog hitterGameLog = HitterGameLog.findByGameAndHitter(game, thePlayer)

                        boolean isNew = false

                        if (!hitterGameLog) {
                            isNew = true
                            hitterGameLog = new HitterGameLog(game: game, hitter: thePlayer)
                        }

                        hitterGameLog.team = theTeam
                        hitterGameLog.opponent = opponent
                        hitterGameLog.year = game.startDate[Calendar.YEAR]



                        if (rotogrinderTeam.status == "unconfirmed") {
                            hitterGameLog.battingOrderUnconfirmed = rotogrinderPlayer.order * 100
                        } else if (rotogrinderTeam.status == "confirmed") {
                            hitterGameLog.battingOrderConfirmed = rotogrinderPlayer.order * 100
                        }


                        if (hitterGameLog.save(flush: true)) {
                            if (isNew) {
                                println "Adding weather and lineup info to hitterGameLog: " + hitterGameLog.game.startDate + " - " + hitterGameLog.hitter.fullName
                            } else {
                                println "Updating weather and lineup info for hitterGameLog: "  + hitterGameLog.game.startDate + " - " + hitterGameLog.hitter.fullName
                            }
                        } else {
                            println "******* PROBLEM SAVING HITTER GAME LOG ***********"
                        }


                    }


                }


                if (game.save(flush: true)) {
                    println "Updating weather and lineup info for game: "  + game.textDescription
                } else {
                    println "******* PROBLEM SAVING GAME ***********"
                }
            } else {
                println "*********** PROBLEM LOCATING ROTOGRINDER GAME ***************"
            }



        }


    }



    public void updateVegasOdds(List<Game> todaysGames) {

        List<RotogrinderGame> rotogrinderGames = getVegasOdds()


        rotogrinderGames?.each { rotogrinderGame ->

            List<Game> games = todaysGames.findAll { theGame ->
                (theGame.awayTeam.rotogrinderId == rotogrinderGame.team || theGame.homeTeam.rotogrinderId == rotogrinderGame.team) &&
                        (theGame.awayTeam.rotogrinderId == rotogrinderGame.opponent || theGame.homeTeam.rotogrinderId == rotogrinderGame.opponent)
            }

            if (games?.size() > 0) {

                games.each { theGame ->

                    if (theGame.awayTeam.rotogrinderId.equals(rotogrinderGame.team)) {
                        theGame.awayVegasProjectedRunsScored = rotogrinderGame.projectedRuns
                        theGame.awayMoneyLine = rotogrinderGame.moneyLine
                        theGame.awayVegasStrikeouts = rotogrinderGame.strikeOuts
                    } else if (theGame.homeTeam.rotogrinderId.equals(rotogrinderGame.team)) {
                        theGame.homeVegasProjectedRunsScored = rotogrinderGame.projectedRuns
                        theGame.homeMoneyLine = rotogrinderGame.moneyLine
                        theGame.homeVegasStrikeouts = rotogrinderGame.strikeOuts
                    }

                    if (theGame.save(flush: true)) {
                        println "Updating game: (" + theGame.startDate + ") " + theGame.homeTeam.name + " / " + theGame.awayTeam.name
                    } else {
                        println "ERROR UPDATING GAME: " + theGame.startDate + ") " + theGame.homeTeam.name + " / " + theGame.awayTeam.name
                    }

                }

            } else {
                println "**********NO GAME FOUND*****************"
            }
        }
    }




    public List<RotogrinderGame> getVegasOdds() {

        copyScript('scripts/rotogrinders.js', 'rotogrinders.js')


        def command = sprintf(grailsApplication.config.node, ['rotogrinders.js'])
        def proc = command.execute()


        def outputStream = new StringBuffer();
        proc.waitForProcessOutput(outputStream, System.err)

        println "$outputStream"


        def result = JSON.parse(outputStream.toString())


        List<RotogrinderGame> games = new ArrayList<>()
        result.each {
            games.add(new RotogrinderGame(it))
        }




        //Grab strikeouts from another page.
        JSONArray strikeouts = getVegasStrikeouts()


        strikeouts?.each { strikeoutGame ->

            RotogrinderGame game = games.find { it.pitcher?.equals(strikeoutGame.pitcher)}

            if (game) {
                game.strikeOuts = NumberUtil.parseDouble(strikeoutGame.strikeouts)
            }


        }




        return games

    }






    public JSONArray getVegasStrikeouts() {


        copyScript('scripts/rotogrinders-strikeouts.js', 'rotogrinders-strikeouts.js')

        String command = sprintf(grailsApplication.config.node, ['rotogrinders-strikeouts.js'])
        def proc = command.execute()

        StringBuffer os = new StringBuffer();
        proc.waitForProcessOutput(os, System.err)

        println "$os"

        def result = JSON.parse(os.toString())

        return result

    }




    public JSONArray getWeatherAndLineups() {

        copyScript('scripts/rotogrinders-lineups.js', 'rotogrinders-lineups.js')


        String command = sprintf(grailsApplication.config.node, ['rotogrinders-lineups.js'])
        def proc = command.execute()

        StringBuffer os = new StringBuffer();
        proc.waitForProcessOutput(os, System.err)

        println "$os"

        def result = JSON.parse(os.toString())

        return result

    }




    public Player getAndUpdatePlayer(RotogrinderWeatherAndLineup.Player rotogrinderPlayer, Team team) {

        Player thePlayer = Player.findByRotogrinderId(rotogrinderPlayer.id)

        if (!thePlayer) {
            thePlayer = playerService.findPlayer(rotogrinderPlayer.name, team)
        }


        if (!thePlayer) {
            println "************ ERROR FINDING ROTOGRINDER PLAYER: $rotogrinderPlayer.name"
        } else {
            thePlayer.rotogrinderId = rotogrinderPlayer.id
            thePlayer.save(flush: true)
        }

        return thePlayer
    }












    public void updateParkFactors() {

        copyScript('scripts/rotogrinders-pf.js', 'rotogrinders-pf.js')


        String command = sprintf(grailsApplication.config.node, ['rotogrinders-pf.js'])
        //def command = "pwd"
        def proc = command.execute()


        def outputStream = new StringBuffer();
        proc.waitForProcessOutput(outputStream, System.err)

        println "$outputStream"


        def result = JSON.parse(outputStream.toString())


        List<RotogrinderParkFactor> parkFactors = new ArrayList<>()
        result.each {
            parkFactors.add(new RotogrinderParkFactor(it))
        }


        parkFactors.each { factor ->

            Team team = Team.findByStadiumName(factor.name)

            ParkFactor parkFactor = ParkFactor.findOrCreateByTeam(team)

            parkFactor.team = team
            parkFactor.lRuns = factor.lRuns
            parkFactor.lHr  = factor.lHr
            parkFactor.l1b =  factor.l1b
            parkFactor.l2b = factor.l2b
            parkFactor.l3b = factor.l3b


            parkFactor.rRuns = factor.rRuns
            parkFactor.rHr = factor.rHr
            parkFactor.r1b = factor.r1b
            parkFactor.r2b = factor.r2b
            parkFactor.r3b = factor.r3b


            if (parkFactor.save(flush:true)) {
                println "Saved park factors for: " + team.abbrev
            } else {
                println "ERROR SAVING PARK FACTOR FOR " + team.abbrev
            }


        }




    }






    public String getWorkingDirectory() {
        def pwdCommand = "pwd"
        def pwdProc = pwdCommand.execute()
        def currentDir = new StringBuffer();
        pwdProc.waitForProcessOutput(currentDir, System.err)

        return currentDir.toString().trim()
    }





    public void updateTeamAbbrev() {

        def fileIn = new File(this.class.classLoader.getResource('scripts/team_id_map.csv'))

        def data = parseCsv(fileIn.text)


        for (line in data) {

            String gamedayId = line."GAMEDAY"
            if (!gamedayId) continue

            String bbrefId = line."BBREFTEAM"
            if (!bbrefId) continue

            String rotogrindId = line."ROTOGRIND"
            if (!bbrefId) continue

            Team team = Team.findByAbbrev(gamedayId)

            team.bbrefId = bbrefId
            team.rotogrinderId = rotogrindId

            team.save(flush:true)
        }




    }






    public void projectionsInCsv(List<HitterGameLog> hitterGameLogs) {

        PrintWriter out = new PrintWriter(new FileWriter("projections.csv"))

        out.println "\"player_id\",\"name\",\"fpts\""

        hitterGameLogs?.each {
            out.println(String.format('"%1$d","%2$s","%3$f"', it.hitter.rotogrinderId, it.hitter.fullName, it.projection))
        }

        out.flush()
        //out.close()


    }




    public void generateRotogrinderProjectionsForDate(Date date) {

        List<HitterGameLog> hitterGameLogs = gameService.getHitterGameLogsForDate(date)


        projectionsInCsv(hitterGameLogs)


    }



    public void copyScript(String location, String name) {

        InputStream is = this.class.classLoader.getResourceAsStream(location)
        OutputStream os = new FileOutputStream(new File(getWorkingDirectory() + "/" + name));

        IOUtils.copy(is, os)
        is.close()
        os.flush()
        os.close()
    }



}
