package com.baseball

import com.baseball.AtBat
import com.baseball.HitterSeason
import com.baseball.ParkFactor
import com.baseball.Team
import com.baseball.domain.HitterLog
import com.baseball.domain.PitcherLog
import grails.transaction.Transactional

@Transactional
class StatService {


    public Double calculateWOba(int bb, int hpb, int singles, int doubles, int triples, int hr, int atBats) {
        if ( atBats == 0 ) return 0
        return (0.7 * (bb + hpb) + (0.9 * singles) + (1.25 * doubles) + (1.6 * triples) + (2 * hr)) / atBats
    }

    public Double calculateWOba(int bb, int hpb, double singles, double doubles, double triples, double hr, int atBats) {
        if ( atBats == 0 ) return 0
        return (0.7 * (bb + hpb) + (0.9 * singles) + (1.25 * doubles) + (1.6 * triples) + (2 * hr)) / atBats
    }

    public Double calculateWfda(int bb, int hpb, int singles, int doubles, int triples, int hr, int rbi, int r, int sb, int so, int atBats) {
        if ( atBats == 0 ) return 0
        return (   (3 * (bb + hpb))  + (3 * singles) + (6 * doubles) + (9 * triples) + (12 * hr)  + (3.5 * rbi) + (3.2 * r)  + (6.0 * sb) ) / atBats
    }

    public Double calculateWfda(int bb, int hpb, double singles, double doubles, double triples, double hr, double rbi, double r, int sb, int so, int atBats) {
        if ( atBats == 0 ) return 0
        return (   (3 * (bb + hpb))  + (3 * singles) + (6 * doubles) + (9 * triples) + (12 * hr)  + (3.5 * rbi) + (3.2 * r)  + (6.0 * sb) ) / atBats
    }


    public Double calculateParkAdjustedWfda(int bb, int hpb, int singles, int doubles, int triples, int hr, int atBats, ParkFactor pf, HitterSeason hitterSeason) {
        if ( atBats == 0 ) return 0

        double bbPoints     = (0.5 * (bb + hpb))
        double singlePoints = (0.5 * singles)
        double doublePoints = 1.0 * doubles
        double triplePoints = 1.5 * triples
        double hrPoints     = 2 * hr

        return (  bbPoints +  singlePoints +   doublePoints +  triplePoints + hrPoints  ) / atBats
    }


    public Double calculateSoPercent(int so, int atBats) {
        if (atBats == 0) return 0.0
        return so / atBats
    }


    public BigDecimal calculateBBPercent(int bb, int atBats) {
        if (atBats == 0) return 0.0
        return bb / atBats
    }

    public Double calculateObp(int hits, int bb, int hpb, int ab, int sfo) {
        if ((ab + bb + hpb + sfo) == 0) return 0
        return (hits + bb + hpb) / (ab + bb + hpb + sfo)
    }

    public Double calculateObp(double hits, double bb, double hpb, double ab, double sfo) {
        if ((ab + bb + hpb + sfo) == 0) return 0
        return (hits + bb + hpb) / (ab + bb + hpb + sfo)
    }

    public Double calculateSlugging(int totalBases, int ab) {
        if (ab == 0) return 0
        return totalBases / ab
    }

    public Double calculateSlugging(double totalBases, double ab) {
        if (ab == 0) return 0
        return totalBases / ab
    }

    public Double calculateGroundBallPercent(int groundBalls, int pa) {
        if (!pa || pa == 0) return 0
        return groundBalls / pa
    }

    public Double calculateLineDrivePercent(int lineDrives, int pa) {
        if (!pa || pa == 0) return 0
        return lineDrives / pa
    }

    public Double calculateFlyBallPercent(int flyBalls, int pa) {
        if (!pa || pa == 0) return 0
        return flyBalls / pa
    }


    public int calculateTotalBases(int singles, int doubles, int triples, int hr) {
        return singles + (2 * doubles) + (3 * triples) + (4 * hr)
    }

    public int calculateTotalBases(double singles, double doubles, double triples, double hr) {
        return singles + (2 * doubles) + (3 * triples) + (4 * hr)
    }


    public Double calculateHitterFanduelPoints(int singles, int doubles, int triples, int hr, int rbi, int r, int bb, int sb, int hpb) {
        return (singles * 3) + (doubles * 6) + (triples * 9) + (hr * 12) + (rbi * 3.5) + (r * 3.2) + (bb * 3) + (sb * 6) + (hpb * 3)
    }

    public Double calculateProjectedFanduelPoints(double singles, double doubles, double triples, double hr, double rbi, double r, double bb, double sb, double hpb) {
        return (singles * 3) + (doubles * 6) + (triples * 9) + (hr * 12) + (rbi * 3.5) + (r * 3.2) + (bb * 3) + (sb * 6) + (hpb * 3)
    }



    public Double calculatePitcherFanduelPoints(int wins, int er, int so, int outs) {
        return (wins * 12) + (er * -3) + (so * 3) + (outs)
    }

    public Double calculateDiff(Double first, Double second) {
        if (!first || first == 0) return 0.0
        if (!second || second == 0) return 0.0
        return (first - second) / first
    }


    public Double calculateIso(int doubles, int triples, int homeRuns, int atBats) {
        if (!atBats || atBats == 0) return 0.0
        return (doubles + (2*triples) + (3*homeRuns)) / atBats
    }


    public Double calculateMoneyLineToWinPercent(Integer moneyLine) {

        if (!moneyLine || moneyLine == 0) return 0

        if (moneyLine > 0) {
            return 100 / (moneyLine + 100)
        } else {
            return ( -(moneyLine) ) /  ((-(moneyLine)) + 100)
        }


    }







    public Double calculateObp(List<AtBat> atBats) {
        if (atBats?.size() == 0) return 0


        int hits = 0
        int bb = 0
        int hbp = 0
        int ab = 0
        int sfo = 0

        atBats?.each {
            if (it?.isHit()) hits++
            if (it?.isBB()) bb++
            if (it?.isHpb()) hbp++
            if (it?.isAtBat()) ab++
            if (it?.isSacFly()) sfo++
        }

        if ((ab + bb + hbp + sfo) == 0) return 0
        return calculateObp(hits, bb, hbp, ab, sfo)
    }






    public Double calculateSlg( List<AtBat> atBats) {
        if (atBats?.size() == 0) return 0

        int ab = 0

        atBats?.each {
            if (it?.isAtBat()) ab++
        }

        int totalBases = calculateTotalBases(atBats)

        if (ab == 0) return 0
        return calculateSlugging(totalBases, ab)

    }

    public Integer calculateTotalBases(List<AtBat> atBats) {
        if (atBats?.size() == 0) return 0

        int singles = 0
        int doubles = 0
        int triples = 0
        int hr = 0

        atBats?.each {
            if (it?.isSingle()) singles++
            if (it?.isDouble()) doubles++
            if (it?.isTriple()) triples++
            if (it?.isHr()) hr++
        }

        return calculateTotalBases(singles, doubles, triples, hr)

    }


    public Double calculateWOba(List<AtBat> atBats) {

        if ( atBats == null || atBats?.size() == 0 ) return 0

        int bb = 0
        int hpb = 0
        int singles = 0
        int doubles = 0
        int triples = 0
        int hr = 0
        int ab = 0

        atBats?.each {
            if (it?.isBB()) bb++
            if (it?.isHpb()) hpb++
            if (it?.isSingle()) singles++
            if (it?.isDouble()) doubles++
            if (it?.isTriple()) triples++
            if (it?.isHr()) hr++
            if (it?.isAtBat()) ab++
        }

        return calculateWOba(bb, hpb, singles, doubles, triples, hr, ab)
    }


    public Double calculateWfda(List<AtBat> atBats) {

        if ( atBats == null || atBats?.size() == 0 ) return 0


        int bb = 0
        int hpb = 0
        int singles = 0
        int doubles = 0
        int triples = 0
        int hr = 0
        int ab = 0

        int rbi = 0
        int r = 0
        int sb = 0
        int so = 0

        atBats?.each {
            if (it?.isBB()) bb++
            if (it?.isHpb()) hpb++
            if (it?.isSingle()) singles++
            if (it?.isDouble()) doubles++
            if (it?.isTriple()) triples++
            if (it?.isHr()) hr++
            if (it?.isAtBat()) ab++
            if (it?.rbi > 0)  {
                rbi += it?.rbi
                r = rbi
            }
            if (it?.isStrikeout()) so++
        }


        return calculateWfda(bb, hpb, singles, doubles, triples, hr, rbi, r, sb, so, ab)
    }


    public Double calculateWfda(List<AtBat> atBats, List<HitterLog> gameLogs) {

        if ( atBats == null || atBats.size() == 0 ) return 0



        int bb=0
        int hpb=0
        int singles=0
        int doubles=0
        int triples=0
        int hr=0
        int rbi=0
        int r=0
        int sb=0
        int so=0
        int pa=0


        atBats.each {
            if (it?.isPa()) pa++
            if (it?.isBB()) bb++
            if (it?.isHpb()) hpb++
            if (it?.isSingle()) singles++
            if (it?.isDouble()) doubles++
            if (it?.isTriple()) triples++
            if (it?.isHr()) hr++
            if (it?.rbi > 0) rbi += it?.rbi
            if (it?.isStrikeout()) so++
        }


        if (gameLogs?.size() > 0) {
            sb = gameLogs?.collect { it?.sb ?: 0}.sum()
            r = gameLogs?.collect { it?.runs ?: 0}.sum()
        }

        if (pa == 0) return 0
        return calculateWfda(bb, hpb, singles, doubles, triples, hr, rbi, r, sb, so, pa)
    }


    public Double calculateWObaFromLogs(List<HitterLog> gameLogs) {

        if ( gameLogs == null || gameLogs.size() == 0 ) return 0



        int atBats = 0
        int bb = 0
        int hpb = 0
        int singles = 0
        int doubles = 0
        int triples = 0
        int hr = 0


        gameLogs?.each {
            if (it.atBats) atBats     = incrementBy(atBats, it.atBats)
            if (it.bb) bb             = incrementBy(bb, it.bb)
            if (it.hbp) hpb     = incrementBy(hpb, it.hbp)
            if (it.singles) singles     = incrementBy(singles, it.singles)
            if (it.doubles) doubles     = incrementBy(doubles, it.doubles)
            if (it.triples) triples     = incrementBy(triples, it.triples)
            if (it.homeRuns) hr     = incrementBy(hr, it.homeRuns)
        }

        if (atBats == 0) return 0

        return calculateWOba(bb, hpb, singles, doubles, triples, hr, atBats)
    }







    public Double calculateSoPercentFromLogs(List<HitterLog> gameLogs) {
        if ( gameLogs == null || gameLogs.size() == 0 ) return 0

        int atBats = gameLogs.collect { it.atBats ? it.atBats : 0}.sum()

        if (atBats == 0) return 0


        int so = gameLogs.collect { it.so ? it.so : 0 }.sum()
        return calculateSoPercent(so, atBats)

    }





    public Double calculateIso(List<AtBat> atBats) {
        return 0
    }




    public Double calculateSoPercent(List<AtBat> atBats) {
        if (atBats?.size() == 0) return 0

        int so = 0
        int pa = 0

        atBats?.each {
            if (it?.isStrikeout()) so++
            if (it?.isPa()) pa++
        }

        if (pa == 0) return 0

        return calculateSoPercent(so, pa)

    }

    public Double calculateBBPercent(List<AtBat> atBats) {
        if (atBats?.size() == 0) return 0

        int bb = 0
        int pa = 0

        atBats?.each {
            if (it?.isBB()) bb++
            if (it?.isPa()) pa++
        }

        if (pa == 0) return 0

        return calculateBBPercent(bb, pa)
    }





    public Double calculateFanduelPoints(List<PitcherLog> gameLogs) {

        if (gameLogs == null || gameLogs.size() == 0) return 0.0

        int wins = 0
        int er = 0
        int so = 0
        int outs = 0

        gameLogs?.each {
            wins += it.wins ?: 0
            er += it.er ?: 0
            so += it.so ?: 0
            outs += it.outs ?: 0
        }

        return calculatePitcherFanduelPoints(wins, er, so, outs)

    }




    public Double calculateFanduelPoints(PitcherLog pitcherGameLog) {

        int wins = pitcherGameLog.wins
        int er = pitcherGameLog.er
        int so = pitcherGameLog.so
        int outs = pitcherGameLog.outs

        return calculatePitcherFanduelPoints(wins, er, so, outs)

    }


    public Double incrementBy(Double original, Double increment) {

        if (original) {
            original += increment
        } else {
            original = increment
        }
        return original

    }

    public Integer incrementBy(Integer original, Integer increment) {

        if (original) {
            original += increment
        } else {
            original = increment
        }
        return original

    }

    public Integer incrementField(Integer f) {

        if (f) {
            f++
        } else {
            f=1
        }
        return f

    }


}
