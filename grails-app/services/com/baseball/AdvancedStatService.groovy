package com.baseball

import com.baseball.AtBat
import com.baseball.HitterGameLog
import com.baseball.HitterLogsAdvancedStatBucket
import com.baseball.HitterSeason
import com.baseball.HitterSeasonSplit
import com.baseball.PitcherGameLog
import com.baseball.PitcherLogsAdvancedStatBucket
import com.baseball.PitcherSeason
import com.baseball.PitcherSeasonSplit
import com.baseball.Player
import com.baseball.gameday.NumberUtil
import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation


class AdvancedStatService {

    HitterService hitterService
    PitcherService pitcherService
    StatService statService
    ProjectionService projectionService
    GameService gameService


    static transactional = false



    public void updateHitterGameLogAdvancedStats(HitterLogsAdvancedStatBucket h) {


        if (!h.hitterGameLog.opponentPitcher) return


        //account for switch hitters
        String bats = h.player?.bats
        if ("S".equals(bats)) {
            bats = "L".equals(h.pitcherSeason?.pitcher?.pitches) ? "R" : "L"
        }


        PitcherSeasonSplit pitcherHandednessSplit = h.pitcherSplits.find { bats?.equals(it?.split) }
        HitterSeasonSplit hitterHandednessSplit = h.hitterSplits.find { h.hitterGameLog?.opponentPitcher?.pitches?.equals(it.split) }

        PitcherSeasonSplit pitcherHomeAwaySplit = h.hitterGameLog.isHome() ? h.pitcherSplits.find{"A".equals(it?.split)} : h.pitcherSplits.find{"H".equals(it?.split) }
        HitterSeasonSplit hitterHomeAwaySplit = h.hitterGameLog.isHome() ? h.hitterSplits.find{"H".equals(it?.split)} : h.hitterSplits.find{"A".equals(it?.split) }

        h.hitterGameLog.careerAtBatsAgainstStarter = h.atBatsVsPitcher?.size()
        h.hitterGameLog.careerWfdaAgainstStarter = statService.calculateWfda(h.atBatsVsPitcher)


        h.hitterGameLog.opponentWObaSeason = h.pitcherSeason?.opponentWOba
        h.hitterGameLog.opponentWObaAgainstHandedness = pitcherHandednessSplit?.opponentWOba
        h.hitterGameLog.opponentWObaAgainstHandednessDiff = statService.calculateDiff(h.hitterGameLog.opponentWObaAgainstHandedness, h.hitterGameLog.opponentWObaSeason)

        h.hitterGameLog.opponentWFdaSeason = h.pitcherSeason?.opponentWFda
        h.hitterGameLog.opponentWFdaAgainstHandedness = pitcherHandednessSplit?.opponentWFda
        h.hitterGameLog.opponentWFdaAgainstHandednessDiff = statService.calculateDiff(h.hitterGameLog.opponentWFdaAgainstHandedness, h.hitterGameLog.opponentWFdaSeason)


        h.hitterGameLog.opponentWfdaHomeAwaySeason = pitcherHomeAwaySplit?.opponentWFda
        h.hitterGameLog.opponentWfdaHomeAwayDiff = statService.calculateDiff(h.hitterGameLog.opponentWfdaHomeAwaySeason, h.pitcherSeason?.opponentWFda)


        if (h.hitterSeason) {
            h.hitterGameLog.wObaSeason = h.hitterSeason.wOba
            h.hitterGameLog.wObaAgainstHandednessSeason = hitterHandednessSplit?.wOba
            h.hitterGameLog.wObaAgainstHandednessDiff = statService.calculateDiff(h.hitterGameLog.wObaAgainstHandednessSeason, h.hitterSeason.wOba)

            h.hitterGameLog.wFdaSeason = h.hitterSeason.wFda
            h.hitterGameLog.wFdaAgainstHandednessSeason = hitterHandednessSplit?.wFda
            h.hitterGameLog.wFdaAgainstHandednessDiff = statService.calculateDiff(h.hitterGameLog.wFdaAgainstHandednessSeason, h.hitterSeason.wFda)


            h.hitterGameLog.isoAgainstHandednessSeason = hitterHandednessSplit?.iso
            h.hitterGameLog.isoAgainstHandednessDiff = statService.calculateDiff(h.hitterGameLog.isoAgainstHandednessSeason, h.hitterSeason.iso)


            h.hitterGameLog.wFdaHomeAwaySeason = hitterHomeAwaySplit?.wFda
            h.hitterGameLog.wfdaHomeAwayDiff = statService.calculateDiff(h.hitterGameLog.wFdaHomeAwaySeason, h.hitterSeason.wOba)

        }



        String boAsString = h.hitterGameLog.battingOrder?.toString()?.take(1)

        if (boAsString) {
            int battingOrder = NumberUtil.parseInt(boAsString)
            h.hitterGameLog.battingOrderDiffFromLast7 = battingOrder - hitterService.getRecentAverageBattingOrder(h.player)
        }


        if (h.hitterGameLog.isHome()) {
            h.hitterGameLog.vegasProjectedRunsScored = h.hitterGameLog.game.homeVegasProjectedRunsScored
        } else {
            h.hitterGameLog.vegasProjectedRunsScored = h.hitterGameLog.game.awayVegasProjectedRunsScored
        }


        HitterSeasonSplit last7Split = h.hitterSplits.find { "7".equals(it.split)}
        HitterSeasonSplit last15Split = h.hitterSplits.find { "15".equals(it.split)}

        if (last7Split?.games > 0) {
            h.hitterGameLog.last7AvgFanduelPoints = last7Split.fanduelPoints / last7Split.games
        }

        if (last15Split?.games > 0) {
            h.hitterGameLog.last15AvgFanduelPoints = last15Split.fanduelPoints / last15Split.games
        }

        if (h.hitterSeason?.games > 0) {
            h.hitterGameLog.seasonAvgFanduelPoints = h.hitterSeason.fanduelPoints / h.hitterSeason.games
        }


        h.hitterGameLog.projection = projectionService.calculateProjection(h)

        if (h.hitterGameLog.fanduelSalary) {
            h.hitterGameLog.value = (h.hitterGameLog.projection / h.hitterGameLog.fanduelSalary) * 1000
        }







    }





    public void updatePitcherGameLogAdvancedStats(PitcherLogsAdvancedStatBucket p) {


        if (p.pitcherSeason) {

            if (p.last5Starts?.size() > 0) {
                p.pitcherGameLog.last5StartsAvgFanduelPoints = p.last5Starts.collect { it?.fanduelPoints ? it.fanduelPoints : 0}.sum() / p.last5Starts?.size()
            }


            p.pitcherGameLog.seasonAvgFanduelPoints = 0
            if (p.pitcherSeason.games > 0) {
                p.pitcherGameLog.seasonAvgFanduelPoints = (p.pitcherSeason.fanduelPoints ?: 0) / (p.pitcherSeason.games ?: 0)
            }


            p.pitcherGameLog.wFdaSeason = p.pitcherSeason.opponentWFda
            p.pitcherGameLog.wObaSeasonLeft = p.pitcherSplits?.find { "L".equals(it.split)}?.opponentWOba
            p.pitcherGameLog.wObaSeasonRight = p.pitcherSplits?.find { "R".equals(it.split)}?.opponentWOba

            p.pitcherGameLog.wFdaSeasonLeft = p.pitcherSplits?.find { "L".equals(it.split)}?.opponentWFda
            p.pitcherGameLog.wFdaSeasonRight = p.pitcherSplits?.find { "R".equals(it.split)}?.opponentWFda

            p.pitcherGameLog.wFdaSeasonHomeAway = p.pitcherGameLog.isHome() ? p.pitcherSplits.find{"H".equals(it?.split)}?.opponentWFda : p.pitcherSplits.find{"A".equals(it?.split)}?.opponentWFda

            p.pitcherGameLog.soPercentSeason = p.pitcherSeason?.soPercent
        }


        if (p.pitcherGameLog.isHome()) {
            p.pitcherGameLog.vegasRunsAgainst = p.pitcherGameLog.game.awayVegasProjectedRunsScored
            p.pitcherGameLog.vegasWinPercent = statService.calculateMoneyLineToWinPercent(p.pitcherGameLog.game.homeMoneyLine)
            p.pitcherGameLog.vegasStrikeouts = p.pitcherGameLog.game.homeVegasStrikeouts
        } else {
            p.pitcherGameLog.vegasRunsAgainst = p.pitcherGameLog.game.homeVegasProjectedRunsScored
            p.pitcherGameLog.vegasWinPercent = statService.calculateMoneyLineToWinPercent(p.pitcherGameLog.game.awayMoneyLine)
            p.pitcherGameLog.vegasStrikeouts = p.pitcherGameLog.game.awayVegasStrikeouts
        }


        if (p.opposingStartingLineup?.size() > 0) {

            p.pitcherGameLog.opponentWOba = statService.calculateWObaFromLogs(p.startingLineupGameLogs)
            p.pitcherGameLog.opponentWFda = statService.calculateWfda(p.startingLineupAtbats, p.startingLineupGameLogs)
            p.pitcherGameLog.opponentSoPercent = statService.calculateSoPercentFromLogs(p.startingLineupGameLogs)
            p.pitcherGameLog.projectedOpponentPoints = p.hitterGameLogs?.collect { it.projection ? it.projection : 0}.sum()


        }


        p.pitcherGameLog.projection = projectionService.calculateProjection(p.pitcherGameLog)


    }






}
