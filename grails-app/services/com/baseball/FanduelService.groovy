package com.baseball

import com.baseball.Game
import com.baseball.HitterGameLog
import com.baseball.HitterSeason
import com.baseball.HitterSeasonSplit
import com.baseball.PitcherGameLog
import com.baseball.PitcherSeason
import com.baseball.PitcherSeasonSplit
import com.baseball.Player
import com.baseball.Team
import com.baseball.fanduel.FanduelPlayer
import grails.io.IOUtils
import grails.transaction.NotTransactional
import grails.transaction.Transactional
import org.springframework.transaction.annotation.Propagation

import static com.xlson.groovycsv.CsvParser.parseCsv

@Transactional
class FanduelService {

    def grailsResourceLocator
    def grailsApplication
    GameService gameService
    HitterService hitterService
    PitcherService pitcherService
    StatService statService
    PlayerService playerService




    public void updateToday() {

        List<Game> todaysGames = gameService.getGamesForDate(new Date())

        List<FanduelPlayer> fanduelPlayers = grabCurrentSalaries()

        println "Fanduel players: " + fanduelPlayers?.size()

        int count=0
        fanduelPlayers.each { fanduelPlayer ->

            println count++ +  " of " + fanduelPlayers.size()

            //Identify the player
            Player player = Player.findByFanduelId(fanduelPlayer.id)
            Team team = Team.findByFanduelId(fanduelPlayer.team)
            Team opponent = Team.findByFanduelId(fanduelPlayer.opponent)

            player = getPlayer(player, fanduelPlayer, team)



            List<Game> games = todaysGames.findAll { theGame ->
                theGame.awayTeam.fanduelId.equals(team.fanduelId) || theGame.homeTeam.fanduelId.equals(team.fanduelId)
            }



            //Update the values for that day
            if ("P".equals(fanduelPlayer.position)) {

                games.each { theGame ->
                    updatePitcherGameLog(player, theGame, fanduelPlayer, team, opponent)
                }



            } else {
                games.each { theGame ->
                    updateHitterGameLog(player, theGame, fanduelPlayer, team, opponent)
                }
            }


//            player.currentPosition = fanduelPlayer.position
//
//            if (!player.save(flush:true)) {
//                println "***************************ERROR UPDATING POSITION FOR PLAYER: " + player.fullName
//            }


        }



    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePitcherGameLog(Player player, Game theGame, FanduelPlayer fanduelPlayer, Team team, Team opponent) {

        PitcherGameLog pitcherGameLog = PitcherGameLog.findByGameAndPitcher(theGame, player)

        boolean isNew = false

        if (!pitcherGameLog) {
            isNew = true
            pitcherGameLog = new PitcherGameLog(game: theGame, pitcher: player)
        }

        pitcherGameLog.team = team
        pitcherGameLog.opponent = opponent
        pitcherGameLog.year = theGame.startDate[Calendar.YEAR]
        pitcherGameLog.fanduelSalary = fanduelPlayer.salary

        if (pitcherGameLog.save(flush: true)) {
            if (isNew) {
                println "Adding Fanduel info to pitcherGameLog: " + pitcherGameLog.game.startDate + " - " + pitcherGameLog.pitcher.fullName
            } else {
                println "Updating Fanduel pitcherGameLog: "  + pitcherGameLog.game.startDate + " - " + pitcherGameLog.pitcher.fullName
            }
        } else {
            println "******* PROBLEM SAVING PITCHER GAME LOG ***********"
        }
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateHitterGameLog(Player player, Game theGame, FanduelPlayer fanduelPlayer, Team team, Team opponent) {

        HitterGameLog hitterGameLog = HitterGameLog.findByGameAndHitter(theGame, player)

        boolean isNew = false

        if (!hitterGameLog) {
            isNew = true
            hitterGameLog = new HitterGameLog(game: theGame, hitter: player)
        }

        hitterGameLog.fanduelSalary = fanduelPlayer.salary
        hitterGameLog.team = team
        hitterGameLog.opponent = opponent
        hitterGameLog.year = theGame.startDate[Calendar.YEAR]




        if (hitterGameLog.save(flush: true)) {
            if (isNew) {
                println "Adding Fanduel info to hitterGameLog: " + hitterGameLog.game.startDate + " - " + hitterGameLog.hitter.fullName
            } else {
                println "Updating Fanduel info for hitterGameLog (${hitterGameLog.id}): "  + hitterGameLog.game.startDate + " - " + hitterGameLog.hitter.fullName
            }
        } else {
            println "******* PROBLEM SAVING HITTER GAME LOG ***********"
        }

    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Player getPlayer(Player player, FanduelPlayer fanduelPlayer, Team team) {

        if (player) return player

        player = playerService.findPlayer(fanduelPlayer.firstName, fanduelPlayer.lastName, team)

        if (!player) {
            println "************ ERROR FINDING FANDUEL PLAYER: $fanduelPlayer.firstName $fanduelPlayer.lastName"
        } else {
            player.fanduelId = fanduelPlayer.id
            player.save(flush: true)
        }



        return player
    }





    private List<FanduelPlayer> grabCurrentSalaries() {

        String FILENAME = "/tmp/fanduel.txt"

        File file = new File(FILENAME)

        if (file.exists()) file.delete()


        copyScript('scripts/fanduel.js', '/fanduel.js')


        String command = sprintf(grailsApplication.config.node, ['fanduel.js'])
        println "Executing command: " + command
        def proc = command.execute()


        def outputStream = new StringBuffer();
        proc.waitForProcessOutput(outputStream, System.err)

        println "$outputStream"


        def data = parseCsv(new File(FILENAME).text)

        List<FanduelPlayer> players = new ArrayList()

        for (line in data) {

            FanduelPlayer player = new FanduelPlayer()
            player.id = line."Id"
            player.position = line."Position"
            player.firstName = line."First Name"
            player.lastName = line."Last Name"
            player.FPPG = line."FPPG"
            player.played = line."Played"
            player.salary = Integer.parseInt(line."Salary")
            player.game = line."Game"
            player.team = line."Team"
            player.opponent = line."Opponent"
            player.injuryIndicator = line."Injury Indicator"
            player.injuryDetails = line."Injury Details"
            player.probablePitcher = line."Probable Pitcher"
            player.battingOrder = line."Batting Order"

            players.add(player)
        }


        return players



    }


    public String getWorkingDirectory() {
        def pwdCommand = "pwd"
        def pwdProc = pwdCommand.execute()
        def currentDir = new StringBuffer();
        pwdProc.waitForProcessOutput(currentDir, System.err)

        return currentDir.toString().trim()
    }

    public void deleteScript(String newPath, String fileName) {
        def pwdCommand = "rm -f " + newPath + "/" + fileName
        def pwdProc = pwdCommand.execute()
        def outputStream = new StringBuffer();
        pwdProc.waitForProcessOutput(outputStream, System.err)

        String result = outputStream?.toString()?.trim()


    }


//    public String moveScript(String currentPath, String newPath) {
//
//
//
//        String mv = "cp " + currentPath + " " + newPath + "/fanduel.js"
//        def proc = mv.execute()
//
//
//        def outputStream = new StringBuffer();
//        proc.waitForProcessOutput(outputStream, System.err)
//
//        def result = outputStream.toString()?.trim()
//        return result
//    }
//

    public void updateTeamAbbrev() {

        def fileIn = grailsResourceLocator.findResourceForURI('scripts/team_id_map.csv').file

        def data = parseCsv(fileIn.text)





        for (line in data) {

            String gamedayId = line."GAMEDAY"
            if (!gamedayId) continue

            String fanduelId = line."FDTEAM"
            if (!fanduelId) continue

            Team team = Team.findByAbbrev(gamedayId)

            team.fanduelId = fanduelId

            team.save(flush:true)
        }



    }






    public void getTodaysNbaContests() {


        copyScript('scripts/fanduel-nba-contests.js', '/fanduel-nba-contests.js')


        String command = sprintf(grailsApplication.config.node, ['fanduel-nba-contests.js'])
        println "Executing command: " + command
        def proc = command.execute()


        def outputStream = new StringBuffer();
        proc.waitForProcessOutput(outputStream, System.err)

        println "$outputStream"




    }







    public void copyScript(String location, String name) {

        InputStream is = this.class.classLoader.getResourceAsStream(location)
        OutputStream os = new FileOutputStream(new File(getWorkingDirectory() + name));

        IOUtils.copy(is, os)
        is.close()
        os.flush()
        os.close()
    }


}
