package com.baseball

import com.baseball.Game
import com.baseball.Pitch
import grails.transaction.Transactional

@Transactional
class PitchService {

    public List<Pitch> getPitchesForGame(Game theGame) {

        return Pitch.withCriteria {
            atBat {
                game {
                    eq('id', theGame.id)
                }
            }
        }

    }
}
