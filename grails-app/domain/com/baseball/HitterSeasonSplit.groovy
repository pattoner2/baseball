package com.baseball

import com.baseball.domain.HitterLog

/**
 * Created by patricktoner on 5/19/16.
 */
class HitterSeasonSplit extends HitterLog {

    static constraints = {
        importFrom HitterLog

        year(nullable:false,  unique: ['hitter', 'split'] )
        split(nullable:false)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        hitter fetch: 'join', lazy: false
    }

    Integer year
    String split


    Date lastUpdated
    Date dateCreated


    public Double getFanduelAvg() {
        if (games == null || games == 0 || fanduelPoints == null) return 0
        return fanduelPoints / games
    }


    public boolean isHandedness() {

        if ("L".equals(split) || "R".equals(split)) return true
        return false

    }

    public boolean isHomeAway() {
        if ("H".equals(split) || "A".equals(split)) return true
        return false
    }

    public boolean isRecent() {
        if ("7".equals(split) || "15".equals(split) || "30".equals(split))
        return true
    }

    public boolean isCurrentSplit(HitterGameLog hitterGameLog) {

        String homeAway = hitterGameLog.isHome() ? "H" : "A"
        String pitches = hitterGameLog.opponentPitcher.pitches

        if (pitches == this.split) return true
        if (homeAway == this.split) return true


        return  false

    }


}
