package com.baseball

import com.baseball.gameday.BoxScore
import com.baseball.olap.dimensions.Hitter.HomeAwayValue
import com.baseball.olap.dimensions.Pitcher

class Game {

    static constraints = {

        gameId(nullable: false, unique: true, maxSize: 40)
        gamePk(nullable: false, unique: true, maxSize: 40)

        rotogrinderId(nullable:true)
        status(nullable:true)

        year(nullable:true)
        type(nullable:false)

        startDate(nullable: false)

        venueWeatherChannelCode(nullable:true)

        homeTeam(nullable: false)
        awayTeam(nullable: false)

        homeProbablePitcher(nullable:true)
        awayProbablePitcher(nullable:true)

        homeStartingPitcher(nullable:true)
        awayStartingPitcher(nullable:true)

        winningPitcher(nullable:true)
        losingPitcher(nullable:true)
        savePitcher(nullable:true)

        homeVegasProjectedRunsScored(nullable:true)
        awayVegasProjectedRunsScored(nullable:true)

        awayVegasStrikeouts(nullable:true)
        homeVegasStrikeouts(nullable:true)

        homeMoneyLine(nullable:true)
        awayMoneyLine(nullable:true)

        parkFactor(nullable:true)
        tempFactor(nullable:true)
        windFactor(nullable:true)

        awayLineupPopulated(nullable:true)
        awayLineupConfirmed(nullable:true)
        awayLineupUnconfirmed(nullable:true)

        homeLineupPopulated(nullable:true)
        homeLineupConfirmed(nullable:true)
        homeLineupUnconfirmed(nullable:true)

        temp(nullable:true)
        weather(nullable:true)
        weatherStatus(nullable:true)
        windDirection(nullable:true)
        windIcon(nullable:true)
        windSpeed(nullable:true)

        awayTeamRuns(nullable:true)
        homeTeamRuns(nullable:true)
        awayTeamHits(nullable:true)
        homeTeamHits(nullable:true)
        awayTeamErrors(nullable:true)
        homeTeamErrors(nullable:true)

        preAllstar(nullable:true)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)
    }

    static mapping = {
        autoTimestamp(true)
        version(false)
        homeTeam lazy: false, fetch: 'join'
        awayTeam lazy: false, fetch: 'join'
    }


    String gameId //from gameday
    String gamePk //from gameday
    Integer rotogrinderId


    Integer year

    String type
    String status

    Date startDate

    String venueWeatherChannelCode

    Team homeTeam
    Team awayTeam

    Player homeProbablePitcher //get from linescore
    Player awayProbablePitcher

    Player homeStartingPitcher
    Player awayStartingPitcher

    Player winningPitcher
    Player losingPitcher
    Player savePitcher

    int awayTeamRuns
    int homeTeamRuns
    int awayTeamHits
    int homeTeamHits
    int awayTeamErrors
    int homeTeamErrors

    Double homeVegasProjectedRunsScored
    Integer homeMoneyLine
    Double homeVegasStrikeouts

    Double awayVegasProjectedRunsScored
    Integer awayMoneyLine
    Double awayVegasStrikeouts

    Double parkFactor
    Double tempFactor
    Double windFactor

    boolean awayLineupPopulated //from gameday
    boolean awayLineupConfirmed //from rotogrinders
    boolean awayLineupUnconfirmed //from rotogrinders

    boolean homeLineupPopulated
    boolean homeLineupConfirmed
    boolean homeLineupUnconfirmed

    Boolean preAllstar

    Integer temp
    String weather
    String weatherStatus
    String windDirection
    String windIcon
    String windSpeed


    Date lastUpdated
    Date dateCreated


    public boolean hasStarted() {
        return (startDate <= new Date())
    }

    public String getTextDescription() {
        return awayTeam.abbrev + " vs. @" + homeTeam.abbrev + " " + startDate.format("MM/dd/YYYY")
    }


    public String getAwayLineupStatus() {

        if (awayLineupPopulated) return "official"
        if (awayLineupConfirmed) return "confirmed"
        if (awayLineupUnconfirmed) return "unconfirmed"
        return null

    }

    public String getHomeLineupStatus() {

        if (homeLineupPopulated) return "official"
        if (homeLineupConfirmed) return "confirmed"
        if (homeLineupUnconfirmed) return "unconfirmed"
        return null

    }


    public String getWeatherIcon() {

        switch(weather) {
            case "icn-sunny":
                return "wi-day-sunny"
            case "icn-dome":
                return "wi-day-sunny"
            case "icn-stormy":
                return "wi-day-thunderstorm"
            case "icn-rainy":
                return "wi-day-rain"
            case "icn-cloudy":
                return "wi-day-cloudy"
        }

    }


    public String getWindIcon() {

        if (windDirection) return "wi-towards-" + windDirection.toLowerCase()
        return null

    }






}
