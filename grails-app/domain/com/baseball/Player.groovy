package com.baseball

import com.baseball.PlayerService

/**
 * Created by patricktoner on 5/21/16.
 */
class Player {

    def playerService

    static constraints = {

        gamedayId(nullable:false, unique: true, maxSize: 40)
        fanduelId(nullable:true)
        rotogrinderId(nullable:true)

        firstName(nullable:false)
        lastName(nullable:false)
        displayName(nullable:true)

        currentTeam(nullable:true)

        pitches(nullable:true)
        bats(nullable:true)

        position(nullable:false)
        currentPosition(nullable:false)

        status(nullable:true)

        last10FantasyPoints(nullable:true)
        seasonAvgFantasyPoints(nullable:true)
        seasonFloorFantasyPoints(nullable:true)
        seasonCeilingFantasyPoints(nullable:true)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        cache true
    }

    String gamedayId
    String fanduelId
    Integer rotogrinderId

    String firstName
    String lastName
    String displayName

    Team currentTeam

    String pitches
    String bats

    String position
    String currentPosition

    String status

    Double last10FantasyPoints
    Double seasonAvgFantasyPoints
    Double seasonFloorFantasyPoints
    Double seasonCeilingFantasyPoints

    Date lastUpdated
    Date dateCreated




    public String getFullName() {
        return firstName + " " + lastName
    }

    public String getAbbrevName() {
        return lastName + ", " + firstName.take(1)
    }

    public getPermaLink() {
        return playerService.getPermaLink(this)
    }


    public getFanduelPosition() {
        if ("CF".equals(position) || "LF".equals(position) || "RF".equals(position)) return "OF"
        return position
    }


}
