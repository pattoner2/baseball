package com.baseball

/**
 * Created by patricktoner on 5/19/16.
 */
class Team {

    static constraints = {

        gamedayId(nullable:false, unique: true, maxSize: 40)
        fanduelId(nullable:true)
        bbrefId(nullable:true)
        rotogrinderId(nullable:true)

        name(nullable:false)
        stadiumName(nullable:true)

        code(nullable:false)
        city(nullable:false)
        abbrev(nullable:false)

        division(nullable:false)
        league(nullable:false)

        lastUpdated(nullable: true)
        dateCreated(nullable: true)
    }

    static mapping = {
        autoTimestamp(true)
    }

    String gamedayId
    String fanduelId
    String bbrefId
    String rotogrinderId

    String name

    String code
    String city
    String abbrev
    String stadiumName

    DivisionValue division
    LeagueValue league //AL, NL


    Date lastUpdated
    Date dateCreated



    public getDisplayName() {
        return name
    }




    public enum LeagueValue {

        AL("AL"),
        NL("NL"),
        TBD("TBD")

        private final String value
        LeagueValue(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }


    public enum DivisionValue {

        AL_EAST("AL_EAST"),
        AL_WEST("AL_WEST"),
        AL_CENTRAL("AL_CENTRAL"),
        NL_EAST("NL_EAST"),
        NL_CENTRAL("NL_CENTRAL"),
        NL_WEST("NL_WEST"),
        TBD("TBD")

        private final String value
        DivisionValue(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }



}
