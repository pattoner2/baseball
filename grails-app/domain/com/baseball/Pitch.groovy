package com.baseball

class Pitch {

    static constraints = {
        atBat(nullable:false)
        sequence(nullable: false)
        sportsvisionId(nullable: true)
        description(nullable: true)
        startSpeed(nullable: true)
        endSpeed(nullable: true)
        type(nullable: true)
        pitchType(nullable: true)


        tfs(nullable: true)                //="233334"
        //tfsZulu(nullable: true)            //="2016-05-09T23:33:34Z"
        y(nullable: true)                //="218.13"
        eventNum(nullable: true)         //="91"
        on1b(nullable: true)              //="408299"
        on2b(nullable: true)               //="521692"
        on3b(nullable:true)
        playGuid(nullable: true)         //="13919870-1e7c-4d09-bd4a-e9f1fc307729"
        szTop(nullable: true)            //="3.5"
        szBot(nullable: true)            //="1.59"
        pfxX(nullable: true)             //="3.23"
        pfxZ(nullable: true)             //="-2.82"
        px(nullable: true)             //="1.086"
        pz(nullable: true)               //="0.765"
        x0(nullable: true)               //="-2.027"
        y0(nullable: true)               //="50.0"
        z0(nullable: true)               //="6.592"
        vx0(nullable: true)              //="6.293"
        vy0(nullable: true)              //="-118.359"
        vz0(nullable: true)              //="-5.883"
        ax(nullable: true)               //="4.57"
        ay(nullable: true)               //="22.808"
        az(nullable: true)               //="-36.101"
        breakY(nullable: true)          //="23.9"
        breakAngle(nullable: true)       //="-8.4"
        breakLength(nullable: true)      //="10.7"
        typeConfidence(nullable: true)   //="2.000"
        zone(nullable: true)             //="14"
        nasty(nullable: true)            //="84"
        spinDir(nullable: true)          //="49.327"
        spinRate(nullable: true)         //="738.570"
    }

    static mapping = {
        autoTimestamp true
        description sqlType: "text"
    }



    AtBat atBat


    Integer sequence

    String sportsvisionId
    String description
    Double startSpeed
    Double endSpeed
    String type
    String pitchType


    Long id                 //="91"
    Long tfs                //="233334"
    //Date tfsZulu            //="2016-05-09T23:33:34Z"
    Double y                //="218.13"
    Double eventNum         //="91"
    Long on1b              //="408299"
    Long on2b               //="521692"
    Long on3b               //="34343"
    String playGuid         //="13919870-1e7c-4d09-bd4a-e9f1fc307729"
    Double szTop            //="3.5"
    Double szBot            //="1.59"
    Double pfxX             //="3.23"
    Double pfxZ             //="-2.82"
    Double px               //="1.086"
    Double pz               //="0.765"
    Double x0               //="-2.027"
    Double y0               //="50.0"
    Double z0               //="6.592"
    Double vx0              //="6.293"
    Double vy0              //="-118.359"
    Double vz0              //="-5.883"
    Double ax               //="4.57"
    Double ay               //="22.808"
    Double az               //="-36.101"
    Double breakY          //="23.9"
    Double breakAngle       //="-8.4"
    Double breakLength      //="10.7"
    Double typeConfidence   //="2.000"
    Double zone             //="14"
    Double nasty            //="84"
    Double spinDir          //="49.327"
    Double spinRate         //="738.570"

    Date lastUpdated
    Date dateCreated

}
