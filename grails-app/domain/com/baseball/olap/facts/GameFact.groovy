package com.baseball.olap.facts

import com.baseball.Game
import com.baseball.Player

/**
 * Created by patricktoner on 9/23/16.
 */
class GameFact {

    static constraints = {

        homeProbablePitcher(nullable:false)
        awayProbablePitcher(nullable:false)

        homeStartingPitcher(nullable:false)
        awayStartingPitcher(nullable:false)

        winningPitcher(nullable:false)
        losingPitcher(nullable:false)
        savePitcher(nullable:false)


        shutOut(nullable:false)
        completeGame(nullable:false)

        homeVegasProjectedRunsScored(nullable:false)
        awayVegasProjectedRunsScored(nullable:false)

        awayVegasStrikeouts(nullable:false)
        homeVegasStrikeouts(nullable:false)

        homeMoneyline(nullable:false)
        awayMoneyline(nullable:false)


    }

    static mapping = {
        autoTimestamp true
        table "fact_game"
    }

    Game game

    Player homeProbablePitcher
    Player awayProbablePitcher

    Player homeStartingPitcher
    Player awayStartingPitcher

    Player winningPitcher
    Player losingPitcher
    Player savePitcher


    int shutOut
    int completeGame

    double homeVegasProjectedRunsScored
    double awayVegasProjectedRunsScored

    double awayVegasStrikeouts
    double homeVegasStrikeouts

    double homeMoneyline
    double awayMoneyline



}
