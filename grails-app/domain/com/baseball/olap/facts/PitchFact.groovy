package com.baseball.olap.facts

import com.baseball.AtBat
import com.baseball.Game
import com.baseball.Player
import com.baseball.Team
import com.baseball.olap.dimensions.*

class PitchFact {

    static constraints = {

        game(nullable:false)

        weather(nullable:false)


        ballPark(nullable:false)


        homeTeam(nullable:false)
        awayTeam(nullable:false)


        hitter(nullable: false)
        pitcher(nullable:false)

        homeProbablePitcher(nullable:false)
        awayProbablePitcher(nullable:false)

        homeStartingPitcher(nullable:false)
        awayStartingPitcher(nullable:false)

        winningPitcher(nullable:false)
        losingPitcher(nullable:false)
        savePitcher(nullable:false)

        on1b(nullable:false)
        on2b(nullable:false)
        on3b(nullable:false)


        atBat(nullable:false)


        gameTime(nullable:false)
        inning(nullable:false)



        pitchType(nullable:false)


        sequence(nullable:false)
        sportsvisionId(nullable:false)



        startSpeed(nullable:false)
        endSpeed(nullable:false)
        type(nullable:false)




        tfs(nullable:false)

        y(nullable:false)
        eventNum(nullable:false)
        playGuid(nullable:false)
        szTop(nullable:false)
        szBot(nullable:false)
        pfxX(nullable:false)
        pfxZ(nullable:false)
        px(nullable:false)
        pz(nullable:false)
        x0(nullable:false)
        y0(nullable:false)
        z0(nullable:false)
        vx0(nullable:false)
        vy0(nullable:false)
        vz0(nullable:false)
        ax(nullable:false)
        ay(nullable:false)
        az(nullable:false)
        breakY(nullable:false)
        breakAngle(nullable:false)
        breakLength(nullable:false)
        typeConfidence(nullable:false)
        zone(nullable:false)
        nasty(nullable:false)
        spinDir(nullable:false)
        spinRate(nullable:false)





        //Measures
        hits(nullable:false)
        singles(nullable:false)
        doubles(nullable:false)
        triples(nullable:false)
        homeRuns(nullable:false)
        tb(nullable:false)
        runs(nullable:false)
        rbi(nullable:false)
        bb(nullable:false)
        sb(nullable:false)
        cs(nullable:false)
        hbp(nullable:false)
        so(nullable:false)
        lob(nullable:false)
        sacBunts(nullable:false)
        sacFlys(nullable:false)
        outs(nullable:false)

        groundOuts(nullable:false)
        flyOuts(nullable:false)

        lineOuts(nullable:false)
        groundBalls(nullable:false)
        lineDrives(nullable:false)
        flyBalls(nullable:false)


        gidp(nullable:false)
        po(nullable:false)
        assists(nullable:false)
        e(nullable:false)



        paHits(nullable:false)
        paSingles(nullable:false)
        padoubles(nullable:false)
        paTriples(nullable:false)
        paHomeRuns(nullable:false)
        paTb(nullable:false)
        paRuns(nullable:false)
        paRbi(nullable:false)
        paBb(nullable:false)
        paSb(nullable:false)
        paCs(nullable:false)
        paHbp(nullable:false)
        paSo(nullable:false)
        paSacBunts(nullable:false)
        paSacFlys(nullable:false)


        earnedRuns(nullable:false)

        strikes(nullable:false)
        balls(nullable:false)


    }

    static mapping = {
        autoTimestamp true
        table "fact_pitch"
    }




    Game game

    Weather weather


    BallPark ballPark


    Team homeTeam
    Team awayTeam


    Hitter hitter
    Pitcher pitcher


    Pitcher homeProbablePitcher
    Pitcher awayProbablePitcher

    Pitcher homeStartingPitcher
    Pitcher awayStartingPitcher

    Pitcher winningPitcher
    Pitcher losingPitcher
    Pitcher savePitcher

    Hitter on1b
    Hitter on2b
    Hitter on3b


    AtBat atBat


    GameTime gameTime
    Inning inning
    PitchType pitchType
    String type


    int sequence
    String sportsvisionId


    double startSpeed
    double endSpeed



    Long tfs                //="233334"
    //Date tfsZulu            //="2016-05-09T23:33:34Z"
    double y                //="218.13"
    double eventNum         //="91"
    String playGuid         //="13919870-1e7c-4d09-bd4a-e9f1fc307729"
    double szTop            //="3.5"
    double szBot            //="1.59"
    double pfxX             //="3.23"
    double pfxZ             //="-2.82"
    double px               //="1.086"
    double pz               //="0.765"
    double x0               //="-2.027"
    double y0               //="50.0"
    double z0               //="6.592"
    double vx0              //="6.293"
    double vy0              //="-118.359"
    double vz0              //="-5.883"
    double ax               //="4.57"
    double ay               //="22.808"
    double az               //="-36.101"
    double breakY          //="23.9"
    double breakAngle       //="-8.4"
    double breakLength      //="10.7"
    double typeConfidence   //="2.000"
    double zone             //="14"
    double nasty            //="84"
    double spinDir          //="49.327"
    double spinRate         //="738.570"





    //Measures
    int hits
    int singles
    int doubles
    int triples
    int homeRuns
    int tb
    int runs
    int rbi
    int bb
    int sb
    int cs
    int hbp
    int so
    int lob
    int sacBunts
    int sacFlys
    int outs

    int groundOuts
    int flyOuts

    int lineOuts
    int groundBalls
    int lineDrives
    int flyBalls


    int gidp
    int po
    int assists
    int e



    double paHits
    double paSingles
    double padoubles
    double paTriples
    double paHomeRuns
    double paTb
    double paRuns
    double paRbi
    double paBb
    double paSb
    double paCs
    double paHbp
    double paSo
    double paSacBunts
    double paSacFlys


    int earnedRuns

    int strikes
    int balls



    Date lastUpdated
    Date dateCreated

}
