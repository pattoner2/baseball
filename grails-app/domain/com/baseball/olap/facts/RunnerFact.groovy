package com.baseball.olap.facts

import com.baseball.AtBat
import com.baseball.Game
import com.baseball.Player
import com.baseball.Team
import com.baseball.olap.dimensions.*

class RunnerFact {

    static constraints = {

        game(nullable:false)

        weather(nullable:false)


        ballPark(nullable:false)


        homeTeam(nullable:false)
        awayTeam(nullable:false)


        hitter(nullable: false)
        pitcher(nullable:false)

        homeProbablePitcher(nullable:false)
        awayProbablePitcher(nullable:false)

        homeStartingPitcher(nullable:false)
        awayStartingPitcher(nullable:false)

        winningPitcher(nullable:false)
        losingPitcher(nullable:false)
        savePitcher(nullable:false)


        atBat(nullable:false)


        gameTime(nullable:false)
        inning(nullable:false)

        start(nullable:false)
        end(nullable:false)
        event(nullable:false)
        eventNum(nullable:false)



        sb(nullable:false)
        cs(nullable:false)
        pickOff(nullable:false)
        pickoffAttempt(nullable:false)
        pickoffError(nullable:false)
    }

    static mapping = {
        autoTimestamp true
        table "fact_runner"
    }




    Game game

    Weather weather

    BallPark ballPark

    Team homeTeam
    Team awayTeam

    Hitter hitter
    Pitcher pitcher

    Pitcher homeProbablePitcher
    Pitcher awayProbablePitcher

    Pitcher homeStartingPitcher
    Pitcher awayStartingPitcher

    Pitcher winningPitcher
    Pitcher losingPitcher
    Pitcher savePitcher


    AtBat atBat


    GameTime gameTime
    Inning inning


    String start
    String end
    String event
    Integer eventNum



    int score
    int earned
    int sb
    int cs
    int pickOff
    int pickoffAttempt
    int pickoffError


    Date lastUpdated
    Date dateCreated

}
