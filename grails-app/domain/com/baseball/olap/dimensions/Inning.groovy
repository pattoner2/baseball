package com.baseball.olap.dimensions

/**
 * Created by patricktoner on 9/24/16.
 */
class Inning {

    static constraints =  {
        inningNum(nullable:false, unique: ['inningTop'])
        inningTop(nullable:false)
    }

    static mapping = {
        autoTimestamp true
        cache true
        table "dim_inning"
    }


    double inningNum
    boolean inningTop

    Date lastUpdated
    Date dateCreated
}
