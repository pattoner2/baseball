package com.baseball.olap.dimensions

/**
 * Created by patricktoner on 9/22/16.
 */
class Weather {

    static constraints = {
        weather(nullable:false, unique: ['weatherStatus', 'windDirection', 'windSpeed', 'temp'])
        weatherStatus(nullable:false)
        windDirection(nullable:false)
        windSpeed(nullable:false)
        temp(nullable:false)
    }

    static mapping = {
        autoTimestamp true
        cache true
        table "dim_weather"
    }


    String weather
    String weatherStatus
    String windDirection
    String windSpeed
    double temp

    Date lastUpdated
    Date dateCreated

}
