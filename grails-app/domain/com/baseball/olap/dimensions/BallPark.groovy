package com.baseball.olap.dimensions

class BallPark {

    static constraints = {
        name(nullable:true, unique: 'teamAbbrev')
        teamAbbrev(nullable:true)
    }

    static mapping = {
        autoTimestamp true
        cache true
        table "dim_ballpark"
    }

    String name
    String teamAbbrev


    Date lastUpdated
    Date dateCreated



}
