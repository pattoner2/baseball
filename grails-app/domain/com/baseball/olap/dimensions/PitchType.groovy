package com.baseball.olap.dimensions

class PitchType {

    static constraints = {
        type(nullable:false)
    }

    static mapping = {
        autoTimestamp true
        cache true
        table "dim_pitch_type"
    }

    String type


}
