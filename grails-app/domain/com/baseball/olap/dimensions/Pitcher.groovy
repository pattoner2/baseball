package com.baseball.olap.dimensions

import com.baseball.Player

class Pitcher {

    static constraints = {
        pitcher(nullable:false, unique: ['hand', 'homeAway', 'gameOutcome'])
        hand(nullable:false)
        homeAway(nullable:false)
        gameOutcome(nullable:false)
    }


    static mapping = {
        autoTimestamp true
        cache true
        table "dim_pitcher"
    }

    Player pitcher
    HandValue hand
    HomeAwayValue homeAway
    GameOutcomeValue gameOutcome



    Date lastUpdated
    Date dateCreated



    public enum HandValue {

        R("R"),
        L("L"),
        NA("NA")

        private final String value
        HandValue(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }

    public enum HomeAwayValue {

        HOME("HOME"),
        AWAY("AWAY"),
        NA("NA")


        private final String value
        HomeAwayValue(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }

    public enum GameOutcomeValue {

        W("W"),
        L("L"),
        NA("NA")

        private final String value
        GameOutcomeValue(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }

}
