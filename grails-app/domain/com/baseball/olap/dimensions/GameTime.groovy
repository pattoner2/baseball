package com.baseball.olap.dimensions

class GameTime {

    static constraints = {
        gameDate(nullable:false, unique: ['day','month', 'year', 'dayOfWeek', 'monthName', 'half', 'gameStart'])

        day(nullable:false)
        month(nullable:false)
        year(nullable:false)

        dayOfWeek(nullable:false)
        monthName(nullable:false)
        half(nullable:false)
        gameStart(nullable:false)
    }

    static mapping = {
        autoTimestamp true
        cache true
        table "dim_game_time"
    }


    Date gameDate

    int day
    int month
    int year

    String dayOfWeek
    String monthName
    HalfName half //1ST, 2ND
    GameStart gameStart


    Date lastUpdated
    Date dateCreated


    public enum HalfName {

        FIRST("1ST"),
        SECOND("2ND")

        private final String value
        HalfName(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }


    public enum GameStart {

        EARLY("EARLY"),
        AFTERNOON("AFTERNOON"),
        NIGHT("NIGHT")

        private final String value
        GameStart(String value){ this.value = value; }
        String toString() { value }
        String getKey() { name() }
    }



}
