package com.baseball

class Runner {

    static constraints = {
        atBat(nullable:false)
        start(nullable:true)
        end(nullable:true)
        event(nullable:true)
        eventNum(nullable:true)
    }

    static mapping = {
        autoTimestamp true
    }


    AtBat atBat

    String start
    String end
    String event
    boolean score
    boolean earned
    Integer eventNum

    Date lastUpdated
    Date dateCreated


    public boolean isBalk() {
        return "Balk".equals(event)
    }

    public boolean isCatcherInterference() {
        return "Catcher Interference".equals(event)
    }

    public boolean isCs() {
        return "Caught Stealing 2B".equals(event)
        return "Caught Stealing 3B".equals(event)
        return "Caught Stealing Home".equals(event)
    }

    public boolean isDoublePlay() {
        return "Double Play".equals(event)
        return "Grounded Into DP".equals(event)
    }

    public boolean isHbp() {
        return "Hit By Pitch".equals(event)
    }

    public boolean isHomeRun() {
        return "Home Run".equals(event)
    }

    public boolean isIbb() {
        return "Intent Walk".equals(event)
    }

    public boolean isBb() {
        return "Walk".equals(event) || isIbb()
    }

    public boolean isLineout() {
        return "Lineout".equals(event)
    }

    public boolean isPassedBall() {
        return "Passed Ball".equals(event)
    }

    public boolean isPickoffStealing() {
        return "Picked off stealing 2B".equals(event)
        return "Picked off stealing 3B".equals(event)
        return "Picked off stealing home".equals(event)
    }


    public boolean isPickoff() {
        return "Picked off stealing 2B".equals(event)
        return "Picked off stealing 3B".equals(event)
        return "Picked off stealing home".equals(event)
        return "Pickoff 1B".equals(event)
        return "Pickoff 3B".equals(event)
    }

    public boolean isPickoffAttempt() {
        return "Pickoff Attempt 1B".equals(event)
        return "Pickoff Attempt 2B".equals(event)
        return "Pickoff Attempt 3B".equals(event)
    }

    public boolean isPickoffError() {
        return "Pickoff Error 1B".equals(event)
        return "Pickoff Error 2B".equals(event)
        return "Pickoff Error 3B".equals(event)
    }


    public boolean isPopout() {
        return "Pop Out".equals(event)
    }


    public boolean isRunnerInterference() {
        return "Runner Interference".equals(event)
    }

    public boolean isRunnerOut() {
        return "Runner Out".equals(event)
    }

    public boolean isSacBunt() {
        return "Sac Bunt".equals(event)
    }

    public boolean isSacFly() {
        return "Sac Fly".equals(event)
    }


    public boolean isSacFlyDp() {
        return "Sac Fly DP".equals(event)
    }

    public boolean isSingle() {
        return "Single".equals(event)
    }

    public boolean isStolenBase() {
        return "Stolen Base 2B".equals(event)
        return "Stolen Base 3B".equals(event)
        return "Stolen Base Home".equals(event)
    }


    public boolean isStrikeout() {
        return "Strikeout".equals(event)
        return "Strikeout - DP".equals(event)
    }

    public boolean isTriple() {
        return "Triple".equals(event)
    }



//
//
//
//
//
//
//    Wild Pitch




//    Bunt Groundout
//    Defensive Indiff
//    Double
//    Error
//    Fan interference
//    Field Error
//    Fielders Choice
//    Fielders Choice Out
//    Flyout
//    Forceout



}
