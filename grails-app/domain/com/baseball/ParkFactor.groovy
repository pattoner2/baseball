package com.baseball

class ParkFactor {

    static constraints = {

        team(nullable:false)
        lRuns(nullable:false)
        lHr(nullable:false)
        l1b(nullable:false)
        l2b(nullable:false)
        l3b(nullable:false)

        rRuns(nullable:false)
        rHr(nullable:false)
        r1b(nullable:false)
        r2b(nullable:false)
        r3b(nullable:false)
    }


    Team team

    Double lRuns
    Double lHr
    Double l1b
    Double l2b
    Double l3b


    Double rRuns
    Double rHr
    Double r1b
    Double r2b
    Double r3b



}
