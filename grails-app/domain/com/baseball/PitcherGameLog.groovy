package com.baseball

import com.baseball.domain.PitcherLog
import com.baseball.olap.dimensions.Pitcher

/**
 * Created by patricktoner on 5/19/16.
 */
class PitcherGameLog extends  PitcherLog {

    static constraints = {
        importFrom PitcherLog
        game(nullable:false, unique: ['pitcher'])
        pitcher(nullable:false)
        year(nullable:false)

        opponent(nullable:false)
        team(nullble:false)

        vegasWinPercent(nullable:true)
        vegasRunsAgainst(nullable:true)
        vegasStrikeouts(nullable:true)


        //Opposing team as a team
        opponentAverageRunsScored(nullable:true)

        soPercentSeason(nullable:true)
        sieraSeason(nullable:true)
        xFipSeason(nullable:true)
        wObaSeasonLeft(nullable:true)
        wObaSeasonRight(nullable:true)

        parkFactor(nullable:true)
        weatherFactor(nullable:true)
        windFactor(nullable:true)

        last5StartsAvgFanduelPoints(nullable:true)
        seasonAvgFanduelPoints(nullable:true)

        projection(nullable:true)
        value(nullable:true)

        opponentWFda(nullable:true)
        wFdaSeason(nullable:true)
        wFdaSeasonHomeAway(nullable:true)
        wFdaSeasonLeft(nullable:true)
        wFdaSeasonRight(nullable:true)

        projectedOpponentPoints(nullable:true)

        gamedayPopulated(nullable:true)

        fanduelPoints(nullable:true)
        fanduelSalary(nullable:true)

        dateCreated(nullable:true)
        lastUpdated(nullable:true)
    }

    static mapping = {
        autoTimestamp(true)
        version(false)
        pitcher fetch: 'join', lazy: false
    }

    Game game
    Integer year

    Player pitcher

    Team opponent
    Team team



    Double vegasWinPercent
    Double vegasRunsAgainst
    Double vegasStrikeouts


    //Opposing team as a team
    Double opponentAverageRunsScored


    Double soPercentSeason
    Double sieraSeason
    Double xFipSeason

    Double wObaSeasonLeft
    Double wObaSeasonRight

    Double wFdaSeasonLeft
    Double wFdaSeasonRight

    Double wFdaSeasonHomeAway //whichever this game happens to be
    Double wFdaSeason

    Double parkFactor
    Double weatherFactor
    Double windFactor

    Double last5StartsAvgFanduelPoints
    Double seasonAvgFanduelPoints

    Double projectedOpponentPoints

    Double projection
    Double value


    Double fanduelPoints
    Integer fanduelSalary

    boolean gamedayPopulated

    Date dateCreated
    Date lastUpdated



    public String getIp() {
        if (!outs || outs == 0) return "0.0"

        int fullIp = outs / 3
        int remainder = outs % 3

        return fullIp + "." + remainder


    }


    public Player getOpponentPitcher() {

        if (game.homeTeam?.id == opponent.id) {
            //I'm away
            return  (game.homeStartingPitcher ? game.homeStartingPitcher : game.homeProbablePitcher)
        } else {
            return (game.awayStartingPitcher ? game.awayStartingPitcher : game.awayProbablePitcher)
        }



    }

    public boolean isHome() {

        if (game.homeTeam?.id == opponent?.id) return false
        return true

    }

    public boolean isProbablePitcher() {

        if (game.awayProbablePitcher?.id == pitcher.id || game.homeProbablePitcher?.id == pitcher.id || game.homeStartingPitcher?.id == pitcher.id || game.awayProbablePitcher?.id == pitcher.id) return true
        return false

    }


    public String getGameId() {
        if (isHome()) return game.id + "-H"
        return game.id + "-A"
    }


    public String getGameOutcome() {
        if (isHome() && (game.homeTeamRuns > game.awayTeamRuns)) return "W"
        return "L"
    }

    public Pitcher.HomeAwayValue getHomeAway() {
        if (team.id == game.homeTeam.id) return Pitcher.HomeAwayValue.HOME
        return Pitcher.HomeAwayValue.AWAY
    }



}
