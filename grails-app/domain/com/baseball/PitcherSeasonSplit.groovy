package com.baseball

import com.baseball.domain.PitcherLog

/**
 * Created by patricktoner on 5/19/16.
 */
class PitcherSeasonSplit extends PitcherLog {

    static constraints = {
        importFrom PitcherLog

        year(nullable:false, unique: ['pitcher', 'split'])
        split(nullable:false)


        dateCreated(nullable:true)
        lastUpdated(nullable:true)
    }

    static mapping = {
        autoTimestamp(true)
        pitcher fetch: 'join', lazy: false
    }

    Integer year
    String split


    Date dateCreated
    Date lastUpdated



    public String getIp() {

        if (outs == null || outs == 0) return "0.0"

        int fullIp = outs / 3
        int remainder = outs % 3

        return fullIp + "." + remainder


    }

    public Double getFanduelAvg() {
        if (games == null || games == 0 || fanduelPoints == null) return 0
        return fanduelPoints / games
    }



}
