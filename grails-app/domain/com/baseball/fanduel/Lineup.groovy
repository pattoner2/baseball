package com.baseball.fanduel

class Lineup {

    static constraints = {
        player(nullable: false)
        contest(nullable:false)
    }


    DfsPlayer player
    Contest contest

    double score

}
