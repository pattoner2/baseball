package com.baseball.fanduel

class DfsPlayer {

    static constraints = {
        fanduelName(nullable:true, unique: true)
        rotogrindersName(nullable:true, unique: true)
    }

    static mapping = {
        autoTimestamp true
    }

    String rotogrindersName
    String fanduelName


    int overallRank
    int nbaRank




    Date lastUpdated
    Date dateCreated

}
