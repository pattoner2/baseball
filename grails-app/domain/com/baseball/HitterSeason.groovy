package com.baseball

import com.baseball.domain.HitterLog

/**
 * Created by patricktoner on 5/19/16.
 */
class HitterSeason extends HitterLog {

    static constraints = {
        importFrom HitterLog

        year(nullable:false,  unique: ['hitter'] )

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        hitter fetch: 'join', lazy: false
    }

    Integer year


    Date lastUpdated
    Date dateCreated



    public Double getFanduelAvg() {
        if (games == null || games == 0 || fanduelPoints == null) return 0
        return fanduelPoints / games
    }

}
