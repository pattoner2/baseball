package com.baseball

import com.baseball.domain.HitterLog

/**
 * Created by patricktoner on 5/19/16.
 */
class HitterCareerSplit extends HitterLog {

    static constraints = {
        importFrom HitterLog

        split(nullable:false)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        hitter fetch: 'join', lazy: false
    }

    String split


    Date lastUpdated
    Date dateCreated


    public Double getFanduelAvg() {
        if (games == null || games == 0 || fanduelPoints == null) return 0
        return fanduelPoints / games
    }


    public boolean isHandedness() {

        if ("L".equals(split) || "R".equals(split)) return true
        return false

    }

    public boolean isHomeAway() {
        if ("H".equals(split) || "A".equals(split)) return true
        return false
    }


}
