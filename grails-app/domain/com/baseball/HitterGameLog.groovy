package com.baseball

import com.baseball.domain.HitterLog
import com.baseball.olap.dimensions.Hitter
import com.baseball.olap.dimensions.Pitcher

/**
 * Created by patricktoner on 5/19/16.
 */
class HitterGameLog extends HitterLog {

    static constraints = {

        importFrom HitterLog
        game(nullable:false,  unique: ['hitter'] )
        hitter(nullable:false)
        year(nullable: false)

        opponent(nullable:false)
        team(nullable:false)

        battingOrder(nullable:true)
        battingOrderConfirmed(nullable:true)
        battingOrderUnconfirmed(nullable:true)


        fanduelPoints(nullable:true)
        fanduelSalary(nullable:true)


        vegasProjectedRunsScored(nullable:true)
        opponentWObaAgainstHandedness(nullable:true)
        opponentWObaAgainstHandednessDiff(nullable:true)

        opponentWObaSeason(nullable:true)
        wObaAgainstHandednessCareer(nullable:true)
        wObaAgainstHandednessSeason(nullable:true)
        wObaAgainstHandednessDiff(nullable:true)

        isoAgainstHandednessCareer(nullable:true)
        isoAgainstHandednessSeason(nullable:true)
        isoAgainstHandednessDiff(nullable:true)
        wObaSeason(nullable:true)


        wFdaSeason(nullable:true)
        wFdaAgainstHandednessCareer(nullable:true)
        wFdaAgainstHandednessSeason(nullable:true)
        wFdaAgainstHandednessDiff(nullable:true)
        wFdaHomeAwaySeason(nullable:true)
        wfdaHomeAwayDiff(nullable:true)


        opponentWFdaSeason(nullable:true)
        opponentWFdaAgainstHandedness(nullable:true)
        opponentWFdaAgainstHandednessDiff(nullable:true)
        opponentWfdaHomeAwaySeason(nullable:true)
        opponentWfdaHomeAwayDiff(nullable:true)

        careerWfdaAgainstStarter(nullable:true)
        careerAtBatsAgainstStarter(nullable:true)

        battingOrderDiffFromLast7(nullable:true)


        parkFactor(nullable:true)
        weatherFactor(nullable:true)
        windFactor(nullable:true)

        last7AvgFanduelPoints(nullable:true)
        last15AvgFanduelPoints(nullable:true)
        seasonAvgFanduelPoints(nullable:true)

        projection(nullable:true)
        value(nullable:true)

        gamedayPopulated(nullable:true)

        hrPercent(nullable:true)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        version(false)
        hitter fetch: 'join', lazy: false
    }



    Game game
    Integer year

    Player hitter

    Team opponent
    Team team

    Integer battingOrder
    Integer battingOrderConfirmed //rotogrinders
    Integer battingOrderUnconfirmed //rotogrinders


    Double vegasProjectedRunsScored

    Double opponentWObaSeason
    Double opponentWObaAgainstHandedness
    Double opponentWObaAgainstHandednessDiff

    Double wObaSeason
    Double wObaAgainstHandednessCareer
    Double wObaAgainstHandednessSeason
    Double wObaAgainstHandednessDiff


    Double wFdaSeason

    Double wFdaAgainstHandednessCareer
    Double wFdaAgainstHandednessSeason
    Double wFdaAgainstHandednessDiff

    Double wFdaHomeAwaySeason
    Double wfdaHomeAwayDiff


    Double opponentWFdaSeason
    Double opponentWFdaAgainstHandedness
    Double opponentWFdaAgainstHandednessDiff

    Double opponentWfdaHomeAwaySeason
    Double opponentWfdaHomeAwayDiff


    Double careerWfdaAgainstStarter
    Double careerAtBatsAgainstStarter


    Double isoAgainstHandednessCareer
    Double isoAgainstHandednessSeason
    Double isoAgainstHandednessDiff

    Double battingOrderDiffFromLast7

    Double parkFactor
    Double weatherFactor
    Double windFactor

    Double hrPercent

    Double last7AvgFanduelPoints
    Double last15AvgFanduelPoints
    Double seasonAvgFanduelPoints

    Double projection
    Double value

    boolean gamedayPopulated

    Double fanduelPoints
    Integer fanduelSalary

    Date lastUpdated
    Date dateCreated


    public Player getOpponentPitcher() {

        if (game.homeTeam?.id == opponent.id) {
            //I'm away
            return  (game.homeStartingPitcher ? game.homeStartingPitcher : game.homeProbablePitcher)
        } else {
            return (game.awayStartingPitcher ? game.awayStartingPitcher : game.awayProbablePitcher)
        }



    }

    public boolean isHome() {

        if (game.homeTeam.id == opponent.id) return false
        return true

    }


    public int getKnownBattingOrder() {

        String lineupStatus = (isHome() ? game.homeLineupStatus : game.awayLineupStatus)

        if (battingOrder > 0) return battingOrder;
        if ("official".equals(lineupStatus)) return -1


        if (battingOrderConfirmed > 0) return battingOrderConfirmed;
        if ("confirmed".equals(lineupStatus)) return -1


        if (battingOrderUnconfirmed > 0) return battingOrderUnconfirmed

        return -1

    }


    public String getBattingOrderStatus() {

        String lineupStatus = (isHome() ? game.homeLineupStatus : game.awayLineupStatus)

        if (battingOrder > 0) return "official";
        if ("official".equals(lineupStatus)) return "out"

        if (battingOrderConfirmed > 0) return "confirmed";
        if ("confirmed".equals(lineupStatus)) return "out"

        if (battingOrderUnconfirmed > 0) return "unconfirmed"
        return "out"

    }

    public String getGameId() {
        if (isHome()) return game.id + "-H"
        return game.id + "-A"
    }


    public Integer getGames() {
        if (gamedayPopulated) return 1
        return 0
    }


    public String getGameOutcome() {
        if (isHome() && (game.homeTeamRuns > game.awayTeamRuns)) return "W"
        return "L"
    }

    public Hitter.HomeAwayValue getHomeAway() {
        if (team.id == game.homeTeam.id) return Hitter.HomeAwayValue.HOME
        return Hitter.HomeAwayValue.AWAY
    }


}
