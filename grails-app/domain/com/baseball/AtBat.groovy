package com.baseball

/**
 * Created by patricktoner on 5/19/16.
 */
class AtBat {

    static constraints = {

        game(nullable:false, unique: ['num', 'eventNum'])
        year(nullable: false)
        eventNum(nullable:true)

        inningNum(nullable:false)
        inningTop(nullable:false)


        hitter(nullable:false)
        pitcher(nullable:false)

        startTimeUtc(nullable:true)

        num(nullable:true)
        balls(nullable:true)
        strikes(nullable:true)
        outs(nullable:true)

        description(nullable:true)
        event(nullable:true)

        score(nullable:true)
        awayTeamRuns(nullable:true)
        homeTeamRuns(nullable:true)

        baseRunner1Id(nullable:true)
        baseRunner2Id(nullable:true)
        baseRunner3Id(nullable:true)

        rbi(nullable:true)
        earnedRuns(nullable:true)

        pitcherHandedness(nullable:true)
        hitterHandedness(nullable:true)

        pitcherHomeAway(nullable:true)
        hitterHomeAway(nullable:true)

        pc(nullable:true)

        lastUpdated(nullable:true)
        dateCreated(nullable:true)

    }

    static mapping = {
        autoTimestamp(true)
        description sqlType: "text"
        game fetch: 'join', lazy: false
        hitter fetch: 'join', lazy: false
        pitcher fetch: 'join', lazy: false
    }

    Game game
    Integer year
    Integer eventNum
    Integer inningNum
    boolean inningTop


    Player hitter
    Player pitcher

    Date startTimeUtc

    Integer num
    Integer balls
    Integer strikes
    Integer outs

    String description
    String event

    boolean score
    Integer awayTeamRuns
    Integer homeTeamRuns

    Integer baseRunner1Id
    Integer baseRunner2Id
    Integer baseRunner3Id

    Integer rbi
    Integer earnedRuns

    String pitcherHandedness
    String hitterHandedness

    String pitcherHomeAway
    String hitterHomeAway

    Integer pc //pitchcount
    Integer pitchesBalls
    Integer pitchesStrikes


    Date lastUpdated
    Date dateCreated



    public boolean isSacFly() {
        return ("Sac Fly".equals(event))
    }

    public boolean isHit() {
        return ("Single".equals(event) || "Double".equals(event) || "Triple".equals(event) || "Home Run".equals(event))
    }

    public boolean isBB() {
        return ("Walk".equals(event) || "Intent Walk".equals(event))
    }

    public boolean isIBB() {
        return ("Intent Walk".equals(event))
    }


    public boolean isHpb() {
        return ("Hit By Pitch".equals(event))
    }

    public boolean isSingle() {
        return "Single".equals(event)
    }

    public boolean isDouble() {
        return "Double".equals(event)
    }

    public boolean isTriple() {
        return "Triple".equals(event)
    }

    public boolean isHr() {
        return "Home Run".equals(event)
    }

    public boolean isOut() {

    }

    public boolean isStrikeout() {
        return ("Strikeout".equals(event) || "Strikeout - DP".equals(event))
    }

    public boolean isAtBat() {

        if ("Intent Walk".equals(event)) return false
        if ("Hit By Pitch".equals(event)) return false
        if ("Sac Bunt".equals(event)) return false
        if ("Sac Fly".equals(event)) return false
        if ("Sac Fly DP".equals(event)) return false
        if ("Walk".equals(event)) return false
        if ("Catcher Interference".equals(event)) return false
        if ("Runner Out".equals(event)) return false
        if ("Ejection".equals(event)) return false

        return true

    }

    public boolean isPa() {
        if ("Ejection".equals(event)) return false
        if ("Runner Out".equals(event)) return false
        return true
    }


    public boolean isGroundBall() {
        if ("Bunt Groundout".equals(event)) return true
        if ("Grounded Into DP".equals(event)) return true
        if ("Groundout".equals(event)) return true
        if ("Fielders Choice".equals(event)) return true
        if ("Fielders Choice Out".equals(event)) return true
        if ("Force Out".equals(event)) return true
        if ("Sac Bunt".equals(event)) return true
        if ("Sacrifice Bunt DP".equals(event)) return true


        if (description?.contains(" ground ball ")) return true
        if (description?.contains(" grounds out ")) return true
        if (description?.contains(" grounds into ")) return true
    }

    public boolean isGroundOut() {
        return (groundBall && isAnOut())
    }


    public boolean isLineDrive() {

        if ("Lineout".equals(event)) return true
        if ("Bunt Lineout".equals(event)) return true
        if ("Bunt Pop Out".equals(event)) return true


        if (description?.contains(" line drive ")) return true
        if (description?.contains(" lines out ")) return true
        if (description?.contains(" lines into ")) return true
    }

    public boolean isLineOut() {
        return (lineDrive && isAnOut())
    }

    public boolean isFlyBall() {
        if ("Flyout".equals(event)) return true
        if ("Pop Out".equals(event)) return true
        if ("Sac Fly".equals(event)) return true
        if ("Sac Fly DP".equals(event)) return true

        if (description?.contains(" bunt pops ")) return true
        if (description?.contains(" pops out ")) return true
        if (description?.contains(" pops into ")) return true
        if (description?.contains(" flies out ")) return true
        if (description?.contains(" fly ball ")) return true
        if (description?.contains(" flies into ")) return true

        return false
    }

    public boolean isFlyOut() {
        return (flyBall && isAnOut())
    }

    public boolean isAnOut() {

        if ("Batter Interference".equals(event)) return true
        if ("Bunt Groundout".equals(event)) return true
        if ("Bunt Lineout".equals(event)) return true
        if ("Bunt Pop Out".equals(event)) return true
        if ("Fan interference".equals(event)) return true
        if ("Fielders Choice Out".equals(event)) return true
        if ("Flyout".equals(event)) return true
        if ("Forceout".equals(event)) return true
        if ("Grounded Into DP".equals(event)) return true
        if ("Groundout".equals(event)) return true
        if ("Lineout".equals(event)) return true
        if ("Pop Out".equals(event)) return true
        if ("Sac Bunt".equals(event)) return true
        if ("Sac Fly".equals(event)) return true
        if ("Sac Fly DP".equals(event)) return true
        if ("Sacrifice Bunt DP".equals(event)) return true
        if ("Strikeout".equals(event)) return true
        if ("Strikeout - DP".equals(event)) return true
        if ("Triple Play".equals(event)) return true

        return false
    }


}
