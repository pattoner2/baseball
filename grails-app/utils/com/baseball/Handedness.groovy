package com.baseball
/**
 * Created by patricktoner on 5/19/16.
 */
enum Handedness {
    LEFT, RIGHT, SWITCH
}