package com.baseball

import com.baseball.HitterService
import com.baseball.PitcherService

class PlayerController {

    HitterService hitterService
    PitcherService pitcherService


    public list() {

        if (!params.offset) params.offset=0
        if (!params.max) params.max = 50

        params.sort = "lastName"

        [players: Player.list(params), playerCount: Player.count()]


    }



    public show(Long id) {

        Player player = Player.get(id)


        List<HitterSeason> hittingSeasons = HitterSeason.withCriteria {
            hitter{
                eq('id', id)
            }

            order('year', 'asc')
        }


        List<PitcherSeason> pitcherSeasons = PitcherSeason.withCriteria {
            pitcher{
                eq('id', id)
            }

            order('year', 'asc')
        }


        List<HitterSeasonSplit> hittingSeasonSplits = HitterSeasonSplit.withCriteria {
            hitter{
                eq('id', id)
            }

            order('year', 'asc')
        }

        List<PitcherSeasonSplit> pitcherSeasonSplits = PitcherSeasonSplit.withCriteria {
            pitcher{
                eq('id', id)
            }

            order('year', 'asc')
        }



        List<HitterSeasonSplit> leftHitterSplits = hittingSeasonSplits.findAll { "L".equals(it.split)}
        List<HitterSeasonSplit> rightHitterSplits = hittingSeasonSplits.findAll { "R".equals(it.split)}
        List<HitterSeasonSplit> homeHitterSplits = hittingSeasonSplits.findAll { "H".equals(it.split)}
        List<HitterSeasonSplit> awayHitterSplits = hittingSeasonSplits.findAll { "A".equals(it.split)}

        List<HitterSeasonSplit> recentHitterSplits = hittingSeasonSplits.findAll {
            "7".equals(it.split) || "15".equals(it.split) || "30".equals(it.split)
        }.sort {
            Integer.parseInt(it.split)
        }


        List<PitcherSeasonSplit> leftPitcherSplits = pitcherSeasonSplits.findAll { "L".equals(it.split)}
        List<PitcherSeasonSplit> rightPitcherSplits = pitcherSeasonSplits.findAll { "R".equals(it.split)}
        List<PitcherSeasonSplit> homePitcherSplits = pitcherSeasonSplits.findAll { "H".equals(it.split)}
        List<PitcherSeasonSplit> awayPitcherSplits = pitcherSeasonSplits.findAll { "A".equals(it.split)}

        List<PitcherSeasonSplit> recentPitcherSplits = pitcherSeasonSplits.findAll {
            "7".equals(it.split) || "15".equals(it.split) || "30".equals(it.split)
        }.sort {
            Integer.parseInt(it.split)
        }


        HitterCareer hitterCareer = HitterCareer.findByHitter(player)
        PitcherCareer pitcherCareer = PitcherCareer.findByPitcher(player)


        List<HitterCareerSplit> hitterCareerSplits = HitterCareerSplit.findAllByHitter(player)
        List<PitcherCareerSplit> pitcherCareerSplits = PitcherCareerSplit.findAllByPitcher(player)


        [player: player,
         hitterCareer: hitterCareer,
         pitcherCareer: pitcherCareer,
         hitterCareerSplits: hitterCareerSplits,
         pitcherCareerSplits: pitcherCareerSplits,
         recentHitterSplits: recentHitterSplits,
         recentPitcherSplits: recentPitcherSplits,
         leftHitterSplits: leftHitterSplits,
         rightHitterSplits: rightHitterSplits,
         homeHitterSplits: homeHitterSplits,
         awayHitterSplits: awayHitterSplits,
         leftPitcherSplits: leftPitcherSplits,
         rightPitcherSplits: rightPitcherSplits,
         homePitcherSplits: homePitcherSplits,
         awayPitcherSplits: awayPitcherSplits,
         hittingSeasonSplits: hittingSeasonSplits,
         pitcherSeasonSplits: pitcherSeasonSplits,
         hittingSeasons: hittingSeasons,
         pitcherSeasons: pitcherSeasons]


    }


    public popupHitter(Long id) {

        Player player = Player.get(id)


        HitterSeason hitterSeason = HitterSeason.findByHitterAndYear(player, new Date()[Calendar.YEAR])

        List<HitterSeasonSplit> hitterSeasonSplits = hitterService.getHitterSeasonSplits(player, new Date()[Calendar.YEAR])
        List<HitterGameLog> recentGameLogs = hitterService.getRecentGameLogsMax(hitterSeason, 10)


        HitterGameLog todayHitterGameLog = hitterService.getGameLogForDate(player, new Date())

        [player: player,
         todayHitterGameLog: todayHitterGameLog,
         hitterSeason: hitterSeason,
         recentGameLogs: recentGameLogs,
         hittingSeasonSplits: hitterSeasonSplits
        ]


    }





    public popupPitcher(Long id) {

        Player player = Player.get(id)


        PitcherSeason pitcherSeason = PitcherSeason.findByPitcherAndYear(player, new Date()[Calendar.YEAR])

        List<PitcherSeasonSplit> pitcherSeasonSplits = hitterService.getHitterSeasonSplits(player, new Date()[Calendar.YEAR])
        List<PitcherGameLog> recentGameLogs = pitcherService.getRecentStarts(pitcherSeason, 10)




        [player: player,
         pitcherSeason: pitcherSeason,
         recentGameLogs: recentGameLogs,
         pitcherSeasonSplits: pitcherSeasonSplits
        ]


    }



    public hittingLog(int year, Long id) {

        Player thePlayer = Player.get(id)

        List<HitterGameLog> gameLogList = HitterGameLog.withCriteria {

            createAlias('game', 'g')
            hitter {
                eq('id', id)
            }

            eq('year', year)
            order("g.startDate", "desc")
        }


        [player: thePlayer,
         year: year,
         gameLogList: gameLogList]

    }


    public pitchingLog(int year, Long id) {

        Player player = Player.get(id)

        List<PitcherGameLog> gameLogList = PitcherGameLog.withCriteria {

            createAlias('game', 'g')
            pitcher {
                eq('id', id)
            }

            eq('year', year)
            order("g.startDate", "desc")
        }

        [player: player,
         year: year,
         gameLogList: gameLogList]
    }


}
