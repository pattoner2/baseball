package com.baseball

import com.baseball.PlayerService
import grails.converters.JSON

class SearchController {


    PlayerService playerService


    def searchJson(String query) {


        List<Player> playerList = playerService.findPlayer(query)


        def result = []

        result.addAll(playerList?.collect {
                [id                : it.id,
                 name         : it.firstName + " " + it.lastName
                ]
        }
        )


        render result as JSON
    }





}
