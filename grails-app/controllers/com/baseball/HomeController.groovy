package com.baseball

import com.baseball.AdvancedStatService
import com.baseball.EtlService
import com.baseball.FanduelService
import com.baseball.GameService
import com.baseball.HitterService
import com.baseball.MlbGamedayService
import com.baseball.PitcherService
import com.baseball.PlayerService
import com.baseball.ProjectionService
import com.baseball.RotogrindersService
import com.mdimension.jchronic.Chronic
import org.hibernate.SessionFactory

import static groovyx.gpars.GParsPool.withPool

class HomeController {


    MlbGamedayService mlbGamedayService
    PlayerService playerService
    HitterService hitterService
    PitcherService pitcherService
    GameService gameService
    FanduelService fanduelService
    RotogrindersService rotogrindersService
    ProjectionService projectionService
    AdvancedStatService advancedStatService
    EtlService etlService

    SessionFactory sessionFactory

    def index(Long time) {

        Date date = new Date();

        if (time) {
            date = new Date(time * 1000 as Long)
        }

        List<Game> games = gameService.getGamesForDate(date)

        List<HitterGameLog> hitterGameLogs = gameService.getHitterGameLogsForDate(date)
        List<PitcherGameLog> pitcherGameLogs = gameService.getProbablePitcherGameLogs(date)


        [games: games,
         hitterGameLogs: hitterGameLogs,
         pitcherGameLogs: pitcherGameLogs,
         date: date
        ]

    }





    def processSeasonStats(int year) {
        mlbGamedayService.processSeasonStats(year)
    }


    def processAdvancedStats(int year) {
        hitterService.processAdvacedStatsForSeason(year)
        pitcherService.processAdvancedStatsForSeason(year)
    }

    def downloadSeasonStats(int year, int year2) {

        if (year2 < year) year2 = year


        for (int i=year; i < year2+1; i++) {
            mlbGamedayService.downloadSeason(i)
        }


    }



    def processNew() {

        def hibSession = sessionFactory.getCurrentSession()


        mlbGamedayService.processNew()

        Date date = new Date()
        int year = date[Calendar.YEAR]

        //mlbGamedayService.processSeasonStats(year)
        rotogrindersService.updateToday()
        fanduelService.updateToday()

        etlService.processDate(date)

        hibSession.flush()
        hibSession.clear()

        hitterService.updateAdvancedStatsForDate(date)
        pitcherService.updateAdvancedStatsForDate(date)


    }


    def processToday() {

        mlbGamedayService.download(new Date())
        //mlbGamedayService.processSeasonStats(new Date()[Calendar.YEAR])
        rotogrindersService.updateToday()


        try {
            fanduelService.updateToday()
        } catch (Exception ex) {
            ex.printStackTrace()
            println "******************************************"
            println "******************************************"
            println "******************************************"
            println "*********ERROR PROCESSING FANDUEL*********"
            println "******************************************"
            println "******************************************"
            println "******************************************"
        }


        etlService.processDate(new Date())

        hitterService.updateAdvancedStatsForDate(new Date())
        pitcherService.updateAdvancedStatsForDate(new Date())
    }


    def updateAdvancedStats() {

        hitterService.updateAdvancedStatsForDate(new Date())
        pitcherService.updateAdvancedStatsForDate(new Date())


    }



    def processCurrentSeason() {

        mlbGamedayService.downloadSeason(new Date()[Calendar.YEAR])
        mlbGamedayService.processSeasonStats(new Date()[Calendar.YEAR])
    }


    def processAllSeasons() {

        for (int i=2008; i< 2017; i++) {
            mlbGamedayService.processSeasonStats(i)

        }
    }

    def downloadAllSeasons() {

        List<Integer> years = new ArrayList<>()

        for (int i=2008; i< 2017; i++) {
            years.add(i)
        }

       // withPool(4) {
            years.each {
                mlbGamedayService.downloadSeason(it)
            }
        //}

    }


    def processFanduel() {
        fanduelService.updateToday()
    }

    def processRotogrinders() {
        rotogrindersService.updateToday()
    }

    def processEtl() {
        etlService.processDate(new Date())
    }


    def test() {

//        fanduelService.getTodaysNbaContests()


        Date startDate = Chronic.parse('march 26 2008')?.beginCalendar?.time

        mlbGamedayService.download(startDate)



//        etlService.processDimensionsForSeason(2008)
//        etlService.processDimensionsForSeason(2009)
//        etlService.processDimensionsForSeason(2010)
//        etlService.processDimensionsForSeason(2011)
//        etlService.processDimensionsForSeason(2012)
//        etlService.processDimensionsForSeason(2013)
//        etlService.processDimensionsForSeason(2014)
//        etlService.processDimensionsForSeason(2015)

//        mlbGamedayService.processSeasonStats(2016)
//
//
//        Date startDate = new Date()
//        mlbGamedayService.download(startDate)

//        //playerService.updatePitcherSeasonStats(Player.get(new Long(2086)), 2009)
//        //mlbGamedayService.download(startDate)
//
//        Player pitcher = Player.get(358)
//
//        for (int i=2011; i< 2017; i++) {
//            pitcherService.updatePitcherSeasonStats(pitcher, i)
//        }

       // fanduelService.updateToday()







//

//        Player player = Player.get(259)
//        hitterService.updateHitterSeasonStats(player, 2008)


//        hitterService.updateAdvancedStatsForGameLog(HitterGameLog.get(2952266))


//
//
//
//
//
//




//        List<Date> dates = []
//
//        Date startDate = Chronic.parse('sept 22 2015')?.beginCalendar?.time
//
//        dates.add(startDate)
//
//        Date endDate = Chronic.parse('nov 15 2015')?.beginCalendar?.time
//
//        startDate.upto(endDate) {
//            dates.add(it)
//        }


//
//        Date startDate = Chronic.parse('july 26 2016')?.beginCalendar?.time
//        mlbGamedayService.download(startDate)
//
//        rotogrindersService.updateToday()
//




//
//        Date date = new Date()
//        date.clearTime()
//
//        def results = HitterGameLog.withCriteria {
//            createAlias("game", "g")
//            hitter {
//                eq('id', new Long(752))
//            }
//            between("g.startDate", date, date + 1)
//        }
//
//
//
//
//        projectionService.calculateProjection(results.get(0))
//


//        def result = pitcherService.updatePitcherSeasonStats(Player.get(454), 2016)

//        pitcherService.updateAdvancedStatsForDate(new Date())


//        rotogrindersService.generateRotogrinderProjectionsForDate(new Date())

    }



    def updateParkFactors() {

        rotogrindersService.updateParkFactors()

    }


    def processAll() {

        for (int i=2008; i< 2017; i++) {
            mlbGamedayService.downloadSeason(i)
        }

    }

}
