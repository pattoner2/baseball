package com.baseball

class HotColdTagLib {
    //static defaultEncodeAs = [taglib: 'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]


    def heated = { attrs, body ->


        boolean hot = false

        String name = attrs.name
        String format = attrs.format
        String inverse = attrs.inverse
        String subtext = attrs.subtext

        Double value = attrs.value ? attrs.value : 0
        Double average = attrs.average
        List<Double> list = attrs.list


        String style = ''
        String formattedValue = ''

        if (value) {
            formattedValue = value
        }






        if (list.size() > 0) {

            list.sort()


            int percentile = (list.findAll { it < value }.size() / list.size()) * 100

            if ("true".equals(inverse)) {
                percentile = 100 - percentile
            }



            if (percentile > 50) hot = true



            switch (format) {

                case "two":
                    formattedValue = value?.formatTwo()
                    break;
                case "three":
                    formattedValue = value?.formatThree()
                    break;
                case "percent":
                    formattedValue = value?.formatPercent()
                    break;
            }



            if (formattedValue && attrs.value) {


                if (hot) {
                    percentile = percentile - (100 - percentile)



                    if (percentile > 0) {
                        if (percentile == 100) {
                            style = "background-color: rgba(255, 165, 0, 1.0);"
                        } else {
                            style = "background-color: rgba(255, 165, 0, ${"." + String.format("%02d", percentile)});"
                        }

                    }

                } else {
                    percentile =  percentile + (100+percentile) - 100

                    if (percentile > 0 || formattedValue?.length() > 0) {

                        percentile = -(percentile-100)

                        if (percentile == 100 && formattedValue?.length() > 0) {
                            style = "background-color: rgba(167, 190, 204, 1.0);"
                        }else {
                            style = "background-color: rgba(167, 190, 204, ${"." + String.format("%02d", percentile)});"
                        }

                    }

                }

            }

        }

        if (subtext) {
            subtext = '(' + subtext + ')'
        }


        out << "<td name='${name}' style='${style}' data-sort='${value}'>${formattedValue} ${subtext ? subtext : ''}</td>"





    }
}