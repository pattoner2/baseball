<g:applyLayout name="main">

    <head>

    </head>





    <content tag="content">

        <h1><a href="/player/show/${player.id}">${player.fullName}</a> <small>${year}</small></h1>




        <g:if test="${gameLogList.size() > 0}">
            <h2>Game Log</h2>
            <table class="table table-bordered stats">

                <tr>
                    <th>Date</th>
                    <th>AB</th>
                    <th>H</th>
                    <th>1B</th>
                    <th>2B</th>
                    <th>3B</th>
                    <th>HR</th>
                    <th>R</th>
                    <th>RBI</th>
                    <th>BB</th>
                    <th>HPB</th>
                    <th>SB</th>
                    <th>CS</th>

                    <th>HPB</th>
                    <th>SO</th>
                    <th>LOB</th>
                    <th>SAC B</th>
                    <th>SAC F</th>
                    <th>GO</th>
                    <th>FO</th>
                    <th>GIDP</th>
                    <th>.AVG</th>

                    <th>FD</th>
                </th>
                </tr>

                <g:each in="${gameLogList}" var="gameLog">
                    <tr>
                        <td>
                            <g:formatDate format="yyyy-MM-dd" date="${gameLog.game.startDate}"/>
                        </td>

                        <td>${gameLog.atBats}</td>
                        <td>${gameLog.hits}</td>
                        <td>${gameLog.singles}</td>
                        <td>${gameLog.doubles}</td>
                        <td>${gameLog.triples}</td>
                        <td>${gameLog.homeRuns}</td>
                        <td>${gameLog.runs}</td>
                        <td>${gameLog.rbi}</td>
                        <td>${gameLog.bb}</td>
                        <td>${gameLog.hbp}</td>
                        <td>${gameLog.sb}</td>
                        <td>${gameLog.cs}</td>

                        <td>${gameLog.hbp}</td>
                        <td>${gameLog.so}</td>
                        <td>${gameLog.lob}</td>
                        <td>${gameLog.sacBunts}</td>
                        <td>${gameLog.sacFlys}</td>
                        <td>${gameLog.groundOuts}</td>
                        <td>${gameLog.flyOuts}</td>
                        <td>${gameLog.gidp}</td>

                        <td>${gameLog.avg?.formatThree()}</td>
                        <td>${gameLog.fanduelPoints?.formatTwo()}</td>
                    </tr>

                </g:each>
            </table>
        </g:if>



    </content>



</g:applyLayout>
