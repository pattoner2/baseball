<g:applyLayout name="main">

    <head>

    </head>





    <content tag="content">

        <h1><a href="/player/show/${player.id}">${player.fullName}</a> <small>${year}</small></h1>




        <g:if test="${gameLogList.size() > 0}">
            <h2>Game Log</h2>

            <table class="table table-bordered stats">

                <tr>
                    <th>Date</th>
                    <th>W</th>
                    <th>L</th>
                    <th>S</th>
                    <th>BS</th>
                    <th>IP</th>
                    <th>ER</th>
                    <th>SO</th>
                    <th>H</th>
                    <th>BB</th>
                    <th>SHO</th>
                    <th>CG</th>
                    <th>PC</th>
                    <th>BF</th>
                    <th>Strikes</th>
                    <th>R</th>
                    <th>HR</th>
                    <th>FD</th>
                </tr>


                <g:each in="${gameLogList}" var="gameLog">

                    <tr>
                        <td>
                            <g:formatDate format="yyyy-MM-dd" date="${gameLog.game.startDate}"/>
                        </td>

                        <td>${gameLog.wins}</td>
                        <td>${gameLog.losses}</td>
                        <td>${gameLog.saves}</td>
                        <td>${gameLog.bs}</td>
                        <td>${gameLog.ip}</td>
                        <td>${gameLog.er}</td>
                        <td>${gameLog.so}</td>
                        <td>${gameLog.h}</td>
                        <td>${gameLog.bb}</td>
                        <td>${gameLog.sho}</td>
                        <td>${gameLog.cg}</td>
                        <td>${gameLog.pc}</td>


                        <td>${gameLog.battersFace}</td>
                        <td>${gameLog.strikes}</td>
                        <td>${gameLog.runs}</td>
                        <td>${gameLog.hr}</td>
                        <td>${gameLog.fanduelPoints?.formatTwo()}</td>
                    </tr>

                </g:each>
            </table>


        </g:if>



    </content>



</g:applyLayout>
