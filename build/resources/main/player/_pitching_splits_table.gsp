<table class="table table-bordered stats pitching">

    <tr>
        <th>Year</th>
        <th>Split</th>
        <th>W</th>
        <th class="advanced">L</th>
        <th>ERA</th>
        <th class="advanced">G</th>
        <th>GS</th>
        <th class="advanced">CG</th>
        <th class="advanced">SHO</th>
        <th class="advanced">S</th>
        <th class="advanced">BS</th>
        <th>IP</th>
        <th>H</th>
        <th class="advanced">R</th>
        <th>ER</th>
        <th>HR</th>
        <th>BB</th>
        <th>SO</th>
        <th class="advanced">PC</th>
        <th class="advanced">BF</th>
        <th class="advanced">Strikes</th>


        <th class="advanced">OBP</th>
        <th class="advanced">SLG</th>
        <th class="advanced">OPS</th>

        <th>WOBA</th>
        <th>wFDA</th>

        <th>ISO</th>

        <th>K%</th>
        <th>BB%</th>

        <th>FD</th>
        <th>FDAvg</th>

    </tr>


    <g:each in="${pitchingSeasonSplits}" var="pitcherSeasonSplit">

        <tr>
            <td>${pitcherSeasonSplit.year}</td>
            <td>${pitcherSeasonSplit.split}</td>

            <td>${pitcherSeasonSplit.wins}</td>
            <td class="advanced">${pitcherSeasonSplit.losses}</td>
            <td>${pitcherSeasonSplit.era?.formatTwo()}</td>
            <td class="advanced">${pitcherSeasonSplit.games}</td>
            <td>${pitcherSeasonSplit.starts}</td>
            <td class="advanced">${pitcherSeasonSplit.cg}</td>
            <td class="advanced">${pitcherSeasonSplit.sho}</td>
            <td class="advanced">${pitcherSeasonSplit.saves}</td>
            <td class="advanced">${pitcherSeasonSplit.bs}</td>
            <td>${pitcherSeasonSplit.ip}</td>
            <td>${pitcherSeasonSplit.h}</td>
            <td class="advanced">${pitcherSeasonSplit.runs}</td>
            <td>${pitcherSeasonSplit.er}</td>
            <td>${pitcherSeasonSplit.hr}</td>
            <td>${pitcherSeasonSplit.bb}</td>
            <td>${pitcherSeasonSplit.so}</td>



            <td class="advanced">${pitcherSeasonSplit.pc}</td>


            <td class="advanced">${pitcherSeasonSplit.battersFace}</td>
            <td class="advanced">${pitcherSeasonSplit.strikes}</td>

            <td class="advanced">${pitcherSeasonSplit.opponentObp?.formatThree()}</td>
            <td class="advanced">${pitcherSeasonSplit.opponentSlg?.formatThree()}</td>
            <td class="advanced">${pitcherSeasonSplit.opponentOps?.formatThree()}</td>

            <td>${pitcherSeasonSplit.opponentWOba?.formatThree()}</td>
            <td>${pitcherSeasonSplit.opponentWFda?.formatTwo()}</td>

            <td>${pitcherSeasonSplit.opponentIso?.formatThree()}</td>

            <td>${pitcherSeasonSplit.soPercent?.formatPercent()}</td>
            <td>${pitcherSeasonSplit.bbPercent?.formatPercent()}</td>

            <td>${pitcherSeasonSplit.fanduelPoints?.formatTwo()}</td>
            <td>${pitcherSeasonSplit.fanduelAvg?.formatTwo()}</td>


        </tr>

    </g:each>

    <g:if test="${pitcherCareerSplit != null}">
        <tr class="total">
            <td>Total</td>
            <td>${pitcherCareerSplit.split}</td>

            <td>${pitcherCareerSplit.wins}</td>
            <td class="advanced">${pitcherCareerSplit.losses}</td>
            <td>${pitcherCareerSplit.era?.formatTwo()}</td>
            <td class="advanced">${pitcherCareerSplit.games}</td>
            <td>${pitcherCareerSplit.starts}</td>
            <td class="advanced">${pitcherCareerSplit.cg}</td>
            <td class="advanced">${pitcherCareerSplit.sho}</td>
            <td class="advanced">${pitcherCareerSplit.saves}</td>
            <td class="advanced">${pitcherCareerSplit.bs}</td>
            <td>${pitcherCareerSplit.ip}</td>
            <td>${pitcherCareerSplit.h}</td>
            <td class="advanced">${pitcherCareerSplit.runs}</td>
            <td>${pitcherCareerSplit.er}</td>
            <td>${pitcherCareerSplit.hr}</td>
            <td>${pitcherCareerSplit.bb}</td>
            <td>${pitcherCareerSplit.so}</td>



            <td class="advanced">${pitcherCareerSplit.pc}</td>


            <td class="advanced">${pitcherCareerSplit.battersFace}</td>
            <td class="advanced">${pitcherCareerSplit.strikes}</td>

            <td class="advanced">${pitcherCareerSplit.opponentObp?.formatThree()}</td>
            <td class="advanced">${pitcherCareerSplit.opponentSlg?.formatThree()}</td>
            <td class="advanced">${pitcherCareerSplit.opponentOps?.formatThree()}</td>

            <td>${pitcherCareerSplit.opponentWOba?.formatThree()}</td>
            <td>${pitcherCareerSplit.opponentWFda?.formatTwo()}</td>

            <td>${pitcherCareerSplit.opponentIso?.formatThree()}</td>

            <td>${pitcherCareerSplit.soPercent?.formatPercent()}</td>
            <td>${pitcherCareerSplit.bbPercent?.formatPercent()}</td>

            <td>${pitcherCareerSplit.fanduelPoints?.formatTwo()}</td>
            <td>${pitcherCareerSplit.fanduelAvg?.formatTwo()}</td>


        </tr>
    </g:if>



</table>