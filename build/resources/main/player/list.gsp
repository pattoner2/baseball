<g:applyLayout name="main">

    <head>

    </head>

    <content tag="content">

        <table class="table table-bordered">
            <g:each in="${players}" var="player">
                <tr>
                    <td>
                        <a href="show/${player.id}">${player.lastName + ", " + player.firstName}</a>
                    </td>
                    <td>
                        ${player.currentTeam?.displayName}
                    </td>
                </tr>

            </g:each>
        </table>



        <ul class="pagination" >
            <g:paginate total="${playerCount ?: 0}" />
        </ul>

    </content>



</g:applyLayout>
