<g:applyLayout name="main">

	<head>

	</head>





	<content tag="content">


		<div class="col-md-10 col-md-offset-1">

			<h1>${player.fullName} <small>${player.currentTeam.displayName}</small></h1>



			<g:if test="${pitcherSeasons.size() > 0}">
				<h2>Pitching</h2>
				<g:render template="pitching_seasons_table" model="[pitcherSeasons: pitcherSeasons, pitcherCareer: pitcherCareer]" />
			</g:if>

			<g:if test="${recentPitcherSplits.size() > 0}">
				<h2>Recent</h2>
				<g:render template="pitching_splits_table" model="[pitchingSeasonSplits: recentPitcherSplits]" />
			</g:if>

			<g:if test="${leftPitcherSplits.size() > 0}">
				<h2>Left Splits</h2>
				<g:render template="pitching_splits_table" model="[pitchingSeasonSplits: leftPitcherSplits, pitcherCareerSplit: pitcherCareerSplits?.find { 'L'.equals(it.split) }]" />
			</g:if>

			<g:if test="${rightPitcherSplits.size() > 0}">
				<h2>Right Splits</h2>
				<g:render template="pitching_splits_table" model="[pitchingSeasonSplits: rightPitcherSplits,  pitcherCareerSplit: pitcherCareerSplits?.find { 'R'.equals(it.split) }]" />
			</g:if>

			<g:if test="${homePitcherSplits.size() > 0}">
				<h2>Home Splits</h2>
				<g:render template="pitching_splits_table" model="[pitchingSeasonSplits: homePitcherSplits,  pitcherCareerSplit: pitcherCareerSplits?.find { 'H'.equals(it.split) }]" />
			</g:if>

			<g:if test="${awayPitcherSplits.size() > 0}">
				<h2>Away Splits</h2>
				<g:render template="pitching_splits_table" model="[pitchingSeasonSplits: awayPitcherSplits,  pitcherCareerSplit: pitcherCareerSplits?.find { 'A'.equals(it.split) }]" />
			</g:if>





			<g:if test="${hittingSeasons.size() > 0}">
				<h2>Hitting</h2>
				<g:render template="hitting_seasons_table" model="[hittingSeasons: hittingSeasons, hitterCareer: hitterCareer]" />
			</g:if>

			<g:if test="${recentHitterSplits.size() > 0}">
				<h2>Recent</h2>
				<g:render template="hitting_splits_table" model="[hittingSeasonSplits: recentHitterSplits]" />
			</g:if>

			<g:if test="${leftHitterSplits.size() > 0}">
				<h2>Left Splits</h2>
				<g:render template="hitting_splits_table" model="[hittingSeasonSplits: leftHitterSplits, hitterCareerSplit: hitterCareerSplits.find { 'L'.equals(it.split)}]" />
			</g:if>

			<g:if test="${rightHitterSplits.size() > 0}">
				<h2>Right Splits</h2>
				<g:render template="hitting_splits_table" model="[hittingSeasonSplits: rightHitterSplits, hitterCareerSplit: hitterCareerSplits.find { 'R'.equals(it.split)}]" />
			</g:if>

			<g:if test="${homeHitterSplits.size() > 0}">
				<h2>Home Splits</h2>
				<g:render template="hitting_splits_table" model="[hittingSeasonSplits: homeHitterSplits, hitterCareerSplit: hitterCareerSplits.find { 'H'.equals(it.split)}]" />
			</g:if>

			<g:if test="${awayHitterSplits.size() > 0}">
				<h2>Away Splits</h2>
				<g:render template="hitting_splits_table" model="[hittingSeasonSplits: awayHitterSplits, hitterCareerSplit: hitterCareerSplits.find { 'A'.equals(it.split)}]" />
			</g:if>






			<g:if test="${hittingSeasons.size() > 0}">
				<h2>Fielding</h2>
				<table class="table table-bordered stats fielding">

					<tr>
						<th>Year</th>
						<th>PO</th>
						<th>ASSISTS</th>
						<th>E</th>
					</th>
					</tr>

					<g:each in="${hittingSeasons}" var="hittingSeason">
						<tr>
							<td>
								<a href="year/${player.id}/${hittingSeason.year}">${hittingSeason.year}</a>
							</td>

							<td>${hittingSeason.po}</td>
							<td>${hittingSeason.assists}</td>
							<td>${hittingSeason.e}</td>
						</tr>

					</g:each>
				</table>
			</g:if>


		</div>





	</content>



</g:applyLayout>
