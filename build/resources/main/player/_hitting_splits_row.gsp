<tr class="${hitterSeasonSplit.isCurrentSplit(todayHitterGameLog) ? 'active-split' : ''}">
    <td>${hitterSeasonSplit.split}</td>
    <td>${hitterSeasonSplit.pa}</td>
    <td>${hitterSeasonSplit.avg?.formatThree()}</td>
    <td>${hitterSeasonSplit.runs}</td>
    <td>${hitterSeasonSplit.homeRuns}</td>
    <td>${hitterSeasonSplit.rbi}</td>
    <td>${hitterSeasonSplit.sb}</td>
    <td>${hitterSeasonSplit.wFda?.formatThree()}</td>
    <td>${hitterSeasonSplit.fanduelAvg?.formatTwo()}</td>
</tr>

<g:if test="${hitterSeasonSplit.isCurrentSplit(todayHitterGameLog)}">

    <tr class="active-split">
        <td>Park/${hitterSeasonSplit.split}</td>
        <td>${hitterSeasonSplit.pa}</td>
        <td>${hitterSeasonSplit.paAvg?.formatThree()}</td>
        <td>${hitterSeasonSplit.paRuns?.toInteger()}</td>
        <td>${hitterSeasonSplit.paHomeRuns?.toInteger()}</td>
        <td>${hitterSeasonSplit.paRbi?.toInteger()}</td>
        <td>${hitterSeasonSplit.paSb?.toInteger()}</td>
        <td>${hitterSeasonSplit.paWFda?.formatThree()}</td>
        <td>${hitterSeasonSplit.fanduelAvg?.formatTwo()}</td>
    </tr>

</g:if>