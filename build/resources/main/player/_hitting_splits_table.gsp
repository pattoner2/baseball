<table class="table table-bordered stats">

    <tr>
        <th>Year</th>
        <th>Type</th>
        <th>G</th>
        <th class="advanced">PA</th>
        <th>AB</th>
        <th>R</th>
        <th>H</th>
        <th>2B</th>
        <th>3B</th>
        <th>HR</th>
        <th>RBI</th>
        <th>SB</th>
        <th>CS</th>
        <th>BB</th>
        <th>SO</th>
        <th>BA</th>
        <th>OBP</th>
        <th>SLG</th>
        <th>OPS</th>
        <th class="advanced">TB</th>
        <th class="advanced">GDP</th>
        <th class="advanced">HPB</th>
        <th class="advanced">SH</th>
        <th class="advanced">SF</th>
        <th class="advanced">IBB</th>

        <th class="advanced">LOB</th>
        <th class="advanced">GO</th>
        <th class="advanced">FO</th>
        <th>K%</th>
        <th>BB%</th>
        <th>ISO</th>

        <th>wOBA</th>

        <th>FD</th>
        <th>FDAvg</th>
    </th>
    </tr>

    <g:each in="${hittingSeasonSplits}" var="hittingSeasonSplit">
        <tr>
            <td>
                ${hittingSeasonSplit.year}
            </td>
            <td>${hittingSeasonSplit.split}</td>
            <td>${hittingSeasonSplit.games}</td>
            <td class="advanced">${hittingSeasonSplit.pa}</td>
            <td>${hittingSeasonSplit.atBats}</td>
            <td>${hittingSeasonSplit.runs}</td>
            <td>${hittingSeasonSplit.hits}</td>
            <td>${hittingSeasonSplit.doubles}</td>
            <td>${hittingSeasonSplit.triples}</td>
            <td>${hittingSeasonSplit.homeRuns}</td>
            <td>${hittingSeasonSplit.rbi}</td>
            <td>${hittingSeasonSplit.sb}</td>
            <td>${hittingSeasonSplit.cs}</td>
            <td>${hittingSeasonSplit.bb}</td>
            <td>${hittingSeasonSplit.so}</td>
            <td>${hittingSeasonSplit.avg?.formatThree()}</td>
            <td>${hittingSeasonSplit.obp?.formatThree()}</td>
            <td>${hittingSeasonSplit.slg?.formatThree()}</td>
            <td>${hittingSeasonSplit.ops?.formatThree()}</td>
            <td class="advanced">${hittingSeasonSplit.tb}</td>
            <td class="advanced">${hittingSeasonSplit.gidp}</td>
            <td class="advanced">${hittingSeasonSplit.hbp}</td>
            <td class="advanced">${hittingSeasonSplit.sacBunts}</td>
            <td class="advanced">${hittingSeasonSplit.sacFlys}</td>
            <td class="advanced">${hittingSeasonSplit.ibb}</td>

            <td class="advanced">${hittingSeasonSplit.lob}</td>

            <td class="advanced">${hittingSeasonSplit.groundOuts}</td>
            <td class="advanced">${hittingSeasonSplit.flyOuts}</td>
            <td>${hittingSeasonSplit.soPercent.formatPercent()}</td>
            <td>${hittingSeasonSplit.bbPercent.formatPercent()}</td>
            <td>${hittingSeasonSplit.iso?.formatThree()}</td>
            <td>${hittingSeasonSplit.wOba?.formatThree()}</td>

            <td>${hittingSeasonSplit.fanduelPoints?.formatTwo()}</td>
            <td>${hittingSeasonSplit.fanduelAvg?.formatTwo()}</td>

        </tr>

    </g:each>

    <g:if test="${hitterCareerSplit}">
        <tr class="total">
            <td>
                Total
            </td>
            <td>${hitterCareerSplit.split}</td>
            <td>${hitterCareerSplit.games}</td>
            <td class="advanced">${hitterCareerSplit.pa}</td>
            <td>${hitterCareerSplit.atBats}</td>
            <td>${hitterCareerSplit.runs}</td>
            <td>${hitterCareerSplit.hits}</td>
            <td>${hitterCareerSplit.doubles}</td>
            <td>${hitterCareerSplit.triples}</td>
            <td>${hitterCareerSplit.homeRuns}</td>
            <td>${hitterCareerSplit.rbi}</td>
            <td>${hitterCareerSplit.sb}</td>
            <td>${hitterCareerSplit.cs}</td>
            <td>${hitterCareerSplit.bb}</td>
            <td>${hitterCareerSplit.so}</td>
            <td>${hitterCareerSplit.avg?.formatThree()}</td>
            <td>${hitterCareerSplit.obp?.formatThree()}</td>
            <td>${hitterCareerSplit.slg?.formatThree()}</td>
            <td>${hitterCareerSplit.ops?.formatThree()}</td>
            <td class="advanced">${hitterCareerSplit.tb}</td>
            <td class="advanced">${hitterCareerSplit.gidp}</td>
            <td class="advanced">${hitterCareerSplit.hbp}</td>
            <td class="advanced">${hitterCareerSplit.sacBunts}</td>
            <td class="advanced">${hitterCareerSplit.sacFlys}</td>
            <td class="advanced">${hitterCareerSplit.ibb}</td>

            <td class="advanced">${hitterCareerSplit.lob}</td>

            <td class="advanced">${hitterCareerSplit.groundOuts}</td>
            <td class="advanced">${hitterCareerSplit.flyOuts}</td>
            <td>${hitterCareerSplit.soPercent.formatPercent()}</td>
            <td>${hitterCareerSplit.bbPercent.formatPercent()}</td>
            <td>${hitterCareerSplit.iso?.formatThree()}</td>
            <td>${hitterCareerSplit.wOba?.formatThree()}</td>

            <td>${hitterCareerSplit.fanduelPoints?.formatTwo()}</td>
            <td>${hitterCareerSplit.fanduelAvg?.formatTwo()}</td>

        </tr>
    </g:if>
</table>