var Nightmare = require('nightmare');
require('nightmare-download-manager')(Nightmare);


var nightmare = Nightmare({ show: false })




nightmare
    .goto('https://rotogrinders.com/pages/mlb-vegas-lines-player-prop-bets-265313')
    .evaluate(function(){

        var games = [];

        var rows = $('#DataTables_Table_0').find('tr:gt(0)');

        $(rows).each(function() {

            var cells = $(this).find("td");

            if (cells.length > 0) {

                var bet = $.trim(cells.eq(0).text());

                if (bet == "Total Strikeouts") {
                    var game = {
                        pitcher: $.trim(cells.eq(1).text()),
                        team: $.trim(cells.eq(2).text()),
                        opp: $.trim(cells.eq(3).text()),
                        strikeouts: $.trim(cells.eq(4).text())
                    }

                    games.push(game);
                }


            }
        });

        return games;
    })
    .end()
    .then(function (result) {
        console.log(result);
    });

