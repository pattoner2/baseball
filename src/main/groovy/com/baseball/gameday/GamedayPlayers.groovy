package com.baseball.gameday

/**
 * Created by patricktoner on 5/21/16.
 */
class GamedayPlayers {

    List<GamedayPlayer> playerList = new ArrayList<>()


    public GamedayPlayers(def element) {

        element.team?.each { team ->
            team.player?.each { player ->
                GamedayPlayer thePlayer = new GamedayPlayer(player)

                thePlayer.gamedayTeamId = team.@'id'

                playerList.add(thePlayer)
            }
        }
    }

}
