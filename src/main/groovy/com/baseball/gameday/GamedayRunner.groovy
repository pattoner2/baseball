package com.baseball.gameday

import groovy.util.slurpersupport.NodeChild

/**
 * Created by patricktoner on 9/3/16.
 */
class GamedayRunner {

    Long id
    String start
    String end
    String event
    String score
    String earned
    Integer eventNum



    public GamedayRunner(NodeChild gameElement) {

        id = NumberUtil.parseDouble(gameElement.@'id'.toString())

        start = gameElement.@'start'.toString()
        end = gameElement.@'end'.toString()
        event = gameElement.@'event'.toString()

        score = gameElement.@'score'.toString()
        earned = gameElement.@'earned'.toString()

        eventNum= NumberUtil.parseInt(gameElement.@'event_num'.toString())
    }
}
