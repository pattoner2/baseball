package com.baseball.gameday
/**
 * Created by patricktoner on 5/21/16.
 */
class LinescorePitcher {

    Long playerId
    String firstName
    String lastName
    String displayName
    Integer wins
    Integer losses
    Double era
    Integer seasonWins
    Integer seasonLosses
    Double seasonEra
    String throwingHand

    public LinescorePitcher(def element) {

        playerId = NumberUtil.parseLong(element.@'id'.toString())
        firstName = element.@'first_name'.toString()
        lastName = element.@'last_name'.toString()
        displayName = element.@'name_display_roster'.toString()
        wins = NumberUtil.parseInt(element.@'wins'.toString())
        losses = NumberUtil.parseInt(element.@'losses'.toString())
        era = NumberUtil.parseDouble(element.@'era'.toString())
        seasonWins = NumberUtil.parseInt(element.@'s_wins'.toString())
        seasonLosses = NumberUtil.parseInt(element.@'s_losses'.toString())
        seasonEra = NumberUtil.parseDouble(element.@'s_era'.toString())
        throwingHand = element.@'throwinghand'.toString()

    }




}
