package com.baseball.gameday
/**
 * Created by patricktoner on 5/21/16.
 */
class GamedayGameEvents {

    List<GamedayAtbat> atBats = new ArrayList<>()


    public GamedayGameEvents(def element) {


        element.inning?.each { inning ->

            Integer inningNum = NumberUtil.parseInt(inning.@'num'.toString())

            def top = inning.top
            def bottom = inning.bottom

            top.children().findAll{ child -> child.name() == "atbat"}.each {
                GamedayAtbat atBat = new GamedayAtbat(it)
                atBat.inningNum = inningNum
                atBat.inningTop = true
                atBats.add(atBat)
            }

            bottom.children().findAll{ child -> child.name() == "atbat"}.each {
                GamedayAtbat atBat = new GamedayAtbat(it)
                atBat.inningNum = inningNum
                atBat.inningTop = false
                atBats.add(atBat)

            }

        }
    }

}
