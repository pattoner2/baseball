package com.baseball.gameday

import com.mdimension.jchronic.Chronic

/**
 * Created by patricktoner on 5/20/16.
 */
class BoxScore {


    String gameId
    String gamePk
    Date date
    String status
    String venueName

    Long awayId
    String awayTeamCode
    String awayFullName

    Long homeId
    String homeTeamCode
    String homeFullName

    int awayTeamRuns
    int homeTeamRuns
    int awayTeamHits
    int homeTeamHits
    int awayTeamErrors
    int homeTeamErrors



    List<Pitching> pitching = new ArrayList<>()
    List<Batting> batting = new ArrayList<>()


    public BoxScore(def element) {

        gameId = element.@'game_id'.toString()
        gamePk  = element.@'game_pk'.toString()
        date  = Chronic.parse(element.@'date'.toString())?.beginCalendar?.time
        status = element.@'status_ind'.toString()
        venueName = element.@'venue_name'.toString()

        awayId = NumberUtil.parseLong(element.@'away_id'.toString())
        awayTeamCode = element.@'away_team_code'.toString()
        awayFullName = element.@'away_fname'.toString()

        homeId = NumberUtil.parseLong(element.@'home_id'.toString())
        homeTeamCode = element.@'home_team_code'.toString()
        homeFullName  = element.@'home_fname'.toString()


        element.pitching?.each {
            pitching.add(new Pitching(it))
        }

        element.batting?.each {
            batting.add(new Batting(it))
        }

        def linescore = element.linescore

        awayTeamRuns = NumberUtil.parseInt(linescore.@'away_team_runs'.toString())
        homeTeamRuns = NumberUtil.parseInt(linescore.@'home_team_runs'.toString())
        awayTeamHits = NumberUtil.parseInt(linescore.@'away_team_hits'.toString())
        homeTeamHits = NumberUtil.parseInt(linescore.@'home_team_hits'.toString())
        awayTeamErrors = NumberUtil.parseInt(linescore.@'away_team_errors'.toString())
        homeTeamErrors = NumberUtil.parseInt(linescore.@'home_team_errors'.toString())

    }



}
