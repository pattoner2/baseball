package com.baseball.gameday

/**
 * Created by patricktoner on 5/20/16.
 */
class NumberUtil {

    static public Double parseDouble(String text) {
        if (text) {
            try {
                return Double.valueOf(text)
            } catch(Exception ex){}
        }

        return null
    }

    static public Integer parseInt(String text) {
        if (text) {
            try {
                return Integer.valueOf(text)
            } catch(Exception ex){}
        }

        return null

    }

    static public Long parseLong(String text) {
        if (text) {
            try {
                return Long.valueOf(text)
            } catch(Exception ex){}
        }

        return null

    }


}
