package com.baseball.gameday

import com.mdimension.jchronic.Chronic
import groovy.util.slurpersupport.NodeChild

/**
 * Created by patricktoner on 9/3/16.
 */
class GamedayPitch {

    String sportsvisionId
    String description
    Double startSpeed
    Double endSpeed
    String type
    String pitchType
    Integer sequence




    Long id                 //="91"
    Long tfs                //="233334"
    Date tfsZulu            //="2016-05-09T23:33:34Z"
    Double y                //="218.13"
    Double eventNum         //="91"
    Long on1b              //="408299"
    Long on2b               //="521692"
    Long on3b
    String playGuid         //="13919870-1e7c-4d09-bd4a-e9f1fc307729"
    Double szTop            //="3.5"
    Double szBot            //="1.59"
    Double pfxX             //="3.23"
    Double pfxZ             //="-2.82"
    Double px               //="1.086"
    Double pz               //="0.765"
    Double x0               //="-2.027"
    Double y0               //="50.0"
    Double z0               //="6.592"
    Double vx0              //="6.293"
    Double vy0              //="-118.359"
    Double vz0              //="-5.883"
    Double ax               //="4.57"
    Double ay               //="22.808"
    Double az               //="-36.101"
    Double breakY          //="23.9"
    Double breakAngle       //="-8.4"
    Double breakLength      //="10.7"
    Double typeConfidence   //="2.000"
    Double zone             //="14"
    Double nasty            //="84"
    Double spinDir          //="49.327"
    Double spinRate         //="738.570"






    public GamedayPitch(NodeChild gameElement) {


        sportsvisionId = gameElement.@'sv_id'.toString()

        description = gameElement.@'des'.toString()

        startSpeed= NumberUtil.parseDouble(gameElement.@'start_speed'.toString())
        endSpeed= NumberUtil.parseDouble(gameElement.@'end_speed'.toString())

        type = gameElement.@'type'.toString()
        pitchType = gameElement.@'pitch_type'.toString()


        id   = NumberUtil.parseLong(gameElement.@'id'.toString())
        tfs =NumberUtil.parseLong(gameElement.@'tfs'.toString())                //="233334"
        tfsZulu = Chronic.parse(gameElement.@'tfs_zulu'.toString())?.beginCalendar?.time
        y       = NumberUtil.parseDouble(gameElement.@'y'.toString())         //="218.13"
        eventNum   = NumberUtil.parseDouble(gameElement.@'event_num'.toString())      //="91"
        on1b  = NumberUtil.parseLong(gameElement.@'on1b'.toString())            //="408299"
        on2b  = NumberUtil.parseLong(gameElement.@'on2b'.toString())             //="521692"
        on3b = NumberUtil.parseLong(gameElement.@'on3b'.toString())             //="521692"
        playGuid   = gameElement.@'play_guid'.toString()      //="13919870-1e7c-4d09-bd4a-e9f1fc307729"
        szTop  = NumberUtil.parseDouble(gameElement.@'sz_top'.toString())         //="218.13"           //="3.5"
        szBot =  NumberUtil.parseDouble(gameElement.@'sz_bot'.toString())         //="218.13"           //="1.59"
        pfxX  = NumberUtil.parseDouble(gameElement.@'pfx_x'.toString())         //="218.13"           //="3.23"
        pfxZ   = NumberUtil.parseDouble(gameElement.@'pfx_z'.toString())         //="218.13"          //="-2.82"
        px   = NumberUtil.parseDouble(gameElement.@'px'.toString())         //="218.13"            //="1.086"
        pz    = NumberUtil.parseDouble(gameElement.@'pz'.toString())         //="218.13"           //="0.765"
        x0   = NumberUtil.parseDouble(gameElement.@'x0'.toString())         //="218.13"            //="-2.027"
        y0    = NumberUtil.parseDouble(gameElement.@'y0'.toString())         //="218.13"           //="50.0"
        z0   = NumberUtil.parseDouble(gameElement.@'z0'.toString())         //="218.13"            //="6.592"
        vx0    = NumberUtil.parseDouble(gameElement.@'vx0'.toString())         //="218.13"          //="6.293"
        vy0  = NumberUtil.parseDouble(gameElement.@'vy0'.toString())         //="218.13"            //="-118.359"
        vz0    = NumberUtil.parseDouble(gameElement.@'vz0'.toString())         //="218.13"          //="-5.883"
        ax     = NumberUtil.parseDouble(gameElement.@'ax'.toString())         //="218.13"          //="4.57"
        ay     = NumberUtil.parseDouble(gameElement.@'ay'.toString())         //="218.13"          //="22.808"
        az      = NumberUtil.parseDouble(gameElement.@'az'.toString())         //="218.13"         //="-36.101"
        breakY   = NumberUtil.parseDouble(gameElement.@'break_y'.toString())         //="218.13"       //="23.9"
        breakAngle   = NumberUtil.parseDouble(gameElement.@'break_angle'.toString())         //="218.13"    //="-8.4"
        breakLength  = NumberUtil.parseDouble(gameElement.@'break_length'.toString())         //="218.13"    //="10.7"
        typeConfidence  = NumberUtil.parseDouble(gameElement.@'type_confidence'.toString())         //="218.13" //="2.000"
        zone       = NumberUtil.parseDouble(gameElement.@'zone'.toString())         //="218.13"      //="14"
        nasty       = NumberUtil.parseDouble(gameElement.@'nasty'.toString())         //="218.13"     //="84"
        spinDir    = NumberUtil.parseDouble(gameElement.@'spin_dir'.toString())         //="218.13"      //="49.327"
        spinRate   = NumberUtil.parseDouble(gameElement.@'spin_rate'.toString())         //="218.13"       //="738.570"



    }
}
