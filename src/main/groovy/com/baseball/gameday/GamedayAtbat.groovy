package com.baseball.gameday

import com.mdimension.jchronic.Chronic

/**
 * Created by patricktoner on 5/25/16.
 */
class GamedayAtbat {

    Integer eventNum
    Integer inningNum
    boolean inningTop

    Long batterId
    Long pitcherId

    Date startTimeUtc

    Integer num
    Integer balls
    Integer strikes
    Integer outs

    String description
    String event

    boolean score
    Integer awayTeamRuns
    Integer homeTeamRuns

    Integer baseRunner1Id
    Integer baseRunner2Id
    Integer baseRunner3Id

    int rbi = 0
    int earnedRun = 0

    List<GamedayRunner> runners = new ArrayList<>()
    List<GamedayPitch> pitches = new ArrayList<>()

    public GamedayAtbat(def element) {

        batterId = NumberUtil.parseLong(element.@'batter'.toString())
        pitcherId = NumberUtil.parseLong(element.@'pitcher'.toString())

        startTimeUtc = Chronic.parse(element.@'start_tfs'.toString())?.beginCalendar?.time

        num = NumberUtil.parseInt(element.@'num'.toString())
        balls = NumberUtil.parseInt(element.@'b'.toString())
        strikes = NumberUtil.parseInt(element.@'s'.toString())
        outs  = NumberUtil.parseInt(element.@'o'.toString())

        description = element.@'des'.toString()
        event = element.@'event'.toString()

        score = element.@'score'.toString()?.equals("T")
        awayTeamRuns = NumberUtil.parseInt(element.@'away_team_runs'.toString())
        homeTeamRuns = NumberUtil.parseInt(element.@'home_team_runs'.toString())

        baseRunner1Id = NumberUtil.parseInt(element.@'b1'.toString())
        baseRunner2Id = NumberUtil.parseInt(element.@'b2'.toString())
        baseRunner3Id = NumberUtil.parseInt(element.@'b3'.toString())

        eventNum = NumberUtil.parseInt(element.@'event_num'.toString())

        int i = 0
        element.pitch?.each { pitch ->
            GamedayPitch gamedayPitch = new GamedayPitch(pitch)
            gamedayPitch.sequence = i++
            pitches.add(gamedayPitch)
        }


        element.runner?.each { runner ->
            GamedayRunner gamedayRunner = new GamedayRunner(runner)
            runners.add(gamedayRunner)

            if ("T".equals(gamedayRunner.score)) {
                rbi++

                if ("T".equals(gamedayRunner.earned)) earnedRun++

            }

        }


    }


}
