package com.baseball.gameday

import com.mdimension.jchronic.Chronic
import groovy.util.slurpersupport.NodeChild

/**
 * Created by patricktoner on 5/20/16.
 */
class GamedayGame {

    String id
    Long primaryKey

    Date timeDate
    String ampm

    Long venueId
    String venueWeatherChannelCode
    String type


    String status

    Long awayTeamId
    String awayCode
    String awayTeamCity
    String awayTeamName
    String awayNameAbbrev

    Long homeTeamId
    String homeCode
    String homeTeamCity
    String homeTeamName
    String homeNameAbbrev

    LinescorePitcher homeProbablePitcher
    LinescorePitcher awayProbablePitcher
    LinescorePitcher winningPitcher
    LinescorePitcher losingPitcher
    LinescorePitcher savePitcher


    public GamedayGame(NodeChild gameElement) {


        id = gameElement.@'id'.toString()

        primaryKey = NumberUtil.parseLong(gameElement.@'game_pk'.toString())


        ampm = gameElement.@'ampm'.toString()



        String timeDateString = gameElement.@'time_date'.toString()



        if (timeDateString) {

            timeDateString += " " + ampm

        } else {

            timeDateString = id.substring(0, id.lastIndexOf("/"))

            String time = gameElement.@'time'
            if ("TBD".equals(time)) {
                timeDateString += " 12:00 PM"
            } else {
                timeDateString += " " + gameElement.@'time' + " " + ampm
            }


        }

        timeDate = Chronic.parse(timeDateString + " ET")?.beginCalendar?.time

        venueId = NumberUtil.parseLong(gameElement.@'venue_id'.toString())
        venueWeatherChannelCode = gameElement.@'venue_w_chan_loc'.toString()
        type = gameElement.@'game_type'.toString()

        status = gameElement.@'status'.toString()

        awayTeamId = NumberUtil.parseLong(gameElement.@'away_team_id'.toString())
        awayCode = gameElement.@'away_code'.toString()
        awayTeamCity = gameElement.@'away_team_city'.toString()
        awayTeamName = gameElement.@'away_team_name'.toString()
        awayNameAbbrev = gameElement.@'away_name_abbrev'.toString()



        homeTeamId = NumberUtil.parseLong(gameElement.@'home_team_id'.toString())
        homeCode = gameElement.@'home_code'.toString()
        homeTeamCity = gameElement.@'home_team_city'.toString()
        homeTeamName = gameElement.@'home_team_name'.toString()
        homeNameAbbrev = gameElement.@'home_name_abbrev'.toString()

        if (gameElement.home_probable_pitcher?.size() > 0) {
            homeProbablePitcher = new LinescorePitcher(gameElement.home_probable_pitcher)
        }

        if (gameElement.away_probable_pitcher?.size() > 0) {
            awayProbablePitcher = new LinescorePitcher(gameElement.away_probable_pitcher)
        }

        if (gameElement.winning_pitcher?.size() > 0) {
            winningPitcher = new LinescorePitcher(gameElement.winning_pitcher)
        }

        if (gameElement.losing_pitcher?.size() > 0) {
            losingPitcher = new LinescorePitcher(gameElement.losing_pitcher)
        }

        if (gameElement.save_pitcher?.size() > 0) {
            savePitcher = new LinescorePitcher(gameElement.save_pitcher)
        }


    }


}
