package com.baseball.gameday

/**
 * Created by patricktoner on 5/20/16.
 */
class Pitching  {

    String teamFlag
    Integer battersFaced
    Integer hits
    Integer runs
    Integer hr
    Integer so
    Integer bb
    Integer out
    Integer er
    Double era

    List<PitchingAppearance> appearances = new ArrayList<>()



    public Pitching(def element) {
        teamFlag = element.@'team_flag'.toString()
        battersFaced = NumberUtil.parseInt(element.@'bf'.toString())
        hits = NumberUtil.parseInt(element.@'h'.toString())
        runs = NumberUtil.parseInt(element.@'r'.toString())
        hr = NumberUtil.parseInt(element.@'hr'.toString())
        so = NumberUtil.parseInt(element.@'so'.toString())
        bb = NumberUtil.parseInt(element.@'bb'.toString())
        out = NumberUtil.parseInt(element.@'out'.toString())
        er = NumberUtil.parseInt(element.@'er'.toString())
        era = NumberUtil.parseDouble(element.@'era'.toString())

        //If there's an id we have a single pitcher instead of an array
        if (element.pitcher?.size() > 0) {
            element.pitcher?.each {
                appearances.add(new PitchingAppearance(it))
            }
        } else  {
            appearances.add(new PitchingAppearance(element.pitcher))
        }






    }


}
