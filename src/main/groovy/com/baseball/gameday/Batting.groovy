package com.baseball.gameday

/**
 * Created by patricktoner on 5/20/16.
 */
class Batting {

    String teamFlag
    Integer atBats
    Double avg
    Integer hits
    Integer bb
    Integer so
    Integer r
    Integer rbi
    Integer lob
    Integer doubles
    Integer triples
    Integer hr
    Double defensiveAvg
    Integer putOuts

    List<BattingAppearance> appearances = new ArrayList<>()

    public Batting(def element) {
         teamFlag = element.@'team_flag'.toString()
         atBats = NumberUtil.parseInt(element.@'ab'.toString())
         avg = NumberUtil.parseDouble(element.@'avg'.toString())
         hits = NumberUtil.parseInt(element.@'h'.toString())
         bb = NumberUtil.parseInt(element.@'bb'.toString())
         so = NumberUtil.parseInt(element.@'so'.toString())
         r = NumberUtil.parseInt(element.@'r'.toString())
         rbi = NumberUtil.parseInt(element.@'rbi'.toString())
         lob = NumberUtil.parseInt(element.@'lob'.toString())
         doubles = NumberUtil.parseInt(element.@'d'.toString())
         triples = NumberUtil.parseInt(element.@'t'.toString())
         hr = NumberUtil.parseInt(element.@'hr'.toString())
         defensiveAvg = NumberUtil.parseDouble(element.@'da'.toString())
         putOuts = NumberUtil.parseInt(element.@'po'.toString())

        element.batter?.each {
            appearances.add(new BattingAppearance(it))
        }



    }

}
