package com.baseball.gameday

/**
 * Created by patricktoner on 5/20/16.
 */
class PitchingAppearance {


    Long playerId
    String playerName
    String displayName
    String position

    Integer battersFace
    Integer numberOfPitches
    Integer strikes
    Integer hits
    Integer runs
    Integer hr
    Integer so
    Integer bb
    Integer outs
    Integer earnedRuns

    boolean won
    boolean lost
    boolean saved
    boolean blewSave

    Double seasonEra
    Integer seasonWins
    Integer seasonLosses
    Integer seasonHolds
    Integer seasonSaves
    Integer seasonBlownSaves
    Double seasonInningsPitched
    Integer seasonHits
    Integer seasonRuns
    Integer seasonWalks
    Integer seasonStrikeouts
    Integer seasonEarnedRuns


    public PitchingAppearance(def element) {

         playerId = NumberUtil.parseLong(element.@'id'.toString())
         playerName = element.@'name'.toString()
         displayName = element.@'name_display_first_last'.toString()
         position =  element.@'pos'.toString()
         battersFace = NumberUtil.parseInt(element.@'bf'.toString())
         numberOfPitches = NumberUtil.parseInt(element.@'np'.toString())
         strikes = NumberUtil.parseInt(element.@'s'.toString())
         hits = NumberUtil.parseInt(element.@'h'.toString())
         runs = NumberUtil.parseInt(element.@'r'.toString())
         hr = NumberUtil.parseInt(element.@'hr'.toString())
         so = NumberUtil.parseInt(element.@'so'.toString())
         bb = NumberUtil.parseInt(element.@'bb'.toString())
         outs = NumberUtil.parseInt(element.@'out'.toString())
         earnedRuns = NumberUtil.parseInt(element.@'er'.toString())
         won = element.@'win'.toString()
         lost = element.@'loss'.toString()
         saved = element.@'save'.toString()
         blewSave = element.@'blown_save'.toString()

         seasonEra = NumberUtil.parseDouble(element.@'era'.toString())
         seasonWins = NumberUtil.parseInt(element.@'w'.toString())
         seasonLosses = NumberUtil.parseInt(element.@'l'.toString())
         seasonHolds = NumberUtil.parseInt(element.@'hld'.toString())
         seasonSaves = NumberUtil.parseInt(element.@'sv'.toString())
         seasonBlownSaves = NumberUtil.parseInt(element.@'bs'.toString())
         seasonInningsPitched = NumberUtil.parseDouble(element.@'s_ip'.toString())
         seasonHits = NumberUtil.parseInt(element.@'s_h'.toString())
         seasonRuns = NumberUtil.parseInt(element.@'s_r'.toString())
         seasonWalks = NumberUtil.parseInt(element.@'s_bb'.toString())
         seasonStrikeouts = NumberUtil.parseInt(element.@'s_so'.toString())
         seasonEarnedRuns = NumberUtil.parseInt(element.@'s_er'.toString())

    }


}
