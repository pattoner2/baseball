package com.baseball.gameday

import com.baseball.interfaces.ExternalPlayer

/**
 * Created by patricktoner on 5/21/16.
 */
class GamedayPlayer implements ExternalPlayer {

    Long playerId
    String firstName
    String lastName
    Integer number
    String pitches //handedness
    String bats
    String position
    String currentPosition
    String teamId
    String gamedayTeamId
    Integer battingOrder
    String gamePosition
    Double avg
    Integer hr
    Integer rbi


    public GamedayPlayer(def element) {

        playerId = NumberUtil.parseLong(element.@'id'.toString())
        firstName = element.@'first'.toString()
        lastName = element.@'last'.toString()


        number = NumberUtil.parseInt(element.@'num'.toString())
        pitches = element.@'rl'.toString()
        bats = element.@'bats'.toString()
        position = element.@'position'.toString()
        currentPosition = element.@'current_position'.toString()
        teamId = NumberUtil.parseLong(element.@'team_id'.toString())
        hr = NumberUtil.parseInt(element.@'hr'.toString())
        battingOrder =NumberUtil.parseInt(element.@'bat_order'.toString())
        gamePosition =  element.@'game_position'.toString()
        avg = NumberUtil.parseDouble(element.@'avg'.toString())
        rbi =  NumberUtil.parseInt(element.@'rbi'.toString())

    }


}
