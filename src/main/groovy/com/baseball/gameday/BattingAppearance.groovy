package com.baseball.gameday

/**
 * Created by patricktoner on 5/20/16.
 */
class BattingAppearance {

    Long playerId
    String playerName
    String displayName
    String position

    Integer battingOrder


    Double avg
    Double fieldingPercentage

    Integer atBats
    Integer hits
    Integer bb
    Integer hbp
    Integer so
    Integer runs
    Integer rbi
    Integer lob
    Integer doubles
    Integer triples
    Integer hr
    Integer sacBunts
    Integer sacFlys
    Integer groundOuts
    Integer flyOuts
    Integer gidp
    Integer sb
    Integer cs
    Integer po
    Integer assists
    Integer errors


    Integer seasonHits
    Integer seasonWalks
    Integer seasonStrikeouts
    Integer seasonRuns
    Integer seasonRbi
    Integer seasonHr


    public BattingAppearance(def element) {

        playerId = NumberUtil.parseLong(element.@'id'.toString())
        playerName = element.@'name'.toString()
        displayName = element.@'name_display_first_last'.toString()
        position = element.@'pos'.toString()
        battingOrder = NumberUtil.parseInt(element.@'bo'.toString())
        atBats = NumberUtil.parseInt(element.@'ab'.toString())
        avg = NumberUtil.parseDouble(element.@'avg'.toString())
        hits = NumberUtil.parseInt(element.@'h'.toString())
        bb = NumberUtil.parseInt(element.@'bb'.toString())
        hbp = NumberUtil.parseInt(element.@'hbp'.toString())
        so = NumberUtil.parseInt(element.@'so'.toString())
        runs = NumberUtil.parseInt(element.@'r'.toString())
        rbi = NumberUtil.parseInt(element.@'rbi'.toString())
        lob = NumberUtil.parseInt(element.@'lob'.toString())
        doubles = NumberUtil.parseInt(element.@'d'.toString())
        triples = NumberUtil.parseInt(element.@'t'.toString())
        hr = NumberUtil.parseInt(element.@'hr'.toString())
        sacBunts = NumberUtil.parseInt(element.@'sac'.toString())
        sacFlys = NumberUtil.parseInt(element.@'sf'.toString())
        groundOuts = NumberUtil.parseInt(element.@'go'.toString())
        flyOuts = NumberUtil.parseInt(element.@'ao'.toString())
        gidp = NumberUtil.parseInt(element.@'gidp'.toString())
        sb = NumberUtil.parseInt(element.@'sb'.toString())
        cs = NumberUtil.parseInt(element.@'cs'.toString())
        po = NumberUtil.parseInt(element.@'po'.toString())
        assists = NumberUtil.parseInt(element.@'a'.toString())
        errors = NumberUtil.parseInt(element.@'e'.toString())
        fieldingPercentage = NumberUtil.parseDouble(element.@'fldg'.toString())


        seasonHits = NumberUtil.parseInt(element.@'s_h'.toString())
        seasonWalks = NumberUtil.parseInt(element.@'s_bb'.toString())
        seasonStrikeouts = NumberUtil.parseInt(element.@'s_so'.toString())
        seasonRuns = NumberUtil.parseInt(element.@'s_r'.toString())
        seasonRbi = NumberUtil.parseInt(element.@'s_rbi'.toString())
        seasonHr = NumberUtil.parseInt(element.@'s_hr'.toString())



    }


}


