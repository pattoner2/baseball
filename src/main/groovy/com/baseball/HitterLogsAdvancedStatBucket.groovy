package com.baseball
/**
 * Created by patricktoner on 8/31/16.
 */
class HitterLogsAdvancedStatBucket {

    com.baseball.Player player
    com.baseball.HitterGameLog hitterGameLog


    com.baseball.HitterSeason hitterSeason
    List<com.baseball.HitterSeasonSplit> hitterSplits

    com.baseball.HitterCareer hitterCareer
    List<com.baseball.HitterCareerSplit> hitterCareerSplits

    com.baseball.PitcherSeason pitcherSeason
    List<com.baseball.PitcherSeasonSplit> pitcherSplits

    com.baseball.PitcherCareer pitcherCareer
    List<com.baseball.PitcherCareerSplit> pitcherCareerSplits

    List<com.baseball.AtBat> atBatsVsPitcher
}
