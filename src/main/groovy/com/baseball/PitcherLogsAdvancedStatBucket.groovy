package com.baseball

/**
 * Created by patricktoner on 8/31/16.
 */
class PitcherLogsAdvancedStatBucket {

    com.baseball.Player player
    com.baseball.PitcherGameLog pitcherGameLog

    com.baseball.PitcherSeason pitcherSeason
    List<com.baseball.PitcherSeasonSplit> pitcherSplits

    List<com.baseball.HitterGameLog> startingLineupGameLogs
    List<com.baseball.AtBat> startingLineupAtbats
    List<com.baseball.Player> opposingStartingLineup

    List<com.baseball.HitterGameLog> hitterGameLogs


    List<com.baseball.PitcherGameLog> last5Starts
}
