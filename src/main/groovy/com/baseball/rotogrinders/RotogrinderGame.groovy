package com.baseball.rotogrinders

import com.baseball.gameday.NumberUtil

/**
 * Created by patricktoner on 6/12/16.
 */
class RotogrinderGame {

    String opponent
    Double projectedRuns
    String pitcher
    Double overUnder
    String team
    Integer moneyLine
    Double strikeOuts

    public RotogrinderGame(def element) {


        opponent = element.opponent
        projectedRuns = NumberUtil.parseDouble(element.projectedRuns)
        pitcher = element.pitcher
        overUnder = NumberUtil.parseDouble(element.overUnder)
        team = element.team
        moneyLine = NumberUtil.parseInt(element.moneyLine)
        strikeOuts = NumberUtil.parseDouble(element.strikeouts)

    }

}
