package com.baseball.rotogrinders

import com.baseball.gameday.NumberUtil

import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * Created by patricktoner on 6/12/16.
 */
class RotogrinderWeatherAndLineup {


    String away
    String home
    Integer scheduleId
    Integer temp
    Date time
    String weather
    String weatherStatus
    String windDirection
    String windIcon
    String windSpeed

    List<Precip> precips = new ArrayList<>()
    List<Team> teams = new ArrayList<>()


    public RotogrinderWeatherAndLineup(def element) {

        DateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z")
        //sdf.setTimeZone(TimeZone.getTimeZone("EDT"));

        away = element.away
        home = element.home
        scheduleId = element.scheduleId
        temp = NumberUtil.parseInt(element.temp.reverse().drop(1).reverse())
        time = sdf.parse(element.time)  //Chronic.parse(element.time)?.beginCalendar?.time
        weather = element.weather
        weatherStatus = element.weatherStatus
        windDirection = element.windDirection
        windIcon = element.windIcon
        windSpeed = element.windSpeed


        element.teams?.each { teamElement ->
            Team team = new Team(teamElement)
            teams.add(team)
        }

        element.precips?.each { precipElement ->
            Precip precip = new Precip(precipElement)
            precips.add(precip)
        }



    }


    public class Precip {

        public Precip(def element) {
            percent = NumberUtil.parseInt(element.percent.reverse().drop(1).reverse())
            time = element.time
        }


        Integer percent
        String time
    }

    public class Team {

        public Team(def element) {
            abbrev = element.abbrev
            status = element.status

            element.players?.each { playerElement ->
                Player player = new Player(playerElement)
                if (player) players.add(player)

            }

        }

        String status
        String abbrev
        List<Player> players = new ArrayList<>()
    }

    public class Player {

        public Player(def element) {
            id = NumberUtil.parseInt(element.id)
            name = element.name
            order = NumberUtil.parseInt(element.order)
            pos = element.pos
        }

        Integer id
        String name
        Integer order
        String pos
    }


}
