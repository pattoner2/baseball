package com.baseball.rotogrinders

import com.baseball.gameday.NumberUtil

/**
 * Created by patricktoner on 6/12/16.
 */
class RotogrinderParkFactor {

    String name
    Double lRuns
    Double lHr
    Double l1b
    Double l2b
    Double l3b
    Double lBb

    Double rRuns
    Double rHr
    Double r1b
    Double r2b
    Double r3b
    Double rBb


    public RotogrinderParkFactor(def element) {
        name = element.name
        lRuns = NumberUtil.parseDouble(element.lRuns)
        lHr = NumberUtil.parseDouble(element.lHr)
        l2b = NumberUtil.parseDouble(element.l2b)
        l3b = NumberUtil.parseDouble(element.l3b)
        lBb = NumberUtil.parseDouble(element.lBb)
        l1b = NumberUtil.parseDouble(element.l1b)


        rRuns = NumberUtil.parseDouble(element.rRuns)
        rHr = NumberUtil.parseDouble(element.rHr)
        r2b = NumberUtil.parseDouble(element.r2b)
        r3b = NumberUtil.parseDouble(element.r3b)
        rBb = NumberUtil.parseDouble(element.rBb)
        r1b = NumberUtil.parseDouble(element.r1b)
    }

}
