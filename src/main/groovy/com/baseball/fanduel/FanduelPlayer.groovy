package com.baseball.fanduel

import com.baseball.interfaces.ExternalPlayer

/**
 * Created by patricktoner on 6/2/16.
 */
class FanduelPlayer implements ExternalPlayer {

    String id
    String position
    String firstName
    String lastName
    String FPPG
    String played
    Integer salary
    String game
    String team
    String opponent
    String injuryIndicator
    String injuryDetails
    String probablePitcher
    String battingOrder


}
