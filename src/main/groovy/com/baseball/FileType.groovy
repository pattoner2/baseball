package com.baseball
/**
 * Created by patricktoner on 5/20/16.
 */
public enum FileType {
    LINE_SCORE("linescore.xml"),
    BOX_SCORE("boxscore.xml"),
    PLAYERS("players.xml"),
    GAME_EVENTS("game_events.xml"),
    INNING_ALL("inning/inning_all.xml")


    private final String fileName

    FileType(String value){
        this.fileName = value;
    }

    String getFileName() {
        fileName
    }

    String getKey() {
        name()
    }

    public static List<FileType> list() {
        return [LINE_SCORE, BOX_SCORE, PLAYERS, INNING_ALL]
    }
}
