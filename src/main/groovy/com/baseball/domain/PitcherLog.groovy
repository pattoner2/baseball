package com.baseball.domain

import com.baseball.Player

/**
 * Created by patricktoner on 8/22/16.
 */
abstract class PitcherLog {

    static constraints = {

        era nullable:true 

        pitcher nullable:false
        games nullable:true
        starts nullable:true
        wins nullable:true
        losses nullable:true
        saves nullable:true
        bs nullable:true

        outs nullable:true
        er nullable:true
        so nullable:true
        h nullable:true
        bb nullable:true
        sho nullable:true
        cg nullable:true
        pc nullable:true
        hbp nullable:true


        battersFace nullable:true
        strikes nullable:true
        balls nullable:true

        runs nullable:true
        singles nullable:true
        doubles nullable:true
        triples nullable:true
        hr nullable:true

        groundOuts nullable:true
        flyOuts nullable:true
        lineOuts nullable:true
        groundBalls nullable:true
        lineDrives nullable:true
        flyBalls nullable:true

        paSingles nullable:true
        paDoubles nullable:true
        paTriples nullable:true
        paHr nullable:true
        paRbi nullable:true
        paRuns nullable:true


        opponentSoPercent nullable:true
        opponentObp nullable:true
        opponentSlg nullable:true
        opponentOps nullable:true
        opponentWOba nullable:true
        opponentIso nullable:true
        opponentWFda nullable:true

        soPercent nullable:true
        bbPercent nullable:true


        siera nullable:true
        xFip nullable:true

        fanduelPoints nullable:true
    }

    Player pitcher

    Integer games = 0
    Integer starts = 0
    Integer wins = 0
    Integer losses = 0
    Integer saves = 0
    Integer bs = 0

    Integer outs = 0
    Integer er = 0
    Integer so = 0
    Integer h = 0
    Integer bb = 0
    Integer sho = 0
    Integer cg = 0
    Integer pc = 0
    Integer hbp = 0

    Integer singles = 0
    Integer doubles = 0
    Integer triples = 0



    Integer battersFace = 0
    Integer strikes = 0
    Integer balls = 0
    Integer runs = 0
    Integer hr = 0

    Double era = 0


    Integer groundOuts = 0
    Integer flyOuts = 0

    Integer lineOuts = 0
    Integer groundBalls = 0
    Integer lineDrives = 0
    Integer flyBalls = 0




    Double paSingles = 0
    Double paDoubles = 0
    Double paTriples = 0
    Double paHr = 0
    Double paRbi = 0
    Double paRuns = 0





    Double opponentObp = 0
    Double opponentSlg = 0
    Double opponentOps = 0
    Double opponentIso = 0
    Double opponentWOba = 0
    Double opponentWFda = 0
    Double opponentSoPercent = 0

    Double siera = 0
    Double xFip = 0


    Double soPercent = 0
    Double bbPercent = 0



    Double fanduelPoints = 0


    public void clearAll() {
        games = 0
        starts = 0
        wins = 0
        losses = 0
        saves = 0
        bs = 0

        outs = 0
        er = 0
        so = 0
        h = 0
        bb = 0
        sho = 0
        cg = 0
        pc = 0
        hbp = 0

        singles = 0
        doubles = 0
        triples = 0

        paSingles = 0
        paDoubles = 0
        paTriples = 0
        paHr = 0
        paRbi = 0
        paRuns = 0


        battersFace = 0
        strikes = 0
        balls = 0
        runs = 0
        hr = 0

        era = 0


        groundOuts = 0
        flyOuts = 0

        lineOuts = 0
        groundBalls = 0
        lineDrives = 0
        flyBalls = 0

        opponentObp = 0
        opponentSlg = 0
        opponentOps = 0
        opponentIso = 0
        opponentWOba = 0
        opponentWFda = 0
        opponentSoPercent = 0

        siera = 0
        xFip = 0


        soPercent = 0
        bbPercent = 0

        fanduelPoints = 0
    }


}
