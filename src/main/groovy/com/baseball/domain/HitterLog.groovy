package com.baseball.domain

import com.baseball.Player

/**
 * Created by patricktoner on 8/22/16.
 */
public class HitterLog {

    static constraints = {

        hitter nullable:false
        games nullable:true
        atBats nullable:true
        pa nullable:true
        hits nullable:true
        singles nullable:true
        doubles nullable:true
        triples nullable:true
        homeRuns nullable:true
        runs nullable:true
        rbi nullable:true
        bb nullable:true
        sb nullable:true
        cs nullable:true

        hbp nullable:true
        so nullable:true
        lob nullable:true
        sacBunts nullable:true
        sacFlys nullable:true
        groundOuts nullable:true
        flyOuts nullable:true


        lineOuts nullable:true
        groundBalls nullable:true
        lineDrives nullable:true
        flyBalls nullable:true


        gidp nullable:true
        po nullable:true
        assists nullable:true
        e nullable:true

        avg nullable:true

        ibb nullable:true

        wOba nullable:true
        wFda nullable:true
        soPercent nullable:true
        bbPercent nullable:true
        iso nullable:true

        obp nullable:true
        slg nullable:true
        ops nullable:true
        tb nullable:true





        paHits nullable:true
        paSingles nullable:true
        paDoubles nullable:true
        paTriples nullable:true
        paHomeRuns nullable:true
        paRuns nullable:true
        paRbi nullable:true
        paBb nullable:true
        paSb nullable:true
        paCs nullable:true
        paHbp nullable:true
        paSo nullable:true
        paSacBunts nullable:true
        paSacFlys nullable:true
        paAvg nullable:true
        paIbb nullable:true
        paObp nullable:true
        paSlg nullable:true
        paOps nullable:true
        paTb nullable:true
        paWOba nullable:true
        paWFda nullable:true
        paSoPercent nullable:true
        paBbPercent nullable:true
        paIso nullable:true



        fanduelPoints nullable:true
    }

    Player hitter


    Integer games = 0
    Integer pa = 0

    Integer atBats = 0
    Integer hits = 0


    Integer singles = 0
    Integer doubles = 0
    Integer triples = 0
    Integer homeRuns = 0
    Integer runs = 0
    Integer rbi = 0
    Integer bb = 0
    Integer sb = 0
    Integer cs = 0
    Integer hbp = 0
    Integer so = 0
    Integer lob = 0
    Integer sacBunts = 0
    Integer sacFlys = 0

    Integer groundOuts = 0
    Integer flyOuts = 0

    Integer lineOuts = 0
    Integer groundBalls = 0
    Integer lineDrives = 0
    Integer flyBalls = 0


    Integer gidp = 0
    Integer po = 0
    Integer assists = 0
    Integer e = 0


    //Calculated stats
    Double avg = 0
    Integer ibb = 0
    Double obp = 0
    Double slg = 0
    Double ops = 0

    Integer tb = 0


    Double wOba = 0
    Double wFda = 0

    Double soPercent = 0
    Double bbPercent = 0

    Double iso = 0



    Double paHits = 0
    Double paSingles = 0
    Double paDoubles = 0
    Double paTriples = 0
    Double paHomeRuns = 0
    Double paRuns = 0
    Double paRbi = 0
    Double paBb = 0
    Double paSb = 0
    Double paCs = 0
    Double paHbp = 0
    Double paSo = 0
    Double paSacBunts = 0
    Double paSacFlys = 0
    Double paAvg = 0
    Double paIbb = 0
    Double paObp = 0
    Double paSlg = 0
    Double paOps = 0
    Double paWOba = 0
    Double paWFda = 0
    Double paSoPercent = 0
    Double paBbPercent = 0
    Double paTb = 0
    Double paIso = 0

    Double fanduelPoints = 0


    public void clearAll() {
        games = 0
        pa = 0

        atBats = 0
        hits = 0


        singles = 0
        doubles = 0
        triples = 0
        homeRuns = 0
        runs = 0
        rbi = 0
        bb = 0
        sb = 0
        cs = 0
        hbp = 0
        so = 0
        lob = 0
        sacBunts = 0
        sacFlys = 0

        groundOuts = 0
        flyOuts = 0

        lineOuts = 0
        groundBalls = 0
        lineDrives = 0
        flyBalls = 0



        gidp = 0
        po = 0
        assists = 0
        e = 0


        avg = 0
        ibb = 0
        obp = 0
        slg = 0
        ops = 0

        tb = 0


        wOba = 0
        wFda = 0

        soPercent = 0
        bbPercent = 0

        iso = 0



        paHits = 0
        paSingles = 0
        paDoubles = 0
        paTriples = 0
        paHomeRuns = 0
        paRuns = 0
        paRbi = 0
        paBb = 0
        paSb = 0
        paCs = 0
        paHbp = 0
        paSo = 0
        paSacBunts = 0
        paSacFlys = 0
        paAvg = 0
        paIbb = 0
        paObp = 0
        paSlg = 0
        paOps = 0
        paWOba = 0
        paWFda = 0
        paSoPercent = 0
        paBbPercent = 0
        paTb = 0
        paIso = 0

    }




}
